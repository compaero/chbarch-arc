

function chBarch() {
    var chbar = this;
    chbar.targetElement = '';
    chbar.categoryId = '';
    chbar.urls = {
        cart: '/cart.html',
        category: '/service/categories'
    };

    this.sendRequest = function(type, action, params, callback){
        if (type == undefined) return false;
        if (callback == undefined) callback = chbar.fillHTMLElement;
        if (params == undefined) params = {};
        params.action = type;
        if ((action != undefined) && (action != '')) params.action = params.action+'/'+action;
        $.ajax({
            data: params,
            dataType: 'html',
            type: 'POST',
            url: chbar.urls[type],
            success: callback
        });
    };

    this.cartAction = function(action, params, strict) {
        if(!strict){
            strict =0;
        }
        params.ms2_action = 'cart/'+action;
        $.post('/cart.html',params, function(data) {
            $('.col-md-5.aside-right').html(data);
            console.log(strict);
            if (strict ==0) {
                chbar.upsale();
                chbar.AddButtonGift();
            }
        });

    };

    this.AddButtonGift = function(action){


        $.post('/service/create-order',{ch_action:'giftsList'}, function(success){
            var OBjgift = jQuery.parseJSON(success);
            if(OBjgift.success == true){
                if($("#btn-gift-select").length==0) {
                    $('.cardmenu').append('<div class="btn-group"><button type="button" class="btn btn-primary btn-lg" id="btn-gift-select"><i class="fa fa-ticket"></i> Подарок</button></div>');

                }
                if(action==1) {
                    if ($("#btn-gift-select").length == 0) {
                        $('.aside-right').append('<div class="btn-group"><button type="button" class="btn btn-primary btn-lg" id="btn-gift-select"><i class="fa fa-ticket"></i> Подарок</button></div>');

                    }
                }
            }
            else{
                $('#btn-gift-select').css('display','none');
            }

        });
    };


    this.fillHTMLElement = function(data) {
        $(chbar.targetElement).html(data);
        return true;
    };

    this.fillHTMLMenu = function(data) {
        $(chbar.targetElement).html(data);
        $findElement = $('[data-id = '+chbar.categoryId+'][type = "button"]');
        $findElement.addClass('btn-info');


        return true;
    };

    this.upsale = function() {
        $.post('/service/upsale',{ch_action: 'upsale/getProducts'}, function (success) {
            result = $.parseJSON(success);
            if (result.success) {
                $('#upsale .modal-body').html(result.html);
                //$('#spOffer-select').modal('toggle');
                if (!result.next) {
                    $('#upsaleBtnReject').addClass('disabled');
                    $('#upsale').addClass('upsale_step2');
                }
                $('#upsale').modal('show');

            }
        });


    };

    this.orderTrash = function() {
        event.preventDefault();
        chbar.cartAction('clean',{});
    };

    this.orderRemoveItem = function() {
        event.preventDefault();
        chbar.cartAction('remove',{key: $(event.target).data('key')});
    };

    this.orderChangeItem = function() {
        event.preventDefault();
        count = $(event.target).data('count');
        key = $(event.target).data('key');
        switch($(event.target).data('dir')) {
            case 'minus': count = count - 1; break;
            case 'plus': count = count + 1; break;
        }
        chbar.cartAction('change',{key: key, count: count});
    };

    this.loadCategory = function() {
        event.preventDefault();
        target = event.target;
        if (target.nodeName.toLowerCase() == 'span') {
            target = target.parentNode;
        }
        button = $(target);
        if($(target).hasClass('managerBlock')){
           window.location.href = '/?q='+$(target).data('id');

        }
        if($(target).hasClass('subcategory')){
            $('.btn-categories.subcategory').removeClass('btn-info');

        } else {

            $('.category').removeClass('btn-info');
            $('.btn-categories.category').addClass('btn-primary');
        }
        //$('.category').removeClass('btn-info');
        $('.btn-categories.subcategory').addClass('btn-success');
        $(target).removeClass('btn-primary btn-success');
        $(target).addClass('btn-info');
        chbar.targetElement = '#menu-wrapper';
        params = {};
        params.cat = $(target).data('id');
        chbar.categoryId = params.cat;
        //console.log($(event.target).data('id'));
        href = $(target).attr('href');
        if (href != undefined && href.indexOf('=', 0) > 0) {
            params.page = href.substr(href.indexOf('=', 0)+1);
        }
        //console.log(params);
        chbar.sendRequest('category','',params,chbar.fillHTMLMenu);
    };


}


(function($) {

    $(document).ready(function(){
        window.chb = new chBarch;
        $(document).on('click', '.order-trash', chb.orderTrash);
        $(document).on('click', '.btn-order-remove-item', chb.orderRemoveItem);
        $(document).on('click', '.order-item-change', chb.orderChangeItem);
        $(document).on('click', '.btn-categories', chb.loadCategory);


    });



}) (jQuery);


!function ($) {$(function(){
    //var d = new Date(2011, 0, 1, 2, 3, 4, 567);



    $('.my-date-picker').click(function(){
        var my_super_num;
        if ($(this).hasClass('my-date-picker-today')) {
            my_super_num = 1;
        }
        else if ($(this).hasClass('my-date-picker-yesterday')) {
            my_super_num = 2;
        }
        $.post('', {get_my_dates:my_super_num}, function(data){
            if(data !== null) {
                $('#datetimepicker1 input').val(data.one);
                $('#datetimepicker2 input').val(data.two);
            }
        },"json");

        alert(myDateArr['yesterday']);
        return false;
    });


//});

    if (!localStorage.getItem('font-size')) {
        localStorage.setItem('font-size', '14px');
    }
    $font_size = localStorage.getItem('font-size');
    var $font_size = '18px';

    setTimeout(function() {$('#place-add').val(localStorage.getItem('place') > 0 ? localStorage.getItem('place') : ''); }, 1000);

    $(document).on('keyup', '#place-add', function() {
        localStorage.setItem('place', parseInt($(this).val(),10));
    });

    $('.workspace, #footer .special-offer > li').css('font-size', $font_size);

// Slide to remove
    /*
     $('.order').on('swiperight swipeleft','.order-item',function(e){
     var order = $(this);
     var item = order.children('.order-name').text();

     var cancelButton = $('<button />', {'type': 'button', 'class': 'btn btn-info btn-lg order-return pull-right', text: 'Восстановить?'}).click(function(e){
     e.stopPropagation();
     menu.fadeOut(function(){
     order.animate({width: 'toggle'}, 400, function(){
     menu.remove();
     });
     });
     });

     var row = $('<td />', {'colspan': 4, 'class': 'text-danger', text: 'Позиция «' + item + '» удалена'}).append(cancelButton);
     var menu = row.wrap('<tr class="order-deleted"></tr>').parent();

     order.after(menu);
     order.animate({width: 'toggle'}, 400, function(){
     menu.fadeIn();
     });
     });
     */
// Add to item

    /*$(document).on('click', '.order-item-plus', function(){
     curCount = $(this).data('count');
     curCount = parseInt(curCount);
     gCount = curCount + 1;
     $.post('',{key: $(this).data('key'), count: (gCount), ms2_action: 'cart/change', ctx: 'web'}, function(success){
     location.pathname = location.pathname;
     });
     });*/

// Remove from item
    /*
     $(document).on('click', '.order-item-minus', function(){
     curCount = $(this).data('count');
     curCount = parseInt(curCount);
     lCount = curCount - 1;
     $.post('',{key: $(this).data('key'), count: (lCount), ms2_action: 'cart/change', ctx: 'web'}, function(success){
     location.pathname = location.pathname;
     });
     });
     */

// Modals

    $(document).on('click', '.prodId', function(){
        prodId = $(this).attr('id');
        prodTitle = $(this).text();
        $.post('/service/create-order',{ch_action:'getProdModal', id: prodId}, function(success){
            $('#insertion').empty();
            $('#insertion').prev().children('.modal-title').text(prodTitle);
            $('#insertion').append(success);
            $('#additional').modal('toggle');
        });
    });

    $(document).on('blur', '.prod-comment', function(){
        $.post('/service/create-order',{ch_action:'addProdComments', key: $(this).data('id'), comment: $('.prod-comment').val()}, function(success){
            //result = $.parseJSON(success);
            //$('.col-lg-8').html(success);
        });
        return false;
    });


    $(document).on('click', '#btn-order-additional', function(){
        //prodId = $(this).attr('id');
        $.post('/service/create-order',{ch_action:'getOrderModal'}, function(success){
            $('#insertion').empty();
            $('#insertion').append(success);
            $('#additional').modal('toggle');
        });
    });
    //========================Выбор подарка===========/////
    $(document).on('click', '#btn-gift-select', function(){

        $.post('/service/create-order',{ch_action:'giftsList'}, function(success){
            $('#giftslist').empty();
            var OBjgift = jQuery.parseJSON(success);
            if(OBjgift.success == true){
                $.each(OBjgift.giftname, function (index, value) {
                    $('#giftslist').append('<div class="giftname">'
                        + value
                        + ' ( '
                        + OBjgift.count[index]
                        + ' ) </div>');


                    $.each(OBjgift.nameprod, function (indexx, value) {


                        if(OBjgift.nameprod[indexx].gift_uid == OBjgift.uid[index]){
                            if($('[id-product = '+OBjgift.idprod[indexx]+']').length && $('[uid = '+OBjgift.nameprod[indexx].gift_uid+']').length){
                                console.log($('[id-product = '+OBjgift.idprod[indexx]+']'));

                            }
                            else{
                            $('#giftslist').append('<button type="button" class="btn btn-primary btn-lg btn-block" id="gift-item" id-product="'+OBjgift.idprod[indexx]+'"uid="'+OBjgift.uid[index]+'" Productname="' +OBjgift.nameprod[indexx].name + '"Product-price="' +OBjgift.price[index] + '" >' +
                                OBjgift.nameprod[indexx].name +
                                '</button>');
                          }
                        }


                    });


                });

            $('#gift-select').modal('toggle');
        };

        });
    });
    //======================Добавление подарка в заказ=======================
    $(document).on('click', '#gift-item', function(){
        giftuid = $(this).attr('uid');
        productid = $(this).attr('id-product');
        prodprice=$(this).attr('product-price');
        options = {price:prodprice,gift_uid:giftuid};
        options = JSON.stringify(options);
        chb.cartAction('add', {id:productid,count:"1",options: options});
        $('#gift-select').modal('toggle');
    });

    $(document).on('blur', '.order-comment', function(){
        $.post('/service/create-order',{ch_action:'addOrderComment', comment: $('.order-comment').val()}, function(success){
            //result = $.parseJSON(success);
            //$('.col-lg-8').html(success);
        });
        return false;
    });

    /*$(document).on('click', '#btn-order-additional', function(){
     $.post('/service/create-order',{ch_action:'getOrderComments'}, function(success){
     $('#order-comments').val(success);
     });
     $('#order-additional').modal('toggle');
     });*/





    $(document).on('click', '#btn-flyer-select', function(){
        $.post('/service/flyer',{ch_action:'flyer/globalParents'}, function(success){
            result = $.parseJSON(success);
            $('#flyer-select .modal-body').html(result.html);
            $('#flyer-select').modal('toggle');
        });
    });

    $(document).on('click', '.btn-flyer-add', function(){
        //$('#flyer-select').modal('toggle');
        $.post('/service/flyer',{ch_action:'flyer/getProducts', parent: $(this).attr('value')}, function(success){
            result = $.parseJSON(success);
            $('#flyer-add .container-fluid').html(result.html);
            $('#flyer-select').modal('toggle');
            $('#flyer-add').modal('toggle');
        });
        //$('#flyer-add-tea').modal('toggle');
    });

    $(document).on('click', '#discountBarmen', function(){
        $.post(location.href,{chDiscount:'10'}, function(success){
            location.pathname = location.pathname;
        });
    });

    $(document).on('click', '#discountCustom', function(){
        $.post(location.href,{chDiscount: $(this).data('discount')}, function(success){
            location.pathname = location.pathname;
        });
    });

    $(document).on('click', '#discountManager', function(){
        $.post(location.href,{chDiscount:'20'}, function(success){
            location.pathname = location.pathname;
        });
    });

// ============================
// ============================
// ============================
// ============================

    $(document).on('click', '#chgoh_init', function(){
        //$('#spOffer-select').modal('toggle');
        //$("#spOfferDoneBtn").data('spoffer-key',$(this).data('spoffer-key'));
        $.post('/service/chguestorderhelperajax',{chgoh_action:'getNode'}, function(success){
            result = $.parseJSON(success);
            $('#chguestorderhelper-modal .modal-title').html(result.pagetitle);
            $('#btn-helper-node-return').val(result.parent);
            $('#btn-helper-node-return').addClass('disabled');
            $('#chguestorderhelper-modal .modal-body').html(result.html);
            $('#chguestorderhelper-modal').modal('toggle');
        });
    });

    $(document).on('click', '.btn-helper-node', function(event){
        //$('#spOffer-select').modal('toggle');
        //$("#spOfferDoneBtn").data('spoffer-key',$(this).data('spoffer-key'));
        $.post('/service/chguestorderhelperajax',{chgoh_action:'getNode',nodeid: this.value}, function(success){
            result = $.parseJSON(success);
            $('#chguestorderhelper-modal .modal-title').html(result.pagetitle);
            $('#btn-helper-node-return').val(result.parent);
            if (result.parent > 0) {
                $('#btn-helper-node-return').removeClass('disabled');
            }
            else {
                $('#btn-helper-node-return').addClass('disabled');
            }
            $('#chguestorderhelper-modal .modal-body').html(result.html);
            //$('#chguestorderhelper-modal').modal('toggle');
        });
    });

    $(document).on('click', '.btn-helper-product', function(){
        //$('#spOffer-select').modal('toggle');
        $(this).addClass('disabled');
        window.helper_product = this.value;
        $.post('/cart.html',{id: this.value, count: 1, ms2_action: 'cart/add', ctx: 'web'}, function(success){
            result = $.parseJSON(success);
            if (result.success) {
                $('#p-helper-message').removeClass('text-danger');
                $('#p-helper-message').addClass('text-success');
                $('#p-helper-message').html(result.message);
            }
            else {
                $('#p-helper-message').addClass('text-danger');
                $('#p-helper-message').removeClass('text-success');
                $('#p-helper-message').html(result.message);
            }
            $("#btn-helper-product-"+window.helper_product).removeClass('disabled');
        });

    });

    $(document).on('click', '#btn-helper-node-close', function(){
        location.href = location.href;
    });



// ============================
// ============================
// ============================
// ============================



// ===== > COMPLEX JS CODE START < ==== //

    $(document).on('click', '.subButton', function() {
        var subUnic = $(this).attr('id');
        var subGroupNum = $(this).attr('data-sub-group');
        var groupLimit = parseInt($(this).attr('data-sub-group-limit'));
        //var maxProductLimit = parseInt($('.complexMain').attr('data-max-product-limit'));
        //alert(1);
        var complexGroupCount = parseInt($('.complexMain').attr('data-group-count'));
        if($('.complexMain').hasClass('group-num-'+subGroupNum+'-is-here')) {
            false;
        }
        else {
            $('.complexMain').addClass('group-num-'+subGroupNum+'-is-here');
            //$('.complexMain').attr('data-group-count',complexGroupCount - 1);
            //$('.complexMain').attr('data-max-product-limit',maxProductLimit + groupLimit);
        }

        var complexCost = $('.complexMain').attr('data-complex-cost');
        var subCost = $(this).attr('data-sub-cost');
        $('.mincountforprod').remove();
        if ($(this).hasClass('selectedButton')) {
            false;
        } else {
            if($('.selectedProducts button.group-'+subGroupNum).length == groupLimit) {
                false;
            } else {
                if ($(this).attr('id') != 'undefined') {
                    var subCurrentId = $(this).attr('id');
                    if ($('.productListBlock').hasClass(subCurrentId)) {
                        if(!$("h2").is(".mincountforprod") && parseInt(groupLimit)>0){
                            var id = $(this).attr('id');
                            if(parseInt($('.selectedProducts').find('.selectedProduct.' + id).children().attr('data-product-count'))>0){
                                var groupLimit = parseInt(groupLimit) - parseInt($('.selectedProducts').find('.selectedProduct.' + id).children().attr('data-product-count'));
                            }


                        $('.productListBlock').prepend('<h2 prodcount ="'+groupLimit+'" class="mincountforprod">Минимальное количество '+groupLimit+'</h2>');

                        }
                        $('.selectedProductsBlock').fadeOut(50);
                        $('.productListBlock').stop().fadeOut(50);
                        $('.productListBlock.' + subCurrentId).stop().delay(60).fadeIn(100);
                    }
                }
            }
        }
    });

    $(document).on('click', '.productButton', function () {
        var checkedComplexCost = $('.complexMain').attr('data-complex-cost');
        var productParent = $(this).attr('data-product-parent');
        var productPrice = $(this).attr('data-product-price');
        var groupLimit = parseInt($('#' + productParent).attr('data-sub-group-limit'));
        var productGroup = $(this).attr('data-product-group');
        var productId = $(this).attr('data-product-id');
        var productName = $(this).attr('data-product-name');
        var productSubCost = $(this).attr('data-product-sub-cost');
        //var complexBaseCost = $('.complexMain').attr('data-complex-base-cost');
        var productCount = $(this).attr('data-product-count');
        var mincount = parseInt($('.productListBlock').find("h2").attr('prodcount'));
        if (parseInt(productSubCost) > 0) {
            var specialStringWithCost = ' <span style="background: #fe9;">+ ' + productSubCost + ' р.</span>';
        } else {
            var specialStringWithCost = '';
        }
        var allprop = {
            checkedComplexCost: checkedComplexCost,
            productParent: productParent,
            productPrice: productPrice,
            groupLimit: groupLimit,
            productGroup: productGroup,
            productId: productId,
            productName: productName,
            productSubCost: productSubCost,
            productCount: productCount,
            specialStringWithCost:specialStringWithCost
        };

        if (productCount < mincount) {
            $('#complex-product-count .modal-body .countbutton').empty();

            var i;
            for (i = 0; i < mincount; i++) {
                $('#complex-product-count .modal-body .countbutton').append(' <div class="col-sm-6 col-md-4"><button type="button" class="btn btn-primary btn-lg btn-block prodcountbut" data-dismiss="modal">' + (i + 1) + '</button></div>');

            }
            $.each(allprop, function (index, val){
                $('.countbutton').attr(index,val);
            });

            $('#complex-product-count').modal('toggle');

        }
        else{
            inputcomplex(allprop);
        }

    });
    $(document).on('click', '.prodcountbut', function () {

        var allprop = {
            checkedComplexCost: $(this).parent().parent().attr('checkedComplexCost'),
            productParent: $(this).parent().parent().attr('productParent'),
            productPrice: $(this).parent().parent().attr('productPrice'),
            groupLimit: $(this).parent().parent().attr('groupLimit'),
            productGroup: $(this).parent().parent().attr('productGroup'),
            productId: $(this).parent().parent().attr('productId'),
            productName: $(this).parent().parent().attr('productName'),
            productSubCost: $(this).parent().parent().attr('productSubCost'),
            productCount:$(this).html(),
            specialStringWithCost:$(this).parent().parent().attr('specialStringWithCost'),
            ostcount: parseInt($('.mincountforprod').attr('prodcount')) - parseInt($(this).html())
        };

        inputcomplex(allprop);
    });
function inputcomplex(allprop) {
    $('.mincountforprod').attr('prodcount',allprop.ostcount);
    $('.mincountforprod').html('Осталось '+allprop.ostcount);
    if($('.selectedProducts').find($('[data-product-id = "'+allprop.productId+'"]')).attr('data-product-id') >0){

        allprop.productCount = parseInt($('.selectedProducts').find($('[data-product-id = "'+allprop.productId+'"]')).attr('data-product-count'))+parseInt(allprop.productCount);
        $('.selectedProducts').find($('[data-product-id = "'+allprop.productId+'"]')).parent().remove();
    }
    var productHTML = '<div  data-product-count="' + allprop.productCount + '" data-product-price="' + allprop.productPrice + '" data-product-id="' + allprop.productId + '" class="thisSelectedProduct col-md-10" data-selected-parent="' + allprop.productParent + '">' +
        '<span class="this-prod-pagetitle">' + allprop.productName + allprop.specialStringWithCost + '</span></button>' +
        '</div>' +
        '<div class="col-md-2">' +
        '<button data-product-group="' + allprop.productGroup + '" data-selected-parent="' + allprop.productParent + '" class="productCloseButton btn btn-block group-' + allprop.productGroup + ' product-' + allprop.productId + '"data-product-price="' + allprop.productPrice + '" data-prod-sub-cost="' + allprop.productSubCost + '"><i class="fa fa-times"></i></button>' +
        '</div>';

    if ($('#' + allprop.productParent).hasClass('selectedButton')) {
        $('.selectedProduct.' + allprop.productParent).html(productHTML);

    } else {
        $('<div class="row selectedProduct ' + allprop.productParent + '">' +
            productHTML +
            '</div>').appendTo('.selectedProducts');


        if (parseInt(allprop.productSubCost) !== 0) {
            allprop.checkedComplexCost = parseInt(allprop.checkedComplexCost) + parseInt(allprop.productSubCost);
            //$('span#complexCost').text(checkedComplexCost);
            $('.complexMain').attr('data-complex-cost', allprop.checkedComplexCost);
        }
        if (parseInt(allprop.productPrice) !== 0) {

            allprop.checkedComplexCost = parseInt(allprop.checkedComplexCost) + parseInt(allprop.productPrice);
            //$('span#complexCost').text(checkedComplexCost);
            $('.complexMain').attr('data-complex-cost', allprop.checkedComplexCost);
        }
    }
    var count=0;
    $('.thisSelectedProduct[data-selected-parent = '+allprop.productParent+']').each(function(key, value) {

        count = parseInt(count) + parseInt($(this).attr('data-product-count'));
    });
    if(count<allprop.groupLimit){

    }
    else{
    $('#' + allprop.productParent).addClass('selectedButton');
    $('#' + allprop.productParent).removeClass('unselectedButton');

    if ($('.selectedProducts button.group-' + allprop.productGroup).length == allprop.groupLimit) {
        //$('.unselectedButton button').attr('disabled', 'disabled');
        $('.subButton.unselectedButton.sub-group-' + allprop.productGroup + ' button').addClass('disabledSub');
    }
    //productGroupsArray[productGroup][] = 1;
    $('#' + allprop.productParent + ' button span.this-status i').attr('class', 'fa fa-check');
    $('#' + allprop.productParent + ' button span.this-status').addClass('checked');
    if ($(this).attr('id') != 'undefined') {
        var subCurrentId = $(this).attr('id');
        if ($('.productListBlock').hasClass(subCurrentId)) {
            $('.productListBlock').stop().fadeOut(50);
            $('.productListBlock.' + subCurrentId).stop().delay(60).fadeIn(100);
        }
    }

    //productGroupsArray[productGroup]=  += 1;
    //alert(productGroupsArray[productGroup]);
    //}
    //alert($('.addOrderButton'));

    if ($('.selectedButton.parent-status-1').length == $('.complexMain').attr('data-max-product-limit')) {
        //$('.modal-body-category').after('<button class="addOrderButton btn btn-block btn-primary">Добавить заказ</button>');
        $('.addOrderButton').fadeIn(100);
        $('.orderButtonBlock .showPrice span').text($('.complexMain').attr('data-complex-cost'));
        $('.orderButtonBlock .showPrice').fadeIn(100);
        $('.addOrderButton').removeAttr('disabled');
    } else {
        $('.addOrderButton').fadeOut(50);
    }

    $('.productListBlock').stop().fadeOut(50);
    $('.selectedProductsBlock').stop().delay(60).fadeIn(100);
    }
};


    $(document).on('click', '.selectedProduct .thisSelectedProduct', function() {
        var thisSubUnic = $(this).attr('data-selected-parent');
        if ($('.productListBlock').hasClass(thisSubUnic)) {
            $('.selectedProductsBlock').fadeOut(50);
            $('.productListBlock').fadeOut(50);
            $('.productListBlock.' + thisSubUnic).stop().delay(60).fadeIn(100);
        }
    });

    $(document).on('click', '.menuGoBack', function() {
        $('.productListBlock').stop().fadeOut(50);
        $('.selectedProductsBlock').stop().delay(60).fadeIn(100);
    });

    $(document).on('click', '.productCloseButton', function() {
        var thisComplexCost = $('.complexMain').attr('data-complex-cost');
        var thisSubUnic = $(this).attr('data-selected-parent');
        var productprice = $(this).attr('data-product-price');
        var thisProductGroup = $(this).attr('data-product-group');
        var thisProductId = $(this).parent().parent().attr('data-product-id');
        $('#'+thisSubUnic).removeClass('selectedButton');
        $('#'+thisSubUnic).addClass('unselectedButton');
        $('#'+thisSubUnic).find('button span.this-status i').attr('class', 'fa fa-plus');
        var thisButtonCost = $(this).attr('data-prod-sub-cost');
        //alert(thisButtonCost);
        if(thisButtonCost !== 0) {
            thisComplexCost = parseInt(thisComplexCost) - thisButtonCost;
            //$('span#complexCost').text(thisComplexCost);
            $('.complexMain').attr('data-complex-cost', thisComplexCost);
            $('.orderButtonBlock .showPrice > span').text(thisComplexCost);
        }
        if(productprice !== 0) {
            thisComplexCost = parseInt(thisComplexCost) - productprice;
            //$('span#complexCost').text(thisComplexCost);
            $('.complexMain').attr('data-complex-cost', thisComplexCost);
            $('.orderButtonBlock .showPrice > span').text(thisComplexCost);
        }
        $(this).parent().parent().remove();
        //$('.selectedProduct.'+thisSubUnic).remove();
        $('.addOrderButton').attr('disabled', 'disabled');
        if($('.selectedButton.parent-status-1').length != $('.complexMain').attr('data-max-product-limit')) {
            $('.addOrderButton').fadeOut(20);
            $('.orderButtonBlock .showPrice').fadeOut(20);
            $('.orderButtonBlock .showPrice span').text('');
        }
        $('.subButton.unselectedButton.sub-group-'+thisProductGroup+' button').removeClass('disabledSub');
    });

    //alert($(this).data('product-id'));
    $(document).on('click', '.complex', function(){
        $.post('/service/complex',{complexAction:'complexOpen','complexIt':$(this).attr('data-product-it')}, function(data){
            result = $.parseJSON(data);
            $('#footer').after(result.html.main);
            //$('.complexMain').attr('data-real-count', $('.complexGroup.group-status-1').length);
            var maxProductLimit = 0;
            $('.complexGroup.group-status-1').each(function() {
                maxProductLimit += Number($(this).attr('data-group-limit'));
            });
            $('.complexMain').attr('data-max-product-limit', maxProductLimit);
            $('#spOffer-select').on('hidden.bs.modal', function (e) {
                $('#spOffer-select').remove();
            });
            $('#spOffer-select').modal('toggle');
        });
    });

    $(document).on('click', '.addOrderButton', function(){
        var complexId = $('.complexMain').attr('data-complex-id');
        var complexCost = $('.complexMain').attr('data-complex-cost');
        var complexType = 'discount';
        var productBlock = [];
        $(".thisSelectedProduct").each(function(key, value) {
            var productId = $(this).attr('data-product-id');
            var productCount = $(this).attr('data-product-count');
            //productBlock.products = {key:[]};
            productBlock[key] = {id:productId, count:productCount};
        });

        productBlock = JSON.stringify(productBlock);
        //alert(productBlock);

        $.post('/service/complex',{complexAction:'complexOrder', 'complexId':complexId, 'complexCost':complexCost, 'complexData':productBlock, 'complexType':complexType}, function(data){
            result = $.parseJSON(data);
            $('#spOffer-select').modal('hide');
            chb.cartAction('add', {});
            //location.pathname = location.pathname;
        });
    });

    /* ---------------------- COMPLEX JS CODE FINISH ---------------------- */

    /* ====================== SPOFFER (SPECIAL OFFER) JS CODE START ====================== */

    //$(document).on('click', '.special_offer', function(){
    //    //alert($(this).data('product-id'));
    //    $.post('/service/special-offer',{ch_action:'spOffer/globalParents','spOffer':$(this).data('product-id')}, function(success){
    //        //alert(success);
    //        result = $.parseJSON(success);
    //        $('#spOffer-select .modal-body').html(result.html);
    //        $('#spOffer-select').modal('toggle');
    //
    //    });
    //});
    //
    ////  SEARCH MY-2
    //$(document).on('click', '.btn-spOffer-add', function(){
    //  //$('#spOffer-select').modal('toggle');
    //  $("#spOfferDoneBtn").data('spoffer-key',$(this).data('spoffer-key'));
    //  $.post('/service/special-offer',{ch_action:'spOffer/getProducts', spOfferKey: $(this).data('spoffer-key'), spOfferPartId: $(this).data('spoffer-partid')}, function(success){
    //    result = $.parseJSON(success);
    //    $('.this-product').fadeOut(100);
    //    $('.modal-product-list').html(result.html);
    //    $('.modal-product-list').fadeIn(100);
    //  //  $('#spOffer-add .container-fluid').html(result.html);
    //  //  $('#spOffer-select').modal('toggle');
    //  //  $('#spOffer-add').modal('toggle');
    //  });
    //});
    //
    ////  SEARCH MY-3
    //$(document).on('click', '.spOfferPartAdd', function(){
    //  $.post('/service/special-offer',{ch_action:'spOffer/addProduct', spOfferKey: $(this).data('spoffer-key'), spOfferPartId: $(this).data('spoffer-partid'), spOfferProductId: $(this).data('spoffer-productid')}, function(success){
    //    result = $.parseJSON(success);
    //    $('.modal-product-list').fadeOut(100);
    //    $('.modal-body-product').html(result.html);
    //    $('.this-product').fadeIn(100);
    //      //$('#spOffer-select').modal('toggle');
    //    //$('#spOffer-add').modal('toggle');
    //  });
    //});

    /* ==========================================*/
    /* ==========================================*/
    /* ==========================================*/
    $(document).on('click', '.special_offer', function(){
        //alert($(this).data('product-id'));
        $.post('/service/special-offer',{ch_action:'spOffer/globalParents','spOffer':$(this).data('product-id')}, function(success){
            result = $.parseJSON(success);
            $('#spOffer-select .modal-body').html(result.html);
            $('#spOffer-select').modal('toggle');
        });
    });

    $(document).on('click', '.btn-spOffer-add', function(){
        //$('#spOffer-select').modal('toggle');
        $("#spOfferDoneBtn").data('spoffer-key',$(this).data('spoffer-key'));
        $.post('/service/special-offer',{ch_action:'spOffer/getProducts', spOfferKey: $(this).data('spoffer-key'), spOfferPartId: $(this).data('spoffer-partid')}, function(success){
            result = $.parseJSON(success);
            $('#spOffer-add .container-fluid').html(result.html);
            $('#spOffer-select').modal('toggle');
            $('#spOffer-add').modal('toggle');
        });
    });

    $(document).on('click', '.spOfferPartAdd', function(){
        $.post('/service/special-offer',{ch_action:'spOffer/addProduct', spOfferKey: $(this).data('spoffer-key'), spOfferPartId: $(this).data('spoffer-partid'), spOfferProductId: $(this).data('spoffer-productid')}, function(success){
            result = $.parseJSON(success);
            $('#spOffer-select .modal-body').html(result.html);
            $('#spOffer-select').modal('toggle');
            $('#spOffer-add').modal('toggle');
        });
    });


    $(document).on('click', '#spOfferDoneBtn', function(){
        $.post('/service/special-offer',{ch_action:'spOffer/add2Cart', spOfferKey: $(this).data('spoffer-key')}, function(success){
            result = $.parseJSON(success);
            location.pathname = location.pathname;
            //location.reload();
            //$('#spOffer-select .modal-body').html(result.html);
            //$('#spOffer-select').modal('toggle');
            //$('#spOffer-add').modal('toggle');
        });
    });

    //$(document).on('click', '#btn-order-additional', function(){
    //    $('#order-additional').modal('toggle');
    //});

    /* ==========================================*/
    /* ==========================================*/
    /* ==========================================*/


    var toPay = 0;
    $(document).on('click', '#btn-order-close', function(){
        $('#order-close').modal('toggle').on('shown.bs.modal', function() {
            toPay = $('#order-result').data('cost');
            if(!(toPay / toPay)) {
                toPay = toPay.replace(/\s+/g,'');
            }
            toPay = parseInt(toPay);
            //toPay = parseInt($('#btn-order-close').text().replace(/\s/g,''),10);
            $('#to-pay').text(toPay+' руб.');
            $('#useAccount').val(false);
            $('#order-card').attr('disabled',false);
            $('#cash').val('').focus();
            $('#order-close').toggleClass('openedBlock');
            if (chbcs.guest.rub_ac !== undefined && chbcs.guest.rub_ac > 0 && chbcs.guest.checked_phone == 1) {
                $('#useAccount_status').html('(' + chbcs.guest.rub_ac + 'р.)');
                $('#useAccountIcon').removeClass('fa-check');
                $('#useAccount').attr('disabled', false);
            } else {
                $('#useAccount_status').html('(0 р.)');
                $('#useAccountIcon').removeClass('fa-check');
                $('#useAccount').attr('disabled', true);
            }
            /*   $("#documents").prop("disabled", false);
            $("#useAccount").prop("disabled", true);
            $("#order-cash").prop("disabled", true);
            $("#order-card").prop("disabled", true);*/
        });
    });

   /* $("#documents").on('click', function(obj){
        $("#useAccount").prop("disabled", false);
        $("#order-cash").prop("disabled", false);
        $("#order-card").prop("disabled", false);
    });*/

    $('#useAccount').on('click', function(obj){
        icon = $('#useAccountIcon');
        button = $('#useAccount');
        if (icon.hasClass('fa-check')) {

            button.removeClass('btn-primary').addClass('btn-info');
            button.val(false);
            icon.removeClass('fa-check');
            icon.attr('value',false);
            toPay = $('#order-result').data('cost');
            if(!(toPay / toPay)) {
                toPay = toPay.replace(/\s+/g,'');
            }
            toPay = parseInt(toPay);
            $('#order-card').attr('disabled',false);
        } else {
            button.removeClass('btn-info').addClass('btn-primary');
            button.val(true);
            icon.addClass('fa-check');
            icon.attr('value',true);

            toPay = $('#order-result').data('cost');
            if(!(toPay / toPay)) {
                toPay = toPay.replace(/\s+/g,'');
            }
            toPay = parseInt(toPay);
            if (chbcs.guest.rub_ac < toPay) {
                toPay = toPay - chbcs.guest.rub_ac;
            } else {
                toPay = 0;
                $('#order-card').attr('disabled',true);
            }
            //toPay = parseInt($('#btn-order-close').text().replace(/\s/g,''),10);
        }
        $('#to-pay').text(toPay+' руб.');
    });

    var callback = function(e){
        var code = e.which ? e.which : e.keyCode;
        if(13 === code){
            if(!$('.starttable').hasClass('checked')) {
                $('.starttable').addClass('checked');
                $('.starttable').focus();
            }
            else {
                $("#order-cash").trigger('click');
            }
        }
    };
    $('#cash').keydown(callback);
    $('.starttable').focus(function(){
        $(this).addClass('checked');
    });
    $('.starttable').keydown(callback);
    $('#cash').focus(function() {
        $('.starttable').removeClass('checked');
    });


    var $balance = 0,
        $orderId = 0;

    $(document).on('click', '#order-cash', function(){
        var cash = parseInt($('#cash').val(),10);
        var starttable = $('.starttable').val();
        if ($('#cash').val().length == 0) cash = 0;
        console.log('cash: '+cash);
        console.log('toPay: '+toPay);
        if (cash >= 0 && cash >= toPay) {
            $(this).addClass('disabled');
            var useAccount = $('#useAccount').val();
            $.post('/service/create-order',{ch_action: 'order/create', payment: 'cash', take_sum: cash, table: starttable, useAccount: useAccount}, function(success) {
                var obj = jQuery.parseJSON(success);
                $orderId = obj.data.msorder;
                orderClear();
                tablesRefresh();
            });

            $('#order-close').modal('toggle');
            $('#order-close-bycash').modal('toggle').on('shown.bs.modal', function() {
                $balance = cash - toPay;
                $('#place').val($('#place-add').val()).focus();
                $('#balance').text($balance+' руб.');
            });
        }
    });
    $(document).on('hidden.bs.modal', '#order-close', function (e) {
        $('#order-cash').removeClass('disabled');
    });

    $(document).on('click', '#order-delayed', function(){
        $.post('/service/create-order',{ch_action:'order/delayed'}, function(success){
            location.pathname = location.pathname;
        });
    });
//******************************************************
    $(document).on('click', '.return-delayed', function(){
        id = $(this).val();
        $.post('/service/create-order',{ch_action:'order/return-delayed', delayedId: $(this).val()}, function(success){
            if (success == '0'){
                $('#forDelayedId').val(id);
                $('#order-delayed-return').modal('toggle');
            }
            else{
                window.location = '/'+ success;
            }
        });
    });

    $(document).on('click', '.order-delayed-return', function(){
        $.post('/service/create-order',{ch_action:'order/return-delayed', delayedId: $(this).val(), confirmation: 'true'}, function(success){
            location.pathname  = success;
        });
    });

    //*********************************************************************
//Изменение номера стола
    $(document).on('blur', '.numTable', function(){
        $.post('/service/create-order',{ch_action:'order/changetable', orderId: $(this).attr('id'), table: $(this).val()}, function(success){
            location.href = location.href;
        });
        return false;
    });

    var callback2 = function(e){
        var code = e.which ? e.which : e.keyCode;
        if(13 === code){
            $('.numTable').blur();
        }
    };

    $('.numTable').keydown(callback2);

//Фишки для заказа
    $(document).on('click', '.orderDib', function(){
        $.post('/service/create-order',{ch_action:'order/dib', orderDibId: $(this).attr('id')}, function(success){
            result = $.parseJSON(success);
            if(result.active) {
                $('#' + result.id).addClass('btn-primary').removeClass('btn-success');
            } else {
                $('#' + result.id).addClass('btn-success').removeClass('btn-primary');
            }
        });
        return false;
    });

//Фишки для товаров
    $(document).on('click', '.prodDib', function(){
        $.post('/service/create-order',{ch_action:'prod/dib', prodId: $('.prod-comment').data('id'), prodDibId: $(this).attr('id')}, function(success){
            result = $.parseJSON(success);
            if(result.active) {
                $('#' + result.id).addClass('btn-primary').removeClass('btn-success');
            } else {
                $('#' + result.id).addClass('btn-success').removeClass('btn-primary');
            }
        });
        return false;
    });




    /*$(document).on('click', '.numTable', function(){

     alert($(this).attr('id'));
     $(this).css("border-style", "1px solid green");
     $(this).removeAttribute("readonly");
     alert($(this).attr('id'));
     $.post(location.pathname,{ch_action:'order/changetable', orderId: $(this).attr('id'), table: $(this).val()}, function(success){
     });
     return false;
     });*/

    /*$(document).on('click', '#order-close-bycash-final', function() {
     var place = $('#place').val();
     if (place>0) {
     localStorage.setItem('data', '{"orderId": ' + $orderId + ', "table": "' + place + '"}');
     $.post('//barch.spbmar5srv/service/create-order',{ch_action: 'order/settable', orderId: $orderId, table: place}, function(success) {
     localStorage.setItem('success', success);
     $('#order-close-bycash').modal('toggle');
     localStorage.setItem('place', '');
     location.reload();
     });
     }
     });*/

    $(document).on('click', '#order-card', function(){
        $('#order-close-bycard').modal('toggle').on('shown.bs.modal' );
        $(this).addClass('disabled');
    });

    $(document).on('hidden.bs.modal', '#order-close-bycard', function (e) {
        $('#order-card').removeClass('disabled');
    });

    $(document).on('click','#order-close-bycard-final', function() {
        $('#order-close-bycard-final').addClass('disabled');
        var starttable = $('.starttable').val();
        var useAccount = $('#useAccount').val();
        $.post('/service/create-order',{ch_action: 'order/create', payment: 'debit', table: starttable, useAccount: useAccount}, function(success) {
            $('#order-close-bycard-final').removeClass('disabled');
            $('#order-close-bycard').toggle();
            $('#order-close').toggle();
            $('.modal-backdrop').remove();
            orderClear();
            tablesRefresh();
            //location.pathname = location.pathname;
        });
        return false;
    });

    var orderClear = function() {
        if (chbcs !== undefined) {
            chbcs.clearGuest();
        }
        $.post('/cart.html',{}, function(data) {
            $('.col-md-5.aside-right').html(data);
        });
    };

    var tablesRefresh = function() {
        //chbcs.clearGuest();
        $.post('/',{chbarch_action:"tables/refresh"}, function(data) {
            $('#footer').html(data);
        });
    };

    $(document).on('click','.flyersDateDetails', function() {
        //alert('qq');
        $.post('/service/flyer',{ch_action: 'flyersUse/details', 'dateBegin': $(this).data('date-begin'), 'dateEnd': $(this).data('date-end')}, function(success) {
            result = $.parseJSON(success);
            $('#manager-flyers-more .modal-body').html(result.html);
            //$('#spOffer-select').modal('toggle');
            $('#manager-flyers-more').modal('toggle');
        });
        return false;
    });

    /*
     $(document).on('click', '.btn-order-remove-item', function() {

     location.reload();
     });
     */

    $('.order-opened').click(function(){
        var $orderId = $(this).data('orderid');
        //alert($orderId);
    });



    $(document).on('hidden.bs.modal', '#upsale.upsale_step2', function (e) {
        $('#upsaleBtnReject').removeClass('disabled');
        $('#upsale').removeClass('upsale_step2');
    });

    //$(document).ready(upsale());

    $(document).on('click','#upsaleBtnReject', function (){
        chb.upsale();
    });

    $(document).on('click','#upsaleBtnRejectAll', function() {
        $.post('/service/upsale',{ch_action: 'upsale/cancel'}, function(success) {
            result = $.parseJSON(success);

            //$('#manager-flyers-more .modal-body').html(result.html);
            //$('#spOffer-select').modal('toggle');
            //$('#manager-flyers-more').modal('toggle');
        });
        //return false;
    });


    /*
     $('.order-opened').click(function(){
     var $orderId = $(this).data('orderid');
     console.log('$orderId='+$orderId);
     $.post('',{ch_action: 'order/details', orderId: $orderId}, function(success) {
     switch(success.status) {
     case 1:
     // Новый
     $('.aside-right .panel-heading').html('<button type="button" class="btn btn-danger order-trash disabled"><i class="fa fa-fw fa-bell-o"></i></button><div class="order-timer">Открыт '+ success.createdon +'</div>Заказ #' +success.orderId);
     break;
     case 2:
     // Оплачен
     $('.aside-right .panel-heading').html('<button type="button" class="btn btn-success order-trash disabled"><i class="fa fa-fw fa-check"></i></button><div class="order-timer">Открыт '+ success.createdon +'</div>Заказ #' +success.orderId);
     break;
     case 3:
     // Закрытый
     $('.aside-right .panel-heading').html('<button type="button" class="btn btn-success order-trash disabled"><i class="fa fa-fw fa-check"></i></button><div class="order-timer">Открыт '+ success.createdon +'</div>Заказ #' +success.orderId);
     break;
     }
     $.each(success.products, function (product_id, product_value) {
     console.log(product_value.pagetitle);
     });
     //$('.aside-right .panel-body .table > tbody').html();
     //$('.aside-right .panel-footer').html();
     console.log(success);
     }, 'json');
     });
     */

    /*
     <tr class="order-item" data-category="12">
     <td class="order-count">1</td>
     <td class="order-name">product_value</td>
     <td class="order-value">product_value</td>
     <td class="order-cost"><b>product_value руб.</b><br><small>product_value руб.</small></td>
     </tr>
     */



    /*
     * Isotope
     *
     ***********************/

//var $container = $('.isotope');
//$container.isotope({
//itemSelector: '.order-opened',
//});

    $(document).on('click', '.orderRepeatCheck', function() {
        //alert(1);
        //$('#footer').after(result.html.main);
        $(this).removeClass('repeatCheckClicked');
        $(this).addClass('repeatCheckClicked');
        $('body').append('<div class="modal fade" tabindex="-1" id="repeatCheckAccess">' +
        '<div class="modal-dialog modal-sm">' +
            '<div class="modal-content">' +
                '<div class="modal-header">' +
                    '<h3>Вы действительно хотите напечатать чек для заказа №'+$(this).attr('data-orderid')+' повторно?</h3>' +
                '</div>' +
                '<div class="modal-body">' +
                    '<div class="row">' +
                        '<div class="col-md-5 col-md-offset-1">' +
                            '<button class="repeatCheckTrue btn btn-block btn-success">Да</button>' +
                        '</div>' +
                        '<div class="col-md-5">' +
                            '<button class="repeatCheckFalse btn btn-block btn-warning">Нет</button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '</div>');


        $('#repeatCheckAccess').modal('toggle');
        return false;
    });
    $(document).on('click', '.repeatCheckTrue', function (e) {
        var orderId = $('.repeatCheckClicked').attr('data-orderid');
        $.post('/service/fiscal?ch_action=fiscal/repeatcheck',{orderId: orderId}, function(success){
            //
        });
        $('button').removeClass('repeatCheckClicked');
        $('#repeatCheckAccess').modal('hide');
    });
    $(document).on('click', '.repeatCheckFalse', function (e) {
        $('#repeatCheckAccess').modal('hide');
    });
    $(document).on('hidden.bs.modal', '#repeatCheckAccess', function (e) {
        $('button').removeClass('repeatCheckClicked');
        $('#repeatCheckAccess').remove();
    });


    $(document).on('click', '.buyProduct', function () {
        var productId = $(this).attr('data-product-id');

        if(!$(this).attr('data-18-only')) {
            //document.getElementById('ms2form_' + productId).submit();
            options = {};
            if ($(this).attr('data-no-discount') !== '') options.nodiscount = 1;
            options = JSON.stringify(options);

            chb.cartAction('add', {id:productId,count:"1",options: options});

            //$.post('/cart.html',{id:productId,count:"1",options: options,ms2_action:"cart/add"}, function(data) {
            //    $('.col-md-5.aside-right').html(data);
            //    upsale();
            //});
        } else {
            $('.buyProduct').removeClass('need_to_acess_18_only');
            $(this).addClass('need_to_acess_18_only');
            $('body').append('<div class="modal fade" tabindex="-1" id="access_18_only">' +
            '<div class="modal-dialog modal-sm">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<h3>Посетителю есть 18 лет?</h3>' +
            '</div>' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="col-md-5 col-md-offset-1">' +
            '<button class="access_18_only_true btn btn-block btn-success">Да</button>' +
            '</div>' +
            '<div class="col-md-5">' +
            '<button class="access_18_only_false btn btn-block btn-warning">Нет</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');

            $('#access_18_only').modal('toggle');
            return false;
        }
    });

    $(document).on('click', '.upsaleAddProduct', function() {
        $(this).children('input').each(function(){
            if($(this).attr('name') == 'id') {
                var productId = $(this).attr('value');
                $.post('/cart.html',{id:productId,count:"1",options:"[]",ms2_action:"cart/add", byUpsale:"1"}, function(data) {
                    $('.col-md-5.aside-right').html(data);
                });
            }
        });
        $(this).children('.btn-primary').addClass('disabled');
        $(this).removeClass('upsaleAddProduct');

        return false;
    });

    $(document).on('click', '.access_18_only_true', function (e) {
        var productId = $('.need_to_acess_18_only').attr('data-product-id');
        //document.getElementById('ms2form_' + productId).submit();
        $.post('/cart.html',{id:productId,count:"1",options:"[]",ms2_action:"cart/add"}, function(data) {
            $('.col-md-5.aside-right').html(data);
        });
        $('button').removeClass('need_to_acess_18_only');
        $('#access_18_only').modal('hide');
    });
    $(document).on('click', '.access_18_only_false', function (e) {
        $('button').removeClass('need_to_acess_18_only');
        $('#access_18_only').modal('hide');
    });
    $(document).on('hidden.bs.modal', '#access_18_only', function (e) {
        $('button').removeClass('need_to_acess_18_only');
        $('#access_18_only').remove();
    });


    $(document).on('click','.customGoodButton', function(button){
        console.log(button.target);
        console.log($(button.target));
        console.log('goodit: '+$(button.target).data('goodit'));
        $.post('/service/create-order',{ch_action: 'customGood/getInfo', goodIT: $(button.target).data('goodit')}, function(success){
            result = $.parseJSON(success);
            if (result.success) {
                $('#customGood-close .modal-title').html(result.data.goodName);
                $('#customGoodIT').val(result.data.goodIT);
                $('#customGood-close').modal('show');
            }
        });
        return false;
    });


    $(document).on('click','#customGood-cash-button', function(button){
        var cash = parseInt($('#customGood-cash-input').val(),10);
        if ($('#customGood-cash-input').val().length == 0) cash = 0;
        console.log('cash: '+cash);
        if (cash >= 0) {
            $(this).addClass('disabled');
            $.post('/service/create-order',{ch_action: 'customGood/sell', goodIT: $('#customGoodIT').val(), payment: 'cash', take_sum: cash, goodSum: cash}, function(success) {
                var obj = jQuery.parseJSON(success);
                $orderId = obj.data.msorder;
                orderClear();
                tablesRefresh();
            });

            $('#customGood-close').modal('hide');
            $('#order-close-bycash').modal('toggle').on('shown.bs.modal', function() {
                $balance = cash - toPay;
                $('#place').val($('#place-add').val()).focus();
                $('#balance').text('0 руб.');
            });
        }
    });

    //=========================Добавление товара из помощника =========================
    $(document).on('click', '.btn-helper-add-to-cart', function(){
        productid = $(this).attr('product-id');
        chb.cartAction('add', {id:productid,count:"1",options: ''},1);
    });


})}(window.jQuery);

function digitalWatch() {
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (hours < 10) hours = "0" + hours;
    if (minutes < 10) minutes = "0" + minutes;
    if (seconds < 10) seconds = "0" + seconds;
    document.getElementById("digital_watch").innerHTML = hours + ":" + minutes + ":" + seconds;
    setTimeout("digitalWatch()", 1000);
}
window.onload=digitalWatch;



// ==========================================
// ==========================================
// ==========================================
// ==========================================
// =========                        =========
// =========         SOLUS          =========
// =========                        =========
// ==========================================
// ==========================================
// ==========================================


$(document).ready(function(){

    $(document).on('click', '#pdopage .thumbnail', function(){
        $(this).parent().children(".full-item").show('slow');
    });
    $(document).on('click', '.closefullitem', function(){
        $(this).parent().hide('slow');
    });


});