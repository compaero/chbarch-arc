<div class="panel-heading no-padding">
    <div class="sort">
        [[!chbarchDetectCorrectParents:toPlaceholder=`parents`? &dst=`submenu` &fromPost=`1`]]
        [[!pdoMenu?
        &tplOuter=`@INLINE <ul[[+classes]]>[[+wrapper]]</ul>`
        &tpl=`@INLINE <li[[+classes]]><button type="button" class="btn btn-lg btn-success btn-categories subcategory" data-id="[[+id]]">[[+menutitle]]</button></li>`
        &tplHere=`@INLINE <li[[+classes]]><button type="button" class="btn btn-lg btn-info btn-categories subcategory" data-id="[[+id]]">[[+menutitle]]</button></li>`
        &parents=`[[+parents]]`
        &templates=`2,4`
        &limit=`0`
        &level=`1`
        &outerClass=`nav nav-pills`
        &firstClass=``
        &lastClass=``
        ]]
    </div>
</div>
<div class="panel-body" id="pdopage">
    [[!chbarchDetectCorrectParents:toPlaceholder=`cat`? &dst=`catalog` &fromPost=`1`]]
    <div class="metro menu-list rows [[+page.nav:isempty=``:else=`pagination-show`]]">
        [[!pdoPage?
        &element=`pdoResources`
        &tpl = `tpl.chbarch.menuItem`
        &limit=`15`
        &parents=`[[+cat]]`
        &resources=`[[+resources]]`
        &tplOperator=`==`
        &leftJoin=`{"msProductData": {"class":"msProductData","on": "msProductData.id = modResource.id"}}`
        &select = `{"modResource":"*","msProductData":"msProductData.price"}`
        &tplCondition = `template`
        &conditionalTpls = `{"0":"tpl.chbarch.menuItem","[[++chbarch_template_product_complex]]":"tpl.chbarch.menuItemComplex","[[++chbarch_template_product]]":"tpl.chbarch.menuItem","[[++chbarch_template_product_special]]":"tpl.chbarch.menuItemSpecial","[[++chbarch_template_product_super]]":"tpl.chbarch.menuItem.sprod","[[++chbarch_template_product_supertea]]":"tpl.chbarch.menuItem.stea"}`
        &templates=`0,[[++chbarch_template_product_complex]],[[++chbarch_template_product]],[[++chbarch_template_product_special]],[[++chbarch_template_product_super]],[[++chbarch_template_product_supertea]]`
        &depth=`[[+depth]]`
        &prepareTVs=`0`
        &includeTVs=`chbarch_ProductSingle, chbarch_ProductDouble, chbarch_ProductSingleOut, chbarch_ProductDoubleOut, chbarch_ProductSmall, chbarch_ProductMedium, chbarch_ProductLarge, chbarch_18_only, chbarch_no_discount`

        &tplPageWrapper=`@INLINE <div class="btn-group btn-group-lg btn-group-justified pagination">[[+first]][[+prev]][[+pages]][[+next]][[+last]]</div>`
        &tplPage=`@INLINE <a href="[[+href]]" class="btn btn-success btn-categories subcategory" data-id="[[+cat]]">[[+pageNo]]</a>`
        &tplPageActive=`@INLINE <a href="[[+href]]" class="btn btn-sm btn-info active btn-categories subcategory" data-id="[[+cat]]">[[+pageNo]]</a>`
        &tplPageFirst=`@INLINE <!--<a href="[[+href]]" class="btn btn-success">В начало</a>-->`
        &tplPageLast=`@INLINE <!--<a href="[[+href]]" class="btn btn-success">В конец</a>-->`
        &tplPagePrev=`@INLINE <!--<a href="[[+href]]" class="btn btn-sm btn-success">&laquo;</a>-->`
        &tplPageNext=`@INLINE <!--<a href="[[+href]]" class="btn btn-sm btn-success">&raquo;</a>-->`
        &tplPageSkip=`@INLINE <!--<a class="btn btn-sm btn-success disabled">...</a>-->`
        &tplPageFirstEmpty=`@INLINE <!--<a class="btn btn-success disabled">В начало</a>-->`
        &tplPageLastEmpty=`@INLINE <!--<a class="btn btn-success disabled">В конец</a>-->`
        &tplPagePrevEmpty=`@INLINE <!--<a class="btn btn-success disabled">&laquo;</a>-->`
        &tplPageNextEmpty=`@INLINE <!--<a class="btn btn-success disabled">&raquo;</a>-->`
        &pageNavScheme=`h`
        &sortby=`[[#[[!+cat]].ch.categorySortDir]]`
        &sortdir=`ASC`
        ]]
    </div>
</div>
<div class="panel-footer no-padding" id="pdopagepag">
    [[+page.nav]]
</div>