<?php
/** @noinspection PhpIncludeInspection */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CONNECTORS_PATH . 'index.php';
/** @var chBarch $chBarch */
$chBarch = $modx->getService('chbarch', 'chBarch', $modx->getOption('chbarch_core_path', null, $modx->getOption('core_path') . 'components/chbarch/') . 'model/chbarch/');
$modx->lexicon->load('chbarch:default');

// handle request
$corePath = $modx->getOption('chbarch_core_path', null, $modx->getOption('core_path') . 'components/chbarch/');
$path = $modx->getOption('processorsPath', $chBarch->config, $corePath . 'processors/');
$modx->request->handleRequest(array(
	'processors_path' => $path,
	'location' => '',
));