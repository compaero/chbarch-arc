chBarch.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateful: true,
		 stateId: 'chbarch-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
		 */
		hideMode: 'offsets',
		items: [{
			html: '<h2>' + _('chbarch') + '</h2>',
			cls: '',
			style: {margin: '15px 0'}
		}, {
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: true,
			hideMode: 'offsets',
			items: [{
				title: _('chbarch_items'),
				layout: 'anchor',
				items: [{
					html: _('chbarch_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'chbarch-grid-items',
					cls: 'main-wrapper',
				}]
			}]
		}]
	});
	chBarch.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(chBarch.panel.Home, MODx.Panel);
Ext.reg('chbarch-panel-home', chBarch.panel.Home);
