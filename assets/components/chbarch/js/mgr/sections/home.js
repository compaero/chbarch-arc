chBarch.page.Home = function (config) {
	config = config || {};
	Ext.applyIf(config, {
		components: [{
			xtype: 'chbarch-panel-home', renderTo: 'chbarch-panel-home-div'
		}]
	});
	chBarch.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(chBarch.page.Home, MODx.Component);
Ext.reg('chbarch-page-home', chBarch.page.Home);