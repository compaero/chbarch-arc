<?php
/**
 * Created by mvoevodskiy
 * Date: 07.05.15
 * Time: 18:27
 */

class chOrderHandler extends msOrderHandler {

    public $defaultReceiver = 'Guest';





//    /* @inheritdoc} */
//    public function add($id, $count = 1, $options = array(), $price = null) {
//        if (empty($id) || !is_numeric($id)) {
//            return $this->error('ms2_cart_add_err_id');
//        }
//        $count = intval($count);
//
//        $filter = array('id' => $id);
//        if (!$this->config['allow_deleted']) {$filter['deleted'] = 0;}
//        if (!$this->config['allow_unpublished']) {$filter['published'] = 1;}
//        /* @var msProduct $product */
//        if ($product = $this->modx->getObject('modResource', $filter)) {
//            if (!($product instanceof msProduct)) {
//                return $this->error('ms2_cart_add_err_product', $this->status());
//            }
//            if ($count > $this->config['max_count'] || $count <= 0) {
//                return $this->error('ms2_cart_add_err_count', $this->status(), array('count' => $count));
//            }
//
//            /* You can prevent add of product to cart by adding some text to $modx->event->_output
//              <?php
//                      if ($modx->event->name = 'msOnBeforeAddToCart') {
//                         $modx->event->output('Error');
//                    }
//
//             * Also you can modify $count and $options variables by add values to $this->modx->event->returnedValues
//                <?php
//                      if ($modx->event->name = 'msOnBeforeAddToCart') {
//                        $values = & $modx->event->returnedValues;
//                        $values['count'] = $count + 10;
//                        $values['options'] = array('size' => '99');
//                    }
//             *
//             * */
//
//            $response = $this->ms2->invokeEvent('msOnBeforeAddToCart', array(
//                'product' => $product,
//                'count' => $count,
//                'options' => $options,
//                'cart' => $this
//            ));
//            if (!($response['success'])) {
//                return $this->error($response['message']);
//            }
////            $price = (int) $price;
////            if ($price === null || ($price < 1)) {
////                $price = $product->getPrice(); // We can modify price by snippet specified in system setting "ms2_price_snippet"
////            }
//            $weight = $product->getWeight(); // And weight by "ms2_weight_snippet"
//            $count = $response['data']['count'];
//            $options = $response['data']['options'];
//
//            $key = md5($id.$price.$weight.($this->modx->toJSON($options)));
//            if (array_key_exists($key, $this->cart)) {
//                return $this->change($key, $this->cart[$key]['count'] + $count);
//            }
//            else {
//                $this->cart[$key] = array(
//                    'id' => $id
//                ,'price' => $price
//                ,'weight' => $weight
//                ,'count' => $count
//                ,'options' => $options
//                ,'ctx' => $this->modx->context->get('key')
//                );
//                $response = $this->ms2->invokeEvent('msOnAddToCart', array('key' => $key, 'cart' => $this));
//                if (!$response['success']) {return $this->error($response['message']);}
//
//                return $this->success('ms2_cart_add_success', $this->status(array('key' => $key)), array('count' => $count));
//            }
//        }
//
//        return $this->error('ms2_cart_add_err_nf', $this->status());
//    }

    public function validate($key, $value) {
        if ($key == 'receiver') {
            $response = $this->ms2->invokeEvent('msOnBeforeValidateOrderValue', array(
                'key' => $key,
                'value' => $value,
            ));
            $value = $response['data']['value'];

            // Transforms string from "nikolaj -  coster--Waldau jr." to "Nikolaj Coster-Waldau Jr."
//            $tmp = preg_replace(array('/[^-a-zа-яёЁ\s\.]/iu', '/\s+/', '/\-+/', '/\.+/'), array('', ' ', '-', '.'), $value);
//            $tmp = preg_split('/\s/', $tmp, -1, PREG_SPLIT_NO_EMPTY);
//            $tmp = array_map(array($this, 'ucfirst'), $tmp);
//            $value = preg_replace('/\s+/', ' ', implode(' ', $tmp));
            if (empty($value)) {$value = $this->defaultReceiver;}

            $response = $this->ms2->invokeEvent('msOnValidateOrderValue', array(
                'key' => $key,
                'value' => $value,
            ));
            $value = $response['data']['value'];

            return $value;
        } else {
            return parent::validate($key, $value);
        }
    }


    /** @inheritdoc} */
    public function submit($data = array()) {
        /*
         * Reset alien order cost
         */
        unset($data['cost']);
        unset($this->order['cost']);

        $response = $this->ms2->invokeEvent('msOnSubmitOrder', array(
            'data' => $data,
            'order' => $this
        ));

        if (!$response['success']) {return $this->error($response['message']);}
        if (!empty($response['data']['data'])) {$this->set($response['data']['data']);}

        $response = $this->getDeliveryRequiresFields();
        if ($this->ms2->config['json_response']) {
            $response = $this->modx->fromJSON($response);
        }
        $requires = $response['data']['requires'];

        $errors = array();
        foreach ($requires as $v) {
            if (!empty($v) && empty($this->order[$v])) {
                $errors[] = $v;
            }
        }
        if (!empty($errors)) {
            return $this->error('ms2_order_err_requires', $errors);
        }

        $user_id = $this->ms2->getCustomerId();
        $cart_status = $this->ms2->cart->status();
        $delivery_cost = $this->getCost(false, true);
        if (isset($_SESSION['chGuestAccounts']) and isset($_SESSION['chGuestAccounts']['cost'])) {
            $order_cost = $_SESSION['chGuestAccounts']['cost'];
            $this->order['properties'] = $_SESSION['chGuestAccounts']['properties'];
        } else {
            $order_cost = $cart_status['total_cost'];
        }
        /** @TODO КОСТЫЛЬ. Изменить присвеоние типа оплаты при нулевой стоимости заказа */
        if ($order_cost === 0) $this->order['payment'] = 5;
        $createdon = date('Y-m-d H:i:s');
        /* @var msOrder $order */
        $order = $this->modx->newObject('msOrder');
        $order->fromArray(array(
            'user_id' => $user_id
            ,'createdon' => $createdon
            ,'num' => $this->getnum()
            ,'delivery' => $this->order['delivery']
            ,'payment' => $this->order['payment']
            ,'cart_cost' => $cart_status['total_cost']
            ,'weight' => $cart_status['total_weight']
            ,'delivery_cost' => $delivery_cost
            /*
             * original order cost: $cart_status['total_cost'] + $delivery_cost
             * original status: 0
             */
            ,'cost' => $order_cost
            //,'cost' => $cart_status['total_cost'] + $delivery_cost
            ,'status' => 1
            ,'context' => $this->ms2->config['ctx']
        ));

        $this->modx->log(1,'msOrder toArray(): '.print_r($order->toArray(),1));

        // Adding address
        /* @var msOrderAddress $address */
        $address = $this->modx->newObject('msOrderAddress');
        $address->fromArray(array_merge($this->order,array(
            'user_id' => $user_id
        ,'createdon' => $createdon
        )));
        $order->addOne($address);

        // Adding products
        $cart = $this->ms2->cart->get();
        $products = array();
        foreach ($cart as $v) {
            if ($tmp = $this->modx->getObject('msProduct', $v['id'])) {
                $name = $tmp->get('pagetitle');
            }
            else {
                $name = '';
            }
            /* @var msOrderProduct $product */
            $product = $this->modx->newObject('msOrderProduct');
            $product->fromArray(array_merge($v, array(
                'product_id' => $v['id']
            ,'name' => $name
            ,'cost' => $v['price'] * $v['count']
            )));
            $products[] = $product;
        }
        $order->addMany($products);

        $response = $this->ms2->invokeEvent('msOnBeforeCreateOrder', array(
            'msOrder' => $order,
            'order' => $this
        ));
        if (!$response['success']) {return $this->error($response['message']);}

        if ($order->save()) {
            $response = $this->ms2->invokeEvent('msOnCreateOrder', array(
                'msOrder' => $order,
                'order' => $this
            ));
            if (!$response['success']) {return $this->error($response['message']);}

            $this->ms2->cart->clean();
            $this->clean();
            if (empty($_SESSION['minishop2']['orders'])) {
                $_SESSION['minishop2']['orders'] = array();
            }
            $_SESSION['minishop2']['orders'][] = $order->get('id');

            return $this->success('', array('msorder' => $order->get('id')));

//            // Trying to set status "new"
//            $response = $this->ms2->changeOrderStatus($order->get('id'), 1);
//            if ($response !== true) {
//                return $this->error($response, array('msorder' => $order->get('id')));
//            }
//            /* @var msPayment $payment*/
//            elseif ($payment = $this->modx->getObject('msPayment', array('id' => $order->get('payment'), 'active' => 1))) {
//                $response = $payment->send($order);
//                if ($this->config['json_response']) {
//                    @session_write_close();
//                    exit(is_array($response) ? $this->modx->toJSON($response) : $response);
//                }
//                else {
//                    if (!empty($response['data']['redirect'])) {
//                        $this->modx->sendRedirect($response['data']['redirect']);
//                        exit();
//                    }
//                    elseif (!empty($response['data']['msorder'])) {
//                        $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id, array('msorder' => $response['data']['msorder'])));
//                        exit();
//                    }
//                    else {
//                        $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id));
//                        exit();
//                    }
//                }
//            }
//            else {
//                if ($this->ms2->config['json_response']) {
//                    return $this->success('', array('msorder' => $order->get('id')));
//                }
//                else {
//                    $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id, array('msorder' => $response['data']['msorder'])));
//                    exit();
//                }
//            }
        }
        return $this->error();
    }


//    function submit($data=array()) {
//
//        $this->modx->log(1, 'custom order class');
//        $response = parent::submit($data);
//        if (!is_array($response)) $response = json_decode($response,1);
//        if ($response['success']) {
//            $sessionGuestVar = $this->config['sessionGuestVar'];
//            if (isset($_SESSION[$sessionGuestVar])) {
//                $sessionGuest = $_SESSION[$sessionGuestVar];
//                if ($guest = $this->modx->getObject('chGuest', array('username'=>$sessionGuest['username']))) {
//                    $order = $this->modx->getObject('msOrder',$response['data']['msOrder']);
//                    $order->Address->set('',$guest->get('username'));
//                    $order->Address->save();
//                }
//
//            }
//
//
//        } else {
//            return $this->ms2->error($response['message'], $response['data']);
//        }
//        $this->modx->log(1, $response);
//        return $response;
//    }
}