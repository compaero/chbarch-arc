<?php

/**
 * The home manager controller for chBarch.
 *
 */
class chBarchHomeManagerController extends chBarchMainController {
	/* @var chBarch $chBarch */
	public $chBarch;


	/**
	 * @param array $scriptProperties
	 */
	public function process(array $scriptProperties = array()) {
	}


	/**
	 * @return null|string
	 */
	public function getPageTitle() {
		return $this->modx->lexicon('chbarch');
	}


	/**
	 * @return void
	 */
	public function loadCustomCssJs() {
		$this->addCss($this->chBarch->config['cssUrl'] . 'mgr/main.css');
		$this->addCss($this->chBarch->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
		$this->addJavascript($this->chBarch->config['jsUrl'] . 'mgr/misc/utils.js');
		$this->addJavascript($this->chBarch->config['jsUrl'] . 'mgr/widgets/items.grid.js');
		$this->addJavascript($this->chBarch->config['jsUrl'] . 'mgr/widgets/items.windows.js');
		$this->addJavascript($this->chBarch->config['jsUrl'] . 'mgr/widgets/home.panel.js');
		$this->addJavascript($this->chBarch->config['jsUrl'] . 'mgr/sections/home.js');
		$this->addHtml('<script type="text/javascript">
		Ext.onReady(function() {
			MODx.load({ xtype: "chbarch-page-home"});
		});
		</script>');
	}


	/**
	 * @return string
	 */
	public function getTemplateFile() {
		return $this->chBarch->config['templatesPath'] . 'home.tpl';
	}
}