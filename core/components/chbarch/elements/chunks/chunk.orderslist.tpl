
    <div class="col-md-3 order-opened" data-orderid="[[+orderId]]" onClick="">
        <div class="thumbnail row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">Заказ: [[+orderId]]</div>
                    <div class="col-md-6"><span class="pull-right">
                        [[+status:ne=`3`:then=`[[+createdon:dateAgo]]`:else=`[[+createdon:strtotime:date=`%R %d.%m.%y`]]`]]
                    </span></div>
                </div>

                <p>Номер стола: [[+status:is=`3`:then=`[[+table]]`:else=`<input type="text" class="numTable" id="[[+orderId]]" value="[[+table]]" style="border-style: none; width: 100px">`]]</p>
                <small>
                    <table class="table table-condensed">
                        [[+productsList]]
                    </table>
                </small>
                [[+status:eq=`1`:then=`<div class="alert alert-danger">Чек не напечатан!</div>`]]
                <div class="row">
                    <div class="col-md-4">
                        <form method="post">
                            <input type="hidden" name="orderId" value="[[+orderId]]">
                            <input type="hidden" name="status" value="3">
                            [[+status:ne=`3`:then=`<button type="submit" class="btn btn-success btn-lg btn-block" name="ch_action" value="order/changestatus">Закрыть</button>`]]
                        </form>
                    </div>
                    <div class="col-md-4">
                        [[+status:ne=`3`:then=`<button data-orderid="[[+orderId]]" class="orderRepeatCheck btn btn-warning btn-lg btn-block"><i class="fa fa-repeat"></i> Чек</button>`]]
                    </div>
                    <div class="col-md-4">
                        <div class="item-cost btn btn-block disabled" style="position:relative;">[[+cost]] <sub><i class="fa fa-rub"></i></sub></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
