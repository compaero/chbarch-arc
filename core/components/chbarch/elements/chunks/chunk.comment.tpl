<hr>
<fieldset>
    <legend>Комментарии к заказу</legend>
    <div class="form-group">
        <textarea data-id="[[+id]]" class="form-control [[+class-comment]]" rows="6" placeholder="Комментариев нет…">[[+comment]]</textarea>
    </div>
</fieldset>