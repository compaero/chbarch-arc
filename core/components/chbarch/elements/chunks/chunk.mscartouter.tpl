<article class="panel panel-default order">
	<div class="panel-heading">
		<form method="post">
			<button type="submit" class="btn btn-danger order-trash" name="ms2_action" value="cart/clean">
				<i class="fa <fa-></fa->fw fa-trash-o"></i>
			</button>
		</form>
		Открыт [[!dateAgo? &input=`[[!chbarchTimeFirstAddToCart]]`]]
		<!--<div class="order-timer"></div>
		&nbsp;-->
	</div>
	<div class="panel-body no-padding">
		<div class="table table-hover table-bordered">
			[[+goods]]
		</div>
	</div>
	<div class="panel-footer no-padding">
		<div class="btn-group btn-group-justified cardmenu">
			<div class="btn-group">
				<button type="button" class="btn btn-warning btn-lg" id="btn-flyer-select"><i class="fa fa-ticket"></i> Флаер</button>
			</div>
			<div class="btn-group">
				<button type="button" class="btn btn-primary btn-lg" id="btn-order-additional"><i class="fa fa-plus"></i> Дополнительно</button>
			</div>
		</div>
        [[!chFlyer.inCart]]
		<!--
			<div class="input-group input-group-lg">
			  <span class="input-group-addon">Номер столика:</span>
			  <input type="text" step="1" min="1" class="form-control input-lg" id="place-add" placeholder="…">
			</div>
		-->

		<div class="input-group input-group-lg">
			</div>
	</div>
</article>
<div class="order-result" id="order-result" data-cost="[[+total_cost]]">
	<button type="button" class="btn btn-success btn-block btn-lg-extra" id="btn-order-close">[[+total_cost]] руб.</button>
</div>