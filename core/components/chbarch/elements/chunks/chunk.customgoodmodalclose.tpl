<div class="modal fade" tabindex="-1" id="customGood-close">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">

                <p>Если Вы уже начали создавать новый заказ, он будет перемещен в "Отложенные"</p>

				<div class="form-group">
					<!--p>Введите полученную сумму</p-->
					<input type="text" step="1" min="0" class="form-control input-lg" id="customGood-cash-input" name="cash" placeholder="Вносимая сумма">
                    <input type="hidden" id="customGoodIT" value="">
				</div>
                <hr>
                <br>
				<button type="button" class="btn btn-success btn-lg btn-block" id="customGood-cash-button" name="ch_action" value="order/create">
					Оплата наличными
				</button>
				<input type="hidden" name="payment" value="cash">

                <br>

				<input type="hidden" name="payment" value="debit">
				<button type="button" class="btn btn-warning btn-lg btn-block" id="customGood-card-button" name="ch_action" value="order/create">
					Оплата по карте
				</button>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Отмена</button>
			</div>
		</div>
	</div>
</div>