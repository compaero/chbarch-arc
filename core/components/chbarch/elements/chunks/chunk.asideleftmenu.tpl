<aside class="col-lg-2 aside-left">
	[[pdoMenu?
	&as_target=`.aside-left`
	&level=`1`
	&parents=`[[+parents]]`
	&context=`web`
	&firstClass=`none`
	&lastClass=`none`
	&outerClass=`nav nav-pills nav-stacked submenu`
	&checkPermissions=`load`
	&tpl=`@INLINE <li[[+classes]]><button type="button" class="[[+addClass]] btn btn-lg btn-primary btn-categories category" data-id="[[+id]]" style="width:100%; text-align:left;"><span class="topicon [[+link_attributes]]"></span><span class="hiddentxt">[[+menutitle]]</span></button></li>`
	&tplHere=`@INLINE <li[[+classes]]><button type="button" class="btn btn-lg btn-info btn-categories category" data-id="[[+id]]" style="width:100%; text-align:left;"><span class="topicon [[+link_attributes]]"></span><span class="hiddentxt">[[+menutitle]]</span></button></li>`
	]]
</aside>