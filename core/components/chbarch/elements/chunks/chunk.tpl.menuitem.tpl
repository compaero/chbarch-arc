<div class="col-sm-3 col-md-3 col-lg-3 metro-item">
	<div class="thumbnail buyProduct" data-product-id="[[+id]]" data-product-it="[[+introtext]]" data-18-only="[[+tv.chbarch_18_only]][[#[[+parent]].chbarch_18_only]]" data-no-discount="[[+tv.chbarch_no_discount]][[#[[+parent]].chbarch_no_discount]]">
		<form method="post" class="ms2_form" id="ms2form_[[+id]]">
			<h4 class="item-title">[[+menutitle:default=`[[+pagetitle]]`]]</h4>
			<!--small>[[+description]]</small-->
			<div class="item-cost">[[chbarchTrimzeros? &input=`[[+price]]`]]</div>
			<input type="hidden" name="id" value="[[+id]]">
			<input type="hidden" name="count" value="1">
			<input type="hidden" name="ms2_action" value="cart/add">
			<!--button class="btn btn-default" type="submit" >Добавить</button-->
		</form>
	</div>
</div>