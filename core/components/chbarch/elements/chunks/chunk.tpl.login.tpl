<div id="login">
	<div class="panel panel-default">
		<div class="panel-heading">Авторизация</div>
		<div class="panel-body">
			<form action="/" method="post" id="auth">
				<div class="form-group">
					<input type="text" class="form-control" name="username" placeholder="Имя пользователя" autocomplete="off" autofocus required>
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="Пароль" autocomplete="off" required>
				</div>
				<input class="loginLoginValue" type="hidden" name="service" value="login" />
			</form>
		</div>
		<div class="panel-footer no-padding">
			<button class="btn btn-success btn-block btn-lg" type="submit" form="auth">Вход</button>
		</div>
	</div>
</div>