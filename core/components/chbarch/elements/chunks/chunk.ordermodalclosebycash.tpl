<div class="modal fade" tabindex="-1" id="order-close-bycash">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Оплата наличными</h4>
			</div>
			<div class="modal-body">
				Сдача: <b id="balance">0 руб.</b>
				<hr>
                <button class="btn btn-success btn-lg btn-block" id="order-close-bycash-final" data-dismiss="modal">
                    Закрыть окно
                </button>
			</div>
		</div>
	</div>
</div>