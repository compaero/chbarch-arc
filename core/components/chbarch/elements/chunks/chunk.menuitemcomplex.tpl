<div class="col-md-4 metro-item">
    <div class="thumbnail complex" data-product-it="[[+introtext]]" data-18-only="[[!+chbarch_18_only]][[!*chbarch_18_only]]">
        <form method="post" class="ms2_form" id="ms2form_[[+id]]">
            <h4 class="item-title">[[+pagetitle]]</h4>
            <!--small>[[+description]]</small-->
            <div class="item-cost">[[chbarchTrimzeros? &input=`[[+price]]`]]</div>
            <input type="hidden" name="id" value="[[+id]]">
            <input type="hidden" name="count" value="1">
            <input type="hidden" name="options" value="[]">
            <input type="hidden" name="ms2_action" value="cart/add">
            <!--button class="btn btn-default" type="submit" >Добавить</button-->
        </form>
    </div>
</div>
