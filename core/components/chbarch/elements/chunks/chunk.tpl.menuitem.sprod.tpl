<div class="col-sm-3 col-lg-3 col-md-3 metro-item">
    <div class="thumbnail">
        <h4 class="item-title">[[+pagetitle]]</h4>
        <div class="item-cost">[[#[[+tv.chbarch_ProductSingle]].price]]</div>
    </div>
    <div class="full-item">
        <h4 class="item-title">[[+pagetitle]]</h4>
        <div class="row">
            <div class="col-md-6">
                <form method="post" class="ms2_form chForm btn-block" style="margin:0" id="ms2form_[[+tv.chbarch_ProductSingle]]">
                    <input type="hidden" name="count" value="1">
                    <input type="hidden" name="options" value="[]">
                    <input type="hidden" name="id" value="[[+tv.chbarch_ProductSingle]]">
                    <input type="hidden" name="ms2_action" value="cart/add">
                    <button data-product-id="[[+tv.chbarch_ProductSingle]]" data-18-only="[[!+chbarch_18_only]][[!*chbarch_18_only]]" class="buyProduct btn btn-success btn-block" type="submit">
                        <img src='/assets/components/chbarch/src/src/images/single1.png'>
                        <div class="item-cost">[[#[[+tv.chbarch_ProductSingle]].price]]</div>
                    </button>
                </form>
            </div>
            <div class="col-md-6">
                <form method="post" class="ms2_form chForm btn-block" style="margin:0" id="ms2form_[[+tv.chbarch_ProductSingleOut]]">
                    <input type="hidden" name="count" value="1">
                    <input type="hidden" name="options" value="[]">
                    <input type="hidden" name="id" value="[[+tv.chbarch_ProductSingleOut]]">
                    <input type="hidden" name="ms2_action" value="cart/add">
                    <button data-product-id="[[+tv.chbarch_ProductSingleOut]]" data-18-only="" class="buyProduct btn btn-success btn-block" type="submit">
                        <img src='/assets/components/chbarch/src/src/images/single2.png'>
                        <div class="item-cost">[[#[[+tv.chbarch_ProductSingleOut]].price]]</div>
                    </button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <form method="post" class="ms2_form chForm btn-block" style="margin:0" id="ms2form_[[+tv.chbarch_ProductDouble]]">
                    <input type="hidden" name="count" value="1">
                    <input type="hidden" name="options" value="[]">
                    <input type="hidden" name="id" value="[[+tv.chbarch_ProductDouble]]">
                    <input type="hidden" name="ms2_action" value="cart/add">
                    <button data-product-id="[[+tv.chbarch_ProductDouble]]" data-18-only="" class="buyProduct btn btn-success btn-block" type="submit">
                        <img src='/assets/components/chbarch/src/src/images/double1.png'>
                        <div class="item-cost">[[#[[+tv.chbarch_ProductDouble]].price]]</div>
                    </button>
                </form>
            </div>

            <div class="col-md-6">
                <form method="post" class="ms2_form chForm btn-block" id="ms2form_[[+tv.chbarch_ProductDoubleOut]]">
                    <input type="hidden" name="count" value="1">
                    <input type="hidden" name="options" value="[]">
                    <input type="hidden" name="id" value="[[+tv.chbarch_ProductDoubleOut]]">
                    <input type="hidden" name="ms2_action" value="cart/add">
                    <button data-product-id="[[+tv.chbarch_ProductDoubleOut]]" data-18-only="" class="buyProduct btn btn-success btn-block" type="submit">
                        <img src='/assets/components/chbarch/src/src/images/double2.png'>
                        <div class="item-cost">[[#[[+tv.chbarch_ProductDoubleOut]].price]]</div>
                    </button>
                </form>
            </div>
        </div>
        <div class="closefullitem"></div>
    </div>
</div>

<!--
<div class="col-md-4 metro-item">
    <div class="thumbnail">
    <h4 class="item-title">[[+menutitle:default=`[[+pagetitle]]`]]</h4>
    <div class="row">
        <div class="col-md-12" style="padding-bottom: 4px;">
            <form method="post" class="chForm btn-block" style="margin:0" id="ms2form_[[+tv.chbarch_ProductSingle]]">
                <input type="hidden" name="count" value="1">
                <input type="hidden" name="options" value="[]">
                <input type="hidden" name="id" value="[[+tv.chbarch_ProductSingle]]">
                <input type="hidden" name="ms2_action" value="cart/add">
                <button data-product-id="[[+tv.chbarch_ProductSingle]]" data-18-only="[[!+chbarch_18_only]][[!*chbarch_18_only]]" class="buyProduct btn btn-success btn-block" type="submit" >Сингл
                    [[#[[+tv.chbarch_ProductSingle]].price]]
                </button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4" style="padding-right:2px;">
            <form method="post" class="chForm btn-block" style="margin:0" id="ms2form_[[+tv.chbarch_ProductDouble]]">
                <input type="hidden" name="count" value="1">
                <input type="hidden" name="options" value="[]">
                <input type="hidden" name="id" value="[[+tv.chbarch_ProductDouble]]">
                <input type="hidden" name="ms2_action" value="cart/add">
                <button data-product-id="[[+tv.chbarch_ProductDouble]]" data-18-only="[[!+tv.chbarch_18_only]][[!*chbarch_18_only]]" class="buyProduct btn btn-success btn-block" type="submit">Дабл
                    [[#[[+tv.chbarch_ProductDouble]].price]]
                </button>
            </form>
        </div>
        <div class="col-md-4" style="padding-right:2px;padding-left:2px;">
            <form method="post" class="chForm btn-block" style="margin:0" id="ms2form_[[+tv.chbarch_ProductSingleOut]]">
                <input type="hidden" name="count" value="1">
                <input type="hidden" name="options" value="[]">
                <input type="hidden" name="id" value="[[+tv.chbarch_ProductSingleOut]]">
                <input type="hidden" name="ms2_action" value="cart/add">
                <button data-product-id="[[+tv.chbarch_ProductSingleOut]]" data-18-only="[[!+chbarch_18_only]][[!*chbarch_18_only]]" class="buyProduct btn btn-success btn-block" type="submit">Cингл (сс)
                    [[#[[+tv.chbarch_ProductSingleOut]].price]]
                </button>
            </form>
        </div>
        <div class="col-md-4" style="padding-left:2px;">
            <form method="post" class="chForm btn-block" id="ms2form_[[+tv.chbarch_ProductDoubleOut]]">
                <input type="hidden" name="count" value="1">
                <input type="hidden" name="options" value="[]">
                <input type="hidden" name="id" value="[[+tv.chbarch_ProductDoubleOut]]">
                <input type="hidden" name="ms2_action" value="cart/add">
                <button data-product-id="[[+tv.chbarch_ProductDoubleOut]]" data-18-only="[[!+chbarch_18_only]][[!*chbarch_18_only]]" class="buyProduct btn btn-success btn-block" type="submit" >Дабл (сс)
                    [[#[[+tv.chbarch_ProductDoubleOut]].price]]
                </button>
            </form>
        </div>
    </div>
</div>
</div>
-->