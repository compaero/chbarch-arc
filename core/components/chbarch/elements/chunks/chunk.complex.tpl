<div class="modal fade chbarch-global" tabindex="-1" id="spOffer-select">
    <div class="modal-dialog modal-dialog-twice complexMain" data-complex-id="[[+complexId]]" data-max-product-limit="0" data-group-count="[[+complexImportantCountGroups]]" data-complex-cost="[[+complexCost]]" data-complex-base-cost="[[+complexCost]]">
        <div class="modal-content this-category">
            <div class="modal-header">
                <h4 class="modal-title"><span>[[+complexName]]</span></span></h4>
                <h3 class="modal-under-title">Базовая цена: [[+complexCost]] р.</h3>
            </div>
            <div class="modal-body modal-body-category">
                [[+complexGroups]]
            </div>
            <div class="modal-footer">
                         <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Отмена</button>
            </div>
        </div>
        <div class="modal-content this-product">
            <div class="selectedProductsBlock" style="">
                <div class="selectedProducts" style=""></div>
                <div class="orderButtonBlock" style="">
                    <div style="display:none;" class="showPrice">К оплате: <span>[[+complexCost]]</span> р.</div>
                    <button style="display:none;" class="addOrderButton btn btn-block btn-primary" disabled="disabled">Добавить заказ</button>
                </div>
            </div>
            [[+complexProductListBlock]]
            <!-- <div class="modal-header">
                <h4 class="modal-title">Выбранные позиции</h4>
            </div>
            <div class="modal-body modal-body-product">
                [[+complexProductList]]
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-block" id="spOfferDoneBtn" name="spOfferDoneBtn" value="">Внести заказ</button>
            </div> -->
        </div>
        <div class="modal-content modal-product-list"></div>
    </div>
</div>