<div class="modal fade" tabindex="-1" id="order-close-bycard">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Оплата по карте</h4>
			</div>
			<div class="modal-body">
				<button type="submit" class="btn btn-success btn-lg btn-block" id="order-close-bycard-final" name="ch_action" value="order/create" data-dismis="modal">
					Подтвердить оплату
				</button>
			</div>
			<div class="modal-footer no-padding">
				<button type="button" class="btn btn-default btn-block btn-block" data-dismiss="modal">Вернуться назад</button>
			</div>
		</div>
	</div>
</div>