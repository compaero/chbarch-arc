    <div class="container-fluid">
        <div class="row">
            <div id="[[+subUnic]]" data-sub-cost="[[+subCost]]" data-sub-group="[[+subGroupNum]]" data-sub-group-limit="[[+subGroupLimit]]" class="subButton col-md-12 unselectedButton parent-status-[[+subGroupStatus]] sub-group-[[+subGroupNum]]">
                <button type="button" class="btn btn-primary btn-lg btn-block" data-spoffer-key="[[+spOfferKey]]" data-spoffer-partid="[[+spOfferPartId]]">
                    <span class="this-status"><i class="fa fa-plus"></i></span>
                    <span class="this-prod-title">[[+subName]]</span>
                </button>
            </div>
        </div>
        <!-- <span>Выбрано: [[#[[+spOfferProductId]].pagetitle: default=`пока не выбрано`]]</span> -->
    </div>