<div class="modal fade" tabindex="-1" id="order-close">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">К оплате <span id="to-pay"></span></h4>
			</div>
			<div class="modal-body">

				<div class="form-group">
					<!--p>Введите полученную сумму</p-->
					<input type="number" step="1" min="0" class="form-control input-lg" id="cash" name="cash" placeholder="Вносимая сумма">
				</div>

                <input type="text" class="form-control starttable" name="table" placeholder="Стол">
                <hr style="margin-top: 10px; margin-bottom:10px;">


				<button type="button" class="btn btn-info btn-block" id="useAccount" value="true" disabled>
					<i class="fa" id="useAccountIcon"></i>
                    Использовать счет
                    <span id="useAccount_status"></span>
				</button>
                <br>
				<button type="button" class="btn btn-success btn-lg btn-block" id="order-cash" name="ch_action" value="order/create">
					Оплата наличными
				</button>
				<input type="hidden" name="payment" value="cash">

                <br>

				<input type="hidden" name="payment" value="debit">
				<button type="button" class="btn btn-warning btn-lg btn-block" id="order-card" name="ch_action" value="order/create">
					Оплата по карте
				</button>

                <hr>

                <button type="button" class="btn btn-danger btn-lg btn-block" id="order-delayed" name="ch_action" value="order/delayed">
                    Отложить заказ
                </button>
                <input type="hidden" name="payment" value="delayed">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Отмена</button>
			</div>
		</div>
	</div>
</div>