<div class="col-sm-6 col-md-4">
<div class="list-group-item [[+productParent]] productButton" data-product-price ="[[+productPrice]]" data-product-count="[[+productCount]]" data-product-name="[[+productName]]" data-product-sub-cost="[[+productSubCost]]" data-product-id="[[+productId]]" data-product-parent="[[+productListParent]]" data-product-group="[[+productGroup]]">
    <button class="btn btn-block complexPartAdd">[[+productName]] [[+productPrice:gt=`1`:then=`+ [[+productPrice]]руб`]]</button>
</div>
</div>