<div class="row" style="padding: 4px; background: #eee;border-bottom: 1px solid #ddd; margin-bottom: 10px;">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-4" style="padding: 4px 4px 4px 18px;">
                <button class="btn btn-primary btn-block" type="button" onclick="location.href='[[~[[*id]]]]?ordtype=opened';">Выполняемые заказы</button>
            </div>


            <div class="col-md-4" style="padding: 4px">
                <button class="btn btn-primary btn-block" type="button" onclick="location.href='[[~[[*id]]]]?ordtype=delayed';">Отложенные заказы</button>
            </div>


            <div class="col-md-4" style="padding: 4px">
                <button class="btn btn-primary btn-block" type="button" onclick="location.href='[[~[[*id]]]]?ordtype=closed';">Закрытые заказы</button>
            </div>
        </div>
    </div>
</div>
<div class="chOrderList">
[[!chbarchGetOrders?]]
</div>