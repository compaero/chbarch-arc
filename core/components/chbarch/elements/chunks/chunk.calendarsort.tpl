<div class="panel-heading panel-form">
	<form class="form-inline sort" method="post">
		<div class="form-group">
			<div class="input-group date" id="datetimepicker1">
				<div class="input-group-addon">с</div>
				<input class="form-control" type="text" size="14" placeholder="дд.мм.гггг чч:мм" name="dateBegin" value="[[!+dateBegin]]">
				<div class="input-group-btn">
					<button class="btn btn-default" type="button">
						<i class="fa fa-fw fa-calendar"></i>
					</button>
				</div>
			</div>
		</div>
		—
		<div class="form-group">
			<div class="input-group date" id="datetimepicker2">
				<div class="input-group-addon">по</div>
				<input class="form-control" type="text" size="14" placeholder="дд.мм.гггг чч:мм" name="dateEnd" value="[[!+dateEnd]]">
				<!--div class="input-group-btn">
				  <button class="btn btn-default disabled" type="button">
					<i class="fa fa-fw fa-calendar"></i>
				  </button>
				</div>-->
			</div>
		</div>
		<button type="submit" class="btn btn-default">Показать</button>
	</form>
</div>