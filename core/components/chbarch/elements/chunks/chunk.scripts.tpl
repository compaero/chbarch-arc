[[!MinifyX?
	&jsSources=`assets/components/chbarch/src/src/js/jquery-2.1.1.min.js, assets/components/chbarch/src/src/js/isotope/isotope.pkgd.min.js, assets/components/chbarch/src/src/js/datepicker/moment-with-locales.min.js, assets/components/chbarch/src/src/js/datepicker/bootstrap-datetimepicker.min.js, assets/components/chbarch/src/src/js/bootstrap/transition.js, assets/components/chbarch/src/src/js/bootstrap/modal.js,/assets/components/ajaxform/js/config.js,/assets/components/ajaxform/js/default.js, assets/components/chbarch/src/src/js/custom.js`
	&minifyJs=`1`
    &minifyCss=`0`
	&registerJs=`default`
	&forceUpdate=`[[++chbarch_force_update_scripts]]`
]]
<!--
&cssSources=`assets/components/chbarch/src/src/style/custom.less, assets/components/chbarch/src/src/style/datepicker/bootstrap-datetimepicker.min.css`
&minifyCss=`1`
&registerCss=`default`
-->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<!-- <script src="assets/components/chbarch/src/js/jquery-2.1.1.min.js"></script> -->
<!-- <script src="assets/components/chbarch/src/js/isotope/isotope.pkgd.min.js"></script> -->
<!-- <script src="assets/components/chbarch/src/js/bootstrap/transition.js"></script> -->
<!-- <script src="assets/components/chbarch/src/js/bootstrap/modal.js"></script> -->
<!-- <script src="assets/components/chbarch/src/js/moment.min.js"></script> -->
<!-- <script src="assets/components/chbarch/src/js/moment.ru.js"></script> -->
<!-- <script src="assets/components/chbarch/src/js/ion.calendar.min.js"></script> -->
<!-- <script src="assets/components/chbarch/src/js/custom.js"></script> -->
