<div class="order-item success [[+option.flyer:notempty=`info`]] [[+option.spOffer:notempty=`danger`]]" data-category="[[+parent]]" style="padding: 0px 0px;">
    <div class="tblcell order-count"><small class="text">[[+idx]]</small></div>
    <div class="tblcell order-name"><small class="text" id="[[+key]]">[[+option.flyer:notempty=`[Ф] `]][[+option.spOffer:notempty=`[K] `]] [[+pagetitle]]</small></div>
	<div class="tblcell order-value controller">
		<!--<form method="post"> -->
		<div class="input-group">
        <span class="input-group-btn">
          <button type="submit" class="btn btn-primary order-item-minus order-item-change" data-key="[[+key]]" data-count="[[+count]]" data-dir = "minus"  name="countDirection" value="down">
			  <i class="fa fa-fw fa-minus" data-key="[[+key]]" data-count="[[+count]]" data-dir = "minus" ></i>
		  </button>
        </span>
			<input type="text" step="1" min="1" maxlength="3" class="form-control count-counter" name="count" value="[[+count]]" data-role="none" data-value="[[!+count]]" data-price="[[!+price]]" readonly>
        <span class="input-group-btn">
        <button type="submit" class="btn btn-primary order-item-plus order-item-change" data-key="[[+key]]" data-count="[[+count]]" data-dir = "plus"  name="countDirection" value="up">
            <i class="fa fa-fw fa-plus" data-key="[[+key]]" data-count="[[+count]]" data-dir = "plus"></i>
        </button>
        </span>
		</div>
		<input type="hidden" name="ms2_action" value="cart/change">
		<input type="hidden" name="key" value="[[+key]]">
<!--		</form>-->
	</div>
	<div class="tblcell order-cost"><p class="text-primary"><b>[[+price:mpy=`[[+count]]`]]</b><br><small>[[+price]]</small></p></div>
	<div class="tblcell order-item-remove">
		<!--<form method="post">
			<input type="hidden" name="key" value="[[+key]]">-->
			<button class="btn btn-danger btn-sm btn-order-remove-item" data-key="[[+key]]" name="ms2_action" value="cart/remove"><i class="fa fa-fw fa-times" data-key="[[+key]]"></i></button>
		<!--</form>-->
	</div>
</div>

<!--<div class="order-item success [[+option.flyer:notempty=`info`]] [[+option.spOffer:notempty=`danger`]]" data-category="[[+parent]]" style="padding: 0px 0px;">
	<div class="tblcell order-count"><small class="text">1</small></div>
	<div class="tblcell order-name"><small class="text" id="[[+key]]">[[+option.flyer:notempty=`[Ф] `]][[+option.spOffer:notempty=`[K] `]] [[+pagetitle]]</small></div>
	<div class="tblcell order-value controller">
		<form method="post">
			<div class="input-group input-group">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary order-item-minus" data-key="[[+key]]" data-count="[[+count]]" onClick="cCount=$('.count-counter',this.form).attr('value');$('.count-counter',this.form).attr('value',parseInt(cCount)-1); return true;" name="countDirection" value="down">
                        <i class="fa fa-fw fa-minus"></i>
                    </button>
                </span>
                <input type="text" step="1" min="1" maxlength="3" class="form-control count-counter" name="count" value="3" data-role="none" data-value="[[!+count]]" data-price="[[!+price]]" readonly>
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary order-item-plus" data-key="[[+key]]" data-count="[[+count]]" onClick="cCount=$('.count-counter',this.form).attr('value');$('.count-counter',this.form).attr('value',parseInt(cCount)+1); return true;" name="countDirection" value="up">
                        <i class="fa fa-fw fa-plus"></i>
                    </button>
                </span>
			</div>
			<input type="hidden" name="ms2_action" value="cart/change">
			<input type="hidden" name="key" value="[[+key]]">
		</form>
	</div>
	<div class="tblcell order-cost"><p class="text-primary"><b>300</b><br><small>100</small></p></div>
	<div class="tblcell order-item-remove">
		<form method="post">
			<input type="hidden" name="key" value="[[+key]]">
			<button class="btn btn-danger btn-sm btn-order-remove-item" type="submit" name="ms2_action" value="cart/remove"><i class="fa fa-fw fa-times"></i></button>
		</form>
	</div>
</div>-->