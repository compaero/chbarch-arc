<div class="modal fade" tabindex="-1" id="order-delayed-return">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Корзина не пуста! Очистить корзину и вернуть заказ?</h4>
            </div>

            <div class="modal-body">
                <button type="submit" class="btn btn-success btn-lg btn-block order-delayed-return" id="forDelayedId" name="ch_action" value="" data-dismis="modal">
                    Очистить и вернуть
                </button>

                <hr>

                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">
                    Не очищать
                </button>
            </div>

        </div>
    </div>
</div>