<div class="col-md-4 metro-item">
	<div class="thumbnail special_offer" data-product-id="[[+id]]" data-product-it="[[+introtext]]" data-18-only="[[!+chbarch_18_only]][[!*chbarch_18_only]]" data-no-discount="[[+tv.chbarch_no_discount]][[*tv.chbarch_no_discount]]">
		<form method="post" class="ms2_form" id="ms2form_[[+id]]">
			<h4 class="item-title">[[+menutitle:default=`[[+pagetitle]]`]]</h4>
			<div class="item-cost">[[chbarchTrimzeros? &input=`[[+price]]`]]</div>
			<input type="hidden" name="id" value="[[+id]]">
			<input type="hidden" name="count" value="1">
			<input type="hidden" name="options" value="[]">
			<input type="hidden" name="ms2_action" value="cart/add">
		</form>
	</div>
</div>