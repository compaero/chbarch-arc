<div class="modal fade" tabindex="-1" id="complex-product-count">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Количество</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid metro countbutton">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-lg btn-block" data-dismiss="modal">Продолжить</button>
            </div>
        </div>
    </div>
</div>