<div class="modal fade" tabindex="-1" id="order-additional">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Заказ #1234</h4>
			</div>
			<div class="modal-body">
				<button type="button" class="btn btn-lg btn-success" id="discountBarmen">Скидка бармену</button>
				<button type="button" class="btn btn-lg btn-success" id="discountManager">Скидка менеджеру</button>
				<button type="button" class="btn btn-lg btn-success" id="discountfifty">Скидка 50%</button>
				<hr>
				<form role="form" method="post">
					<fieldset>
						<legend>Комментарии к заказу</legend>
						<div class="form-group">
						  <textarea id="prod-comments" class="form-control" rows="6" placeholder="Комментариев нет…"></textarea>
						</div>
					  </fieldset>
					<fieldset>
						<legend>Удалить заказ</legend>
						<div class="form-group">
							<form method="post">
								<label>Вы можете удалить заказ. Внимание, это действие нельзя отменить!</label>
								<button type="submit" class="btn btn-danger btn-lg btn-block" name="ms2_action" value="cart/clean">Удалить заказ</button>
							</form>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-lg btn-block" data-dismiss="modal">Продолжить</button>
			</div>
		</div>
	</div>
</div>