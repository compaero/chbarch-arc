<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="collapse navbar-collapse" id="menu-top">
			[[!pdoMenu?
			&parents=`1`
			&tpl=`@INLINE <li><button type="button" class="btn btn-lg btn-warning" onClick="location.href='[[+link]][[+link_attributes]]';">[[+menutitle]]</button></li>`
			&outerClass=`nav navbar-nav`
			&checkPermissions=`load`
			&level=`1`
			&userid=`[[!+modx.user.id]]`
			]]
			<ul class="nav navbar-nav navbar-right">
				<form method="get" action="[[~1]]?service=logout">
					<li>
						<button type="button" class="tab btn btn-success" id="chgoh_init"><i class="fa fa-fw fa-clock-o"></i>Помощник</button>
						<button type="button" class="tab btn btn-success disabled"><i class="fa fa-fw fa-clock-o"></i> <span id="digital_watch"></span></button>
						<button type="submit" class="tab btn btn-default" id="btn-sign-out" name="service" value="logout">[^t^] Выход ([[!+modx.user.username]]) <i class="fa fa-fw fa-sign-out"></i></button></li>
				</form>
			</ul>
		</div>
	</div>
</nav>
<div class="notify">
    <div class="notifymessage" id="notifymessage">
        <div class="success">

        </div>
        <div class="error">

        </div>
    </div>
    <div class="notifyguest" id="notifyguest" [[!+guestData]]>
        [[!+guestDisplayName]] [[!+guestDisplayName:notempty=`<small><a onclick="chbcs.clearGuest();">(Очистить)</a></small>`]]
    </div>
</div>