<div class="modal fade" tabindex="-1" id="additional">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Заказ #1234</h4>
            </div>
            <div class="modal-body" id="insertion">
            <!--подстановка контента(для заказа или для товара) в зависимости от запроса-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-lg btn-block" data-dismiss="modal">Продолжить</button>
            </div>
        </div>
    </div>
</div>