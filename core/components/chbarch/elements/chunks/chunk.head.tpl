<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>[[++site_name]] - [[*pagetitle]]</title>
<base href="[[++site_url]]">
<link rel="stylesheet" href="/assets/components/chbarch/src/src/style/custom.css" type="text/css">
<link rel="stylesheet" href="/customsolus.css" type="text/css">
<script src="/assets/components/minishop2/js/web/lib/jquery.min.js" type="text/javascript"></script>
<!--link type="text/css" href="src/src/style/ion.calendar.css" rel="stylesheet" /-->
<link rel="shortcut icon" href="/assets/components/chbarch/src/src/ico/favicon.ico">
<script src="assets/components/chbarch/js/mgr/chbarch.js" type="text/javascript"></script>