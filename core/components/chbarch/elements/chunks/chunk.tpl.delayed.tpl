<div class="col-md-3 order-opened" data-orderid="[[+orderId]]" onClick="">
    <div class="thumbnail row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6"><span class="pull-right" style="padding: 10px 0; font-size: 14px; color: #666;">[[+delayedTime]]</span></div>
            </div>
            <small>
                <table class="table table-condensed">
                    [[+productsList]]
                </table>
            </small>
            <div class="row" style="padding-top: 50px; padding-bottom: 10px;">
                <div class="col-md-4 col-md-offset-8">
                    <button type="button" class="btn btn-success btn-lg return-delayed" name="ch_action" value="[[+delayedId]]" style="position:absolute; left: 0; bottom: 0;">Вернуть</button>
                    <!-- <div class="item-cost btn btn-block disabled" style="position:relative;">[[+delayedCost]] <sub><i class="fa fa-rub"></i></sub></div> -->
                </div>
            </div>
        </div>
    </div>
</div>