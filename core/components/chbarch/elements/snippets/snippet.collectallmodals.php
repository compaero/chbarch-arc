<?php
$query = $modx->newQuery('modChunk');
$query->where(array('name:LIKE'=>'ch%.modal.%'));

$query->prepare();
$query->stmt->execute();
$chunks = $query->stmt->fetchAll(PDO::FETCH_ASSOC);

$output = '';

foreach ($chunks as $chunk) {
	$output .= '[[$'.$chunk['modChunk_name'].']]'."\n";
}

return $output;
//die($output);