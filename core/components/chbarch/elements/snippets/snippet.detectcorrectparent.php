<?php
// Name: chbarchComplex
// Description: This snippet construct them.

if (isset($_REQUEST['complexAction'])) {
    $complexAction = $_REQUEST['complexAction'];
}

$collection = array(
    'id',
    'introtext',
    'name' => 'Lunch',                      // Set Variable
    'cost' => 179,                          // Set Cost
    'groups' => array(                      // Set Product List
        0 => array(                         // Set First Group with Params (Limit, Subgroup)
            'limit' => 2,
            'subgroup' => array(
                array(                      // Set Params for Parents (Name, Cost, ProductsIt, ParentsIt, Products)
                    'name' => 'Суп',
                    'cost' => 0,
                    'productsIt' => array(),
                    'parentsIt' => array('83daaeb567114059bc5508ef95f0ffd5'),
                    'products' => array()    // Set Params for Final Product (Id, Pagetitle, Cost)
                ),
                array(
                    'name' => 'Пирог',
                    'cost' => 0,
                    'productsIt' => array(),
                    'parentsIt' => array('0259128c14f0fc7529118a93a4b0369a'),
                    'products' => array()
                ),
                array(
                    'name'=> 'Салат',
                    'cost' => 0,
                    'productsIt' => array('f25d9e0894807fdc53ee3f2780cad269', '511892f3281e138ae262c12f1d3916ff', '244309d376945613eed329a5a4da310c'),
                    'parentsIt' => array(),
                    'products' => array()
                ),
            ),
        ),

        1 => array(                         // Set First Group with Params (Limit, Subgroup)
            'limit' => 2,
            'subgroup' => array(
                array(                      // Set Params for Parents (Name, Cost, ProductsIt, ParentsIt, Products)
                    'name' => 'Холодный чай',
                    'cost' => 0,
                    'productsIt' => array('2bf264a7f76a415bd67cd29b4d76be91'),
                    'parentsIt' => array(),
                    'products' => array(),
                ),
                array(
                    'name' => 'Горячий чай',
                    'cost' => 50,
                    'productsIt' => array('30836cfcc119ee79f7827519fcd7f123', '97968b29bd6ce7f4409265f5a5839168', 'e162e24373019e73ded4b7cc79f7cd24', 'b28a59eb8b813455b766892502a9c674', '4b56d2f563942833d1286bfdd81fcbc3', '5e0b1d23ca7df6fd6d41a179b3f68b68', '3abef401b015a8a3bf0cbc789783fc72', '0246cb29667c92cd29c1e47c488faef7'),
                    'parentsIt' => array(),
                    'products' => array()
                ),
            ),
        ),
    ),
);

$products = array();
$output = array();

function getComplex($complexIt) {
    global $modx;
    $complexIt = trim($complexIt);
    if(!empty($complexIt)) {
        if(!($complexData = $modx->getObject('msProduct',array('introtext'=>$complexIt)))) {
            return json_encode(array('success'=>0,'msg'=>'Special offer with introtext='.$complexIt.' not found'));
        }
        else {
            $complex = array();
            $complex['id'] = $complexData->get('id');
            $complex['introtext'] = $complexIt;
            $complex['name'] = $complexData->get('pagetitle');
            $complex['cost'] = $complexData->get('price');
            $complex['groups'] = json_decode($complexData->get('content'), true);
            $complex = getComplexProducts($complex);
            return $complex;
        }
    }
}
function getComplexProducts($complex) {
    global $modx;
    $groupList = '';
    $notImportantGroups = 0;
    $compProductListBlock = '';
    foreach ($complex['groups'] as $groupNum => $subgroupNum) {
        $subgroupList = '';
        $productListBlock = '';
        foreach ($subgroupNum['subgroup'] as $key => $value) {
            if (is_array($value)) {
                $subView['subUnic'] = md5($value['name']);
                if (!empty($value['productsIt'])) {
                    //for ($i = 0; $i < count($value['productsIt']); $i++) {
                    foreach ($value['productsIt'] as $i => $productId) {
                        if($prod = $modx->getObject('modResource', array('introtext' => $productId))) {
                            $value['products'][$prod->get('id')] = $prod->get('pagetitle');
                        } else {
                            $modx->log(1, "product not found! introtext: ".$productId);
                        }
                    }
                }
                if (!empty($value['parentsIt'])) {
                    for ($i = 0; $i < count($value['parentsIt']); $i++) {
                        $par = $modx->getObject('modResource', array('introtext' => $value['parentsIt'][$i]));
                        $prods = $par->getMany('Children');
                        $productList = '';
                        foreach ($prods as $prod) {
                            $value['products'][$prod->get('id')] = $prod->get('pagetitle');
//                            $complex['groups'][$groupNum]['subgroup'][$key]['products'][$prod->get('id')] = $prod->get('pagetitle');
                        }
                    }
                }
                if(!empty($value['products'])) {
                    foreach($value['products'] as $productId => $productName) {
                        $product = array('productName' => $productName,
                            'productId' => $productId,
                            'productGroup' => $groupNum,
                            'productParent' => $subView['subUnic'],
                            'productSubCost' => $value['cost']);
                        if(!$value['count']) {
                            $product['productCount'] = 1;
                            $value['count'] = 0;
                        } else {
                            $product['productCount'] = $value['count'];
                        }
                        $productList .= dressData($product, 'product');
                    }

                }

                $productListView = array('productList' => $productList,
                    'productListParent' => $subView['subUnic']);
                $subView['subCost'] = $value['cost'];
                if($value['cost'] > 0) {
                    $subView['subName'] = $value['name'] . ' + ' . $value['cost'] . ' р.';
                } else {
                    $subView['subName'] = $value['name'];
                }
                $subView['subGroupNum'] = $groupNum;
                $subView['subGroupLimit'] = $subgroupNum['limit'];
                $subView['subGroupStatus'] = $subgroupNum['status'];
//                $subView['subProdList'] = $productList;
                $productList = '';
            }
            $subgroupList .= dressData($subView, 'sub');
            $productListBlock .= dressData($productListView, 'productList');
        }
        $groupView = array(
            'groupLimit' => $subgroupNum['limit'],
            'groupStatus' => $subgroupNum['status'],
            'groupSubs' => $subgroupList,
            'groupNum' => $groupNum,
        );
        $groupList .= dressData($groupView, 'group');
        if($subgroupNum['status'] == 0) {
            $notImportantGroups++;
        }

        $compProductListBlock .= $productListBlock;
        $productListBlock = '';
    }
    $complexView = array(
        'complexId' => $complex['id'],
        'complexName' => $complex['name'],
        'complexCost' => $complex['cost'],
        'complexGroups' => $groupList,
        'complexProductListBlock' => $compProductListBlock,
        'complexImportantCountGroups' => count($complex['groups']) - $notImportantGroups,
        'complexCountGroups' => count($complex['groups']),
    );
    $complex = dressData($complexView, 'complex');
    return $complex;
}
// !FOR EXAMPLE: $param = complex | group | sub | productList | product
function dressData($data, $param) {
    global $modx;
    switch($param) {
        case 'complex':
            return $modx->getChunk('tpl.chbarch.complex', $data);
        case 'group':
            return $modx->getChunk('tpl.chbarch.complexGroup', $data);
        case 'sub':
            return $modx->getChunk('tpl.chbarch.complexSub', $data);
        case 'productList':
            return $modx->getChunk('tpl.chbarch.complexProductList', $data);
        case 'product':
            return $modx->getChunk('tpl.chbarch.complexProduct', $data);
    }
}

/* =================================================================================== */
/* =================================================================================== */
/* =================================================================================== */

switch ($complexAction) {
    case 'complexOpen':
        if (!empty($_REQUEST['complexIt'])) {
            $complexIt = $_REQUEST['complexIt'];
            $complex = getComplex($complexIt);
            return json_encode(array('data' => 1, 'html' => array('main' => $complex)));
            unset($complexAction);
            unset($_REQUEST['complexAction']);
        }
        break;

    case 'complexOrder':
        if (!empty($_REQUEST['complexId']) && !empty($_REQUEST['complexCost']) && !empty($_REQUEST['complexData'])) {
//            return 'all right';
            $complexId = $_REQUEST['complexId'];
            $complexCost = $_REQUEST['complexCost'];
            $complexData = json_decode($_REQUEST['complexData']);
            $complexKey = sha1(mktime()+$complexId);
//            var_dump($complexData);
            $products = array();
            foreach($complexData as $key) {
                $products[$key->id] = $key->count;
            }


            $miniShop2 = $modx->getService('minishop2');
            $miniShop2->initialize('web', array('json_response' => true));
            if (!($miniShop2 instanceof miniShop2)) {
                @session_write_close();
                exit('Could not initialize miniShop2');
            }
//            $allPrices = 234;
            $productsData = $modx->getCollection('msProduct',array('id:IN'=>array_keys($products)));
//            var_dump($productsData[1]->get('id'));
            $countProducts = count($products);
//            if (!$countProducts) $modx->log(1, 'no products returned from DB');
//            $i = 0;
            foreach ($productsData as $product) {
                $allPrices = $allPrices + $product->get('price');
//                $modx->log(1, '[spO] prepare product: '.$product->id.' prod price: '.$product->get('price').' all prices: '.$allPrices);
            }
            $coeff = $complexCost / $allPrices;

            $allPrices = 0;

            $i = 0;
            $allPrices = 0;
            foreach ($products as $key => $count) {
                $options = array();
                $id = $productsData[$key]->get('id');
                if(!empty($_REQUEST['complexData']) && $_REQUEST['complexData'] == 'discount') {
                    $productPrice = round($productsData[$key]->get('price') * $coeff);
                    if((int)$key->get('price') == 119) {
                        $productPrice = 79;
                    }
                    $count = 1;
                } else {
                    $productPrice = round($productsData[$key]->get('price') * $coeff / $count);
                    $allPrices = $allPrices + $productPrice * $count;
                    if ($i == $countProducts - 1) {
                        $diff = $complexCost - $allPrices;
                        $productPrice = round($productPrice + $diff);
                    }
                }
                $options['price'] = $productPrice;
                $options['count'] = $count;
                $options['spOffer'] = $complexKey;
                $miniShop2->cart->add($id, $count, $options);
                $i++;
            }
            unset($i);
            unset($_REQUEST);
            return json_encode(array('data'=>1, 'html'=>''));
        }
        else {
            return 'hello';
        }
        break;

    default: break;
}

unset($complexAction);
unset($_REQUEST['complexAction']);