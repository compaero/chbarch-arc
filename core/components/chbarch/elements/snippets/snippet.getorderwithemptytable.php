<?php


$tpl = $modx->getOption('tpl', $scriptProperties, 'tpl.chbarch.orderWithEmptyTable');
/*function printCheckForWorker($orderId, $table, $atoll, &$modx, &$fiscalTestmode) {
	$checkLines = array();
	if ($orderId) {
		$checkLines[] = ' ';
		$checkLines[] = 'Заказ '.$orderId;
		$checkLines[] = ' ';
		$checkLines[] = 'Стол: '.$table;
		$checkLines[] = ' ';
		$checkLines[] = '____________________________________________ ';
		$checkLines[] = ' ';

		if (!$fiscalTestmode) {
			foreach ($checkLines as $line) $atoll->printLine($line);
			$atoll->printClishe();
		} else {
			echo "<pre>";
			print_r($checkLines);
			echo "</pre>";
		}
	}
}
*/

if (isset($_REQUEST['ch_action'])) {
	$action = $_REQUEST['ch_action'];
	switch ($action) {
		case 'order/settable':
			$orderId = (int) $_REQUEST['orderId'];
			$table = $_REQUEST['table'];
			$order = $modx->getObject('msOrder',array('id'=>$orderId));
			$order->set('properties',json_encode(array('table'=>$table)));
			$order->save();
			break;

		default: break;
	}
}

$dateBegin = date("Y-m-d H:i:s",mktime(date('H')-1, date('i'), date('s'), date("m")  , date("d"), date("Y")));
//$modx->log(1,$dateBegin);
$query = $modx->newQuery('msOrder');
$query->where(array(
	'msOrder.properties'=> null,
	'msOrder.createdon:>'=>$dateBegin,
	'msOrder.status:IN'=>array(1,2),
));
$query->sortby('id','ASC');
$query->limit(5);
//$query->prepare();
//$modx->log(1, $query->toSQL());
$orders = $modx->getCollection('msOrder',$query);
//$modx->log(1,'count: '.count($orders));
//die(var_dump($orders));
$output = '';
foreach ($orders as $order) {
    $placeholders['orderId'] = $order->get('id');
    $address = $order->getOne('Address');
    $props = $address->get('properties');
    $placeholders['table'] = !empty($props['table']) ? $props['table'] : 0;
//    $placeholders['table'] = '';
//    $placeholders['table'] = !empty($tpl) ? $tpl : 0;
	if(!$props['table']) {
		$output .= $modx->getChunk('tpl.chbarch.orderWithEmptyTable', $placeholders);
//		$modx->log(1, 'Что-nj новенькое! '.$props['table'].$output);
	}
}
return $output;