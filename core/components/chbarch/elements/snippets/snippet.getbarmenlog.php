<?php
$dateBegin = $_REQUEST['dateBegin'] ? $_REQUEST['dateBegin'] : date("Y-m-d H:i:s",mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
$dateEnd = $_REQUEST['dateEnd'] ? $_REQUEST['dateEnd'] : date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s"), date("m")  , date("d"), date("Y")));
$modx->setPlaceholder('dateBegin',$dateBegin);
$modx->setPlaceholder('dateEnd',$dateEnd);


$query = $modx->newQuery('modManagerLog');
$query->where(array('user'=>4,
	'AND:occurred:>='=>$dateBegin,
	'AND:occurred:<='=>$dateEnd));
//$query->limit(50);
$query->sortby('occurred', 'DESC');
$query->prepare();
$query->stmt->execute();
$logRecords = $query->stmt->fetchAll(PDO::FETCH_ASSOC);

$output = '<table class="table table-condensed"><tr><td>Дата, время</td><td>Действие</td></tr>';
foreach ($logRecords as $logRecord) {
	$output .= '<tr><td>'.$logRecord['modManagerLog_occurred'].'</td><td>'.$logRecord['modManagerLog_action'].'</td>';

	//var_dump($logRecord);
	//break;
}

$output .= '</table>';

return $output;