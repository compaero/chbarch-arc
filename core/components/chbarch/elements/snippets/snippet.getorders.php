<?php
$dateBegin = $_REQUEST['dateBegin'] ? $_REQUEST['dateBegin'] : date("Y-m-d H:i:s",mktime(date("H")-5, date("i"), date("s"), date("m")  , date("d"), date("Y")));
$dateEnd = $_REQUEST['dateEnd'] ? $_REQUEST['dateEnd'] : date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s"), date("m")  , date("d"), date("Y")));
$modx->setPlaceholder('dateBegin',$dateBegin);
$modx->setPlaceholder('dateEnd',$dateEnd);

if (isset($_REQUEST['ch_action'])) {
    $action = $_REQUEST['ch_action'];
    switch ($action) {
        case 'order/changestatus':
            $orderId = $_REQUEST['orderId'];
            $status = $_REQUEST['status'];
            $order = $modx->getObject('msOrder',array('id'=>$orderId));
            $order->set('status', $status);
            $order->save();
            //exit();
            break;
        /*Устарело, изменение стола перенесенно в сниппет createOrder
         * case 'order/changetable':
            $orderId = (int) $_POST['orderId'];
            $table = $_POST['table'];

            $order = $modx->getObject('msOrder',array('id'=>$orderId));
            $address = $order->getOne('Address');

            $props = $address->get('properties');
            $props['table'] = $table;
            $address->set('properties',$props);
            $address->save();
            break;
        Устарело
         * case 'cart/returndelayed':
            $miniShop2 = $modx->getService('minishop2');
            $miniShop2->initialize('web', array('json_response' => true));
            if (!($miniShop2 instanceof miniShop2)) {
                //@session_write_close();
                exit('Could not initialize miniShop2');
            }
            $response = $miniShop2->cart->get();
            if (empty($response)) {
                $delayedId = $_POST['delayedId'];
                $delayedOrder = $_SESSION['delayedOrders'][$delayedId];
                $miniShop2->cart->set($delayedOrder);
                unset($_SESSION['delayedOrders'][$delayedId]);
                //$modx->sendRedirect($modx->makeUrl(12));
            }

            break;*/

        default: break;
    }
}

$output = '';
if (isset($_GET['ordtype'])) {
    $ordtype = $_GET['ordtype'];
    switch ($ordtype) {
        case 'opened':
        case 'closed':
            $status = ($ordtype == 'closed') ? '3' : '1,2';
            //$tpl = ($ordtype == 'opened')?'tpl.Opened': 'tpl.Closed';
            //$tpl = 'tpl.chbarch.orders';
            $sortDir = ($status == 3) ? 'DESC' : 'ASC';
            $query = $modx->newQuery('msOrder');
            $query->where(array('status:IN' => explode(',', $status),
            'createdon:>=' => $dateBegin,
            'createdon:<=' => $dateEnd));
            $query->sortby('id', $sortDir);
            $orders = $modx->getCollection('msOrder', $query);
            foreach ($orders as $order) {
                $placeholders = array();
                $placeholders['orderId'] = $order->get('id');
                $placeholders['createdon'] = $order->get('createdon');
                $placeholders['cost'] = $order->get('cost');
                $placeholders['status'] = $order->get('status');

                $address = $order->getOne('Address');
                $props = $address->get('properties');

                 $placeholders['table'] = isset($props['table']) ? $props['table'] : 0;

                /*$orderProps = $order->get('properties');
                $placeholders['table'] = isset($orderProps['table']) ? $orderProps['table'] : 0;*/

                $products = $modx->getCollection('msOrderProduct', array('order_id' => $order->get('id')));
                $placeholders['productsList'] = '';
                foreach ($products as $product) {
                    $placeholders['productsList'] .= '<tr><td>' . $product->name . '</td><td>' . $product->count . '</td>';
                }
                //die(print_r($placeholders));
                $output .= $modx->getChunk('tpl.chbarch.ordersList', $placeholders);
            }
            break;
        
        case 'delayed':
            //тут выводим отложенные заказы, из сессии.
            if(isset($_SESSION['delayedOrders'])) {
                $delayedOrders = $_SESSION['delayedOrders'];
                foreach($delayedOrders as $delayedOrder => $key) {
                    $placeholders['delayedId'] = $delayedOrder;
                    $placeholders['delayedTime'] = $key['time'];
                    $placeholders['delayedCost'] = $key['cost'];

                    unset($key['time']);
                    unset($key['cost']);
                    unset($key['OrderProperties']);

                    $placeholders['productsList'] = '';
                    foreach ($key['cart'] as $products=>$product) {
                        if (is_array($product)) {
                            $id = $product['id'];
                            $count = $product['count'];
                            $response = $modx->runProcessor('resource/get', array('id' => $id));
                            $name = $response->response['object']['pagetitle'];
                            $placeholders['productsList'] .= '<tr><td>' . $name . '</td><td>' . $count . '</td>';
                        }
                    }
                    $output .= $modx->getChunk('tpl.chbarch.delayed', $placeholders);
                }
            }
            break;

        default: break;
    }
}
return $output;






//***********************************************************************



/*$tpl = $modx->getOption('tpl', $scriptProperties, 'tpl.chbarch.orders');

$status = $scriptProperties['status'];
if ($status == 10) {
    $output = '';
    $delayedOrders = $_SESSION['delayedOrders'];
    foreach ($delayedOrders as $delayedOrder => $v) {
        $placeholders = array();
        $placeholders['orderId'] = '111';
        $output .= $modx->getChunk('tpl.chbarch.orders',$placeholders);
    }
    return $output;
}


$sortDir = ($status == 3) ? 'DESC' : 'ASC';
$query = $modx->newQuery('msOrder');
$query->where(array('status:IN'=>explode(',',$status),
	'createdon:>='=>$dateBegin,
	'createdon:<='=>$dateEnd));
$query->sortby('id',$sortDir);
$orders = $modx->getCollection('msOrder',$query);

$output = '';
foreach ($orders as $order) {
	$placeholders = array();
	$placeholders['orderId'] = $order->get('id');
	$placeholders['createdon'] = $order->get('createdon');
	$placeholders['cost'] = $order->get('cost');
	$placeholders['status'] = $order->get('status');
	$orderProps = $order->get('properties');
	$placeholders['table'] = isset($orderProps['table']) ? $orderProps['table'] : 0;

	$products = $modx->getCollection('msOrderProduct',array('order_id'=>$order->get('id')));
	$placeholders['productsList'] = '';
	foreach ($products as $product) {
		$placeholders['productsList'] .= '<tr><td>'.$product->name.'</td><td>'.$product->count.'</td>';
	}
	//die(print_r($placeholders));

    $output .= $modx->getChunk($tpl,$placeholders);
    //$output .= $modx->getChunk('tpl.order.opened',$placeholders);

}

return $output;

*/


$orderId = $_REQUEST['orderId'];
$order = $modx->getObject('msOrder',array('id'=>$orderId));
$result = array('orderId'=>$order->id,
	'cost'=>$order->cost,
	'createdon'=>$order->createdon,
	'payment'=>$order->payment,
	'status'=>$order->status
);
$result['products'] = array();
$orderProps = $order->get('properties');
if ($orderProps) $orderProps = json_decode($orderProps);
$result['table'] = isset($orderProps['table']) ? $orderProps['table'] : 0;
$products = $modx->getCollection('msOrderProduct',array('order_id'=>$order->id));
$productsTmp = array();
foreach ($products as $product) {
	$productsTmp[] = array('id'=>$product->id,
		'pagetitle'=>$product->name,
		'count'=>$product->count,
		'price'=>$product->price,
		'cost'=>$product->cost);
}
$result['products'] = $productsTmp;
exit(json_encode($result));