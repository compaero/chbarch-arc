<?php

$barmenGroup = $modx->getObject("modUserGroup", array("name"=>"Barmens"));
$barmenGroupId =  $barmenGroup->get("id");
$managerGroup = $modx->getObject("modUserGroup", array("name"=>"Managers"));
$managerGroupId =  $managerGroup->get("id");
$sManagerGroup = $modx->getObject("modUserGroup", array("name"=>"StarManagers"));
$sManagerGroupId =  $sManagerGroup->get("id");
$user = $modx->user;
$groups = $user->getUserGroups();
if (in_array($sManagerGroupId,$groups) or in_array($managerGroupId,$groups)) $pageId = 6;
if($blacktie = $modx->getObject('modResource',array('introtext'=>'fb69df49c20e9baca7ffad1196c20acf'))){
    $blacktieId = $blacktie->get('id');
}
if (in_array($barmenGroupId,$groups)) $pageId = $blacktieId;
if ($pageId <> 1) $modx->sendRedirect($modx->makeUrl($pageId));