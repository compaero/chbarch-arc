<?php
/**
 * Created by PhpStorm.
 * User: Владимир
 * Date: 10.12.2014
 * Time: 16:46
 */

if(!empty($_GET['action'])) {
    $action = $_GET['action'];
    switch ($action) {
        case 'createGroup':
            if (!empty($_GET['group'])){
                $group = $_GET['group'];
                createUserGroup($modx, $group);
            }
            break;
        case 'createUser':
            if (!empty($_GET['user']) and !empty($_GET['password'])){
                $user = $_GET['user'];
                $password = $_GET['password'];
                createUser ($modx, $user, $password);
            }
            break;
        case 'moveUser':
            if (!empty($_GET['user']) and !empty($_GET['group'])){
                $user = $_GET['user'];
                $group = $_GET['group'];
                moveUserToGroup($modx, $user, $group);
            }
            break;
    }
}


function createUserGroup(&$modx, $nameGroup) {
    //Создаем группы
    $response = $modx->runProcessor('security/group/create', array('name' => $nameGroup));
    if ($response->isError()) {
        $modx->log(modX::LOG_LEVEL_ERROR, 'Не удалось создать группу '.$nameGroup);
    } else {
        $modx->log(modX::LOG_LEVEL_ERROR, 'Группа '.$nameGroup.' создана.');
    }
}

function createUser (&$modx, $nameUser, $passwordUser) {
    //Создаем пользователей
    $data = array(
        'username' => $nameUser,
        'email' => 'aaa@aaa.ru',
        'passwordnotifymethod' => 's',
        'active' => 1,
        //'primary_group' => $idGroup,
        'passwordgenmethod' => 'spec',
        'specifiedpassword' => $passwordUser,
        'confirmpassword' => $passwordUser,
    );
    $response = $modx->runProcessor('security/user/create', $data);
    if ($response->isError()) {
        $modx->log(modX::LOG_LEVEL_ERROR, 'Не удалось создать пользователя'.$nameUser);
    } else {
        $modx->log(modX::LOG_LEVEL_ERROR, 'Пользователь '.$nameUser.' создан.');
    }
}

function moveUserToGroup(&$modx, $nameUser, $nameGroup) {
    $userObj = $modx->getObject('modUser', array('username' =>$nameUser));
    $userId = $userObj->get('id');

    $groupUserObj = $modx->getObject('modUserGroup', array('name' =>$nameGroup));
    $groupUserId = $groupUserObj->get('id');

    $response = $modx->runProcessor('security/group/user/create', array('usergroup'=> $groupUserId, 'user'=>$userId, 'role'=>'1'));
    if ($response->isError()) {
        $modx->log(modX::LOG_LEVEL_ERROR, 'Не удалось перенести пользователя '.$nameUser.' в группу '.$nameGroup);
    } else {
        $modx->log(modX::LOG_LEVEL_ERROR, 'Пользователя '.$nameUser.' перемещен в группу '.$nameGroup);
    }
}