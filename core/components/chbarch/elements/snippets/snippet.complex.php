<?php
// Name: chbarchComplex
// Description: This snippet construct them.

if (isset($_REQUEST['complexAction'])) {
    $complexAction = $_REQUEST['complexAction'];
} else {
    $complexAction = '';
}


$products = array();
$output = array();

function getComplex($complexIt) {
    global $modx;
    $complexIt = trim($complexIt);
    if(!empty($complexIt)) {
        if(!($complexData = $modx->getObject('msProduct',array('introtext'=>$complexIt)))) {
            return json_encode(array('success'=>0,'msg'=>'Special offer with introtext='.$complexIt.' not found'));
        }
        else {
            $complex = array();
            $complex['id'] = $complexData->get('id');
            $complex['introtext'] = $complexIt;
            $complex['name'] = $complexData->get('pagetitle');
            $complex['cost'] = $complexData->get('price');
            $complex['groups'] = unserialize($complexData->get('content'));
//            $complex['groups'] = json_decode($complexData->get('content'), true);
            $modx->log(1,'complex array: '.print_r($complex,1));
            $complex = getComplexProducts($complex);
            return $complex;
        }
    }
    return '';
}

function getComplexProducts($complex) {
    global $modx;
    $groupList = '';
    $notImportantGroups = 0;
    $compProductListBlock = '';
    foreach ($complex['groups'] as $groupNum => $subgroupNum) {
        $subgroupList = '';
        $productListBlock = '';
        foreach ($subgroupNum['subgroup'] as $key => $value) {
            $subView = array();
            $productListView = array();
            if (is_array($value)) {
                $subView['subUnic'] = md5($value['name']);
                if (!empty($value['productsIt'])) {
                    //for ($i = 0; $i < count($value['productsIt']); $i++) {
                    foreach ($value['productsIt'] as $i => $productId) {
                        if (is_array($productId)) {
                            if ($prod = $modx->getObject('modResource', array('introtext' => $productId['product']))) {
                                $value['products'][$prod->get('id')] = array(
                                    'pagetitle' => $prod->get('pagetitle'),
                                    'price'     => $productId['price']
                                );
                            }


                        }
                        else{
                            /** @var modResource $prod */
                            if ($prod = $modx->getObject('modResource', array('introtext' => $productId))) {
                                $value['products'][$prod->get('id')] = $prod->get('pagetitle');

                            }
                        }
                    }
                }
                if (!empty($value['parentsIt'])) {
                    for ($i = 0; $i < count($value['parentsIt']); $i++) {
                        $par = $modx->getObject('modResource', array('introtext' => $value['parentsIt'][$i]));
                        $prods = $par->getMany('Children');
                        foreach ($prods as $prod) {
                            $value['products'][$prod->get('id')] = $prod->get('pagetitle');
//                            $complex['groups'][$groupNum]['subgroup'][$key]['products'][$prod->get('id')] = $prod->get('pagetitle');
                        }
                    }
                }
                $productList = '';
                if(!empty($value['products'])) {
                    foreach($value['products'] as $productId => $productName) {
                        if(is_array($productName)){
                            $pageTitle = $productName['pagetitle'];
                            $productPrice =$productName['price'];
                        }
                        else{
                            $pageTitle = $productName;
                            $productPrice = 0;
                        }
                        $product = array('productName' => $pageTitle,
                            'productId' => $productId,
                            'productGroup' => $groupNum,
                            'productPrice' => $productPrice,
                            'productParent' => $subView['subUnic'],
                            'productSubCost' => $value['cost']);

                        if(!$value['count']) {
                            $product['productCount'] = 1;
                            $value['count'] = 0;
                        } else {
                            $product['productCount'] = $value['count'];
                        }
                        $productList .= dressData($product, 'product');
                    }

                }

                $productListView = array('productList' => $productList,
                    'productListParent' => $subView['subUnic']);
                $subView['subCost'] = $value['cost'];
                if($value['cost'] > 0) {
                    $subView['subName'] = $value['name'] . ' + ' . $value['cost'] . ' р.';
                } else {
                    $subView['subName'] = $value['name'];
                }

                $subView['subGroupNum'] = $groupNum;
                $subView['subGroupLimit'] = $value['submax'];
                $subView['subGroupStatus'] = $subgroupNum['status'];
//                $subView['subProdList'] = $productList;
            }
            $subgroupList .= dressData($subView, 'sub');
            $productListBlock .= dressData($productListView, 'productList');
        }
        $groupView = array(
            'groupLimit' => $subgroupNum['limit'],
            'groupStatus' => $subgroupNum['status'],
            'groupSubs' => $subgroupList,
            'groupNum' => $groupNum,
        );
        $groupList .= dressData($groupView, 'group');
        if($subgroupNum['status'] == 0) {
            $notImportantGroups++;
        }

        $compProductListBlock .= $productListBlock;
    }
    $complexView = array(
        'complexId' => $complex['id'],
        'complexName' => $complex['name'],
        'complexCost' => $complex['cost'],
        'complexGroups' => $groupList,
        'complexProductListBlock' => $compProductListBlock,
        'complexImportantCountGroups' => count($complex['groups']) - $notImportantGroups,
        'complexCountGroups' => count($complex['groups']),
    );
    $complex = dressData($complexView, 'complex');
    return $complex;
}
// !FOR EXAMPLE: $param = complex | group | sub | productList | product
function dressData($data, $param) {
    global $modx;
    switch($param) {
        case 'complex':
            return $modx->getChunk('tpl.chbarch.complex', $data);
        case 'group':
            return $modx->getChunk('tpl.chbarch.complexGroup', $data);
        case 'sub':
            return $modx->getChunk('tpl.chbarch.complexSub', $data);
        case 'productList':
            return $modx->getChunk('tpl.chbarch.complexProductList', $data);
        case 'product':
            return $modx->getChunk('tpl.chbarch.complexProduct', $data);
    }
    return '';
}

/* =================================================================================== */
/* =================================================================================== */
/* =================================================================================== */

switch ($complexAction) {
    case 'complexOpen':
        if (!empty($_REQUEST['complexIt'])) {
            $complexIt = $_REQUEST['complexIt'];
            $complex = getComplex($complexIt);
            return json_encode(array('data' => 1, 'html' => array('main' => $complex)));
//            unset($complexAction);
//            unset($_REQUEST['complexAction']);
        }
        break;

    case 'complexOrder':
        if (!empty($_REQUEST['complexId']) && !empty($_REQUEST['complexCost']) && !empty($_REQUEST['complexData'])) {
//            return 'all right';
            $complexId = $_REQUEST['complexId'];
            $complexCost = $_REQUEST['complexCost'];
            $complexData = json_decode($_REQUEST['complexData']);
            $complexKey = sha1(mktime()+$complexId);
//            var_dump($complexData);
            $products = array();
            foreach($complexData as $key) {
                $products[$key->id] = $key->count;
            }


            $miniShop2 = $modx->getService('minishop2');
            $miniShop2->initialize('web', array('json_response' => true));
            if (!($miniShop2 instanceof miniShop2)) {
                @session_write_close();
                exit('Could not initialize miniShop2');
            }

            $options = array();
            $options['price'] = 0;
            $options['count'] = 1;
            $options['spOffer'] = $complexKey;
            $miniShop2->cart->add($complexId, 1, $options);
            $allPrices = 0;
            /** @var msProduct[] $productsData */
            $productsData = $modx->getCollection('msProduct',array('id:IN'=>array_keys($products)));
//            var_dump($productsData[1]->get('id'));
            $countProducts = count($products);
//            if (!$countProducts) $modx->log(1, 'no products returned from DB');
//            $i = 0;
            foreach ($productsData as $product) {
                $allPrices = $allPrices + $product->get('price') * $products[$product->get('id')];
//                $modx->log(1, '[spO] prepare product: '.$product->id.' prod price: '.$product->get('price').' all prices: '.$allPrices);
            }
            $coeff = $complexCost / $allPrices;

            $allPrices = 0;

            $i = 0;
            $allPrices = 0;
            $singleProductKey = '---';
            foreach ($products as $key => $count) {
                $options = array();
                $id = $productsData[$key]->get('id');
                if(!empty($_REQUEST['complexData']) && $_REQUEST['complexData'] == 'discount') {
                    $productPrice = round($productsData[$key]->get('price') * $coeff);
//                    if((int)$key->get('price') == 119) {
//                        $productPrice = 79;
//                    }
                    $count = 1;
                } else {
                    $productPrice = round($productsData[$key]->get('price') * $coeff);
                }
                $allPrices += $productPrice * $count;
                $options['price'] = $productPrice;
                $options['count'] = $count;
                $options['spOffer'] = $complexKey;
                $options['nodiscount'] = 1;
                $response = $modx->fromJSON($miniShop2->cart->add($id, $count, $options));
                if ($count == 1) {
                    $singleProductKey = $response['data']['key'];
                }
                $i++;
            }
            $diff = $complexCost - $allPrices;
            $cart = $miniShop2->cart->get();
            if ($diff and isset($cart[$singleProductKey])) {
                $cart[$singleProductKey]['price'] += $diff;
//                $productPrice = round($productPrice + $diff);
                $miniShop2->cart->set($cart);
            }
            unset($i);
            unset($_REQUEST);
            return json_encode(array('data'=>1, 'html'=>''));
        }
        else {
            return 'hello';
        }
        break;

    default: break;
}

unset($complexAction);
unset($_REQUEST['complexAction']);