<?php

/** @var modX $modx */
/** @var array $scriptProperties */
/** @var chBarch $chBarch */
if (!$chBarch = $modx->getService('chbarch', 'chBarch', $modx->getOption('chbarch_core_path', null, $modx->getOption('core_path') . 'components/chbarch/') . 'model/chbarch/', $scriptProperties)) {
    return 'Could not load chBarch class!';
}

$action = '';
if (isset($_REQUEST['ch_action']))
	$action = $_REQUEST['ch_action'];
switch ($action) {
    case 'order/changetable':
        $orderId = (int) $_REQUEST['orderId'];
        if(!$_REQUEST['table']) {
            $table = 0;
        } else {
            $table = $_REQUEST['table'];
        }

        $order = $modx->getObject('msOrder',array('id'=>$orderId));
        /** @var msOrderAddress $address */
        $address = $order->getOne('Address');

        $props = $address->get('properties');
        $props['table'] = $table;
        unset($props['tableprinted']);
        $address->set('properties',$props);
        $address->save();
        break;

    case 'getProdModal':
        $output = '';
        $prodId = $_REQUEST['id'];
        //$output .= $modx->getChunk('tpl.chbarch.others');
        $miniShop2 = $modx->getService('minishop2');
        $miniShop2->initialize('web', array('json_response' => true));
        if (!($miniShop2 instanceof miniShop2)) {
            //@session_write_close();
            exit('Could not initialize miniShop2');
        }
        $cart = $miniShop2->cart->get();
        $product = $cart[$prodId];
        $options = json_decode($product['options'],1);

        //---------Берем из системной настройки фишки продукта и проверяем с фишками в базе--------------
        $prodDibs = json_decode($modx->getOption('chbarch_products_dibs'), 1);
        $dibs = array();
        if (isset($options['prodDibs'])){
            $dibs = $options['prodDibs'];
        }

        foreach ($prodDibs as $prodDib => $propertiesDib) {
            $nameDib = $propertiesDib['name'];
            $iconDib = $propertiesDib['icon'];
            $idDib = $prodDib;

            $key = array_search($idDib, $dibs);
            $class = 'btn-success';
            if($key !== false) {$class = 'btn-primary';}

            $data = array(
                'nameDib' => $nameDib,
                'idDib' => $idDib,
                'iconDib' => $iconDib,
                'class' => $class,
                'cat' => 'prod',
            );
            $output .= $modx->getChunk('tpl.chbarch.chips', $data);
        }

        //--------Берем комментарий продукта--------------
        $comment = '';
        if (isset($options['comment'])) {
            $comment = $options['comment'];
        }
        $data = array(
            'id' => $prodId,
            'class-comment' => 'prod-comment',
            'comment' => $comment,
        );
        $output .= $modx->getChunk('tpl.chbarch.comment', $data);
        return $output;

        break;

    case 'addProdComments':
        $key = $_REQUEST['key'];
        $comment = $_REQUEST['comment'];

        $miniShop2 = $modx->getService('minishop2');
        $miniShop2->initialize('web', array('json_response' => true));
        if (!($miniShop2 instanceof miniShop2)) {
            //@session_write_close();
            exit('Could not initialize miniShop2');
        }
        $cart = $miniShop2->cart->get();

        $product = $cart[$key];

        $options = json_decode($product['options'], 1);
        $options['comment'] = $comment;
        $json_options = json_encode($options);
        $product['options'] = $json_options;
        $cart[$key] = $product;
        $miniShop2->cart->set($cart);
        break;

    case 'getOrderModal':
        $output = '';
        $output .= $modx->getChunk('tpl.chbarch.others');

        //Берем из системной настройки фишки заказа
        $orderDibs = json_decode($modx->getOption('chbarch_orders_dibs'), 1);
        $dibs = array();
        if (isset($_SESSION['OrderProperties']['orderDibs'])) {
            $dibs = $_SESSION['OrderProperties']['orderDibs'];
        }

        foreach ($orderDibs as $orderDib => $propertiesDib) {
            $nameDib = $propertiesDib['name'];
            $iconDib = $propertiesDib['icon'];
            $idDib = $orderDib;

            $key = array_search($idDib, $dibs);
            $class = 'btn-success';
            if($key !== false) {$class = 'btn-primary';}

            $data =  array(
                'nameDib' => $nameDib,
                'idDib' => $idDib,
                'iconDib' => $iconDib,
                'class' => $class,
                'cat' => 'order',
            );

            $output .= $modx->getChunk('tpl.chbarch.chips', $data);
        }

        if(isset($_SESSION['OrderProperties']['OrderComment'])) {
            $comment = $_SESSION['OrderProperties']['OrderComment'];
        }

        $data = array(
            'class-comment' => 'order-comment',
            'comment' => $comment,
        );
        $output .= $modx->getChunk('tpl.chbarch.comment', $data);

        return $output;
        break;

    case 'addOrderComment':
        $_SESSION['OrderProperties']['OrderComment'] = $_REQUEST['comment'];
        //$_SESSION['OrderComment'] = $_POST['comment'];
        break;

    /*Устарело
     case 'getOrderComments':
        $comment = '';
        if(isset($_SESSION['OrderComments'])) {
            $comment = $_SESSION['OrderComments'];
        }
        return $comment;
        break;

    case 'order/settable':
		$orderId = (int) $_POST['orderId'];
		$table = $_POST['table'];
		$order = $modx->getObject('msOrder',array('id'=>$orderId));
		$order->set('properties',json_encode(array('table'=>$table)));
		$order->save();
		break;*/

	case 'order/create' :
        $modx->getService('miniShop2');
        $c = $modx->newQuery('msOrder');
        $c->select("msOrder.id");
        $c->sortby('id', 'DESC');
        $c->limit(1);

        if ($orderin = $modx->getObject('msOrder', $c)) {
            $lastorder = $orderin->get('createdon');
            $lastorder = strtotime($lastorder) + 5;
            $lastorder = date('Y-m-d H:i:s', $lastorder);
            if (strtotime($lastorder) >= strtotime(date('Y-m-d H:i:s'))) {
                return ('{"success":true,"message":"","data":{"msorder":' . $orderin->get('createdon') . '}}');
            }
        }
		$notFiscalPayment = 5;
		if (isset($_REQUEST['payment'])) $payment = $_REQUEST['payment'];
        $useAccount = false;
        if (isset($_REQUEST['useAccount']) and $_REQUEST['useAccount'] === 'true') $useAccount = true;

        $orderComment = (isset($_SESSION['OrderProperties']['OrderComment'])) ? $_SESSION['OrderProperties']['OrderComment'] : '';
        $propsArr['Dibs'] = (isset($_SESSION['OrderProperties']['orderDibs'])) ? ($_SESSION['OrderProperties']['orderDibs']) : '';
        $propsArr['table'] = $_REQUEST['table'];
        unset($_SESSION['OrderProperties']);

        $miniShop2 = $modx->getService('minishop2');
		$miniShop2->initialize('web', array('json_response' => true));
		if (!($miniShop2 instanceof miniShop2)) {
			@session_write_close();
			exit('Could not initialize miniShop2');
		}

		switch ($payment) {
			case 'debit':
                $paymentId = 4;
                break;
			case 'cash':
			default:
                $paymentId = 1;
                $propsArr['take_sum'] = (isset($_REQUEST['take_sum'])) ? $_REQUEST['take_sum'] : '';
                break;
		}
		if (($paymentId == 1) and (!$chBarch->findFiscalMode())) $paymentId = $notFiscalPayment;

        $props = json_encode($propsArr);

        $receiver = 'Guest';
        $email = 'guest@chainikoff.ru';
        $sessionGuestVar = $modx->getOption('chguests_session_var');
        if (isset($_SESSION[$sessionGuestVar]) and (is_array($_SESSION[$sessionGuestVar]))) {
            $sessionGuest = $_SESSION[$sessionGuestVar]['username'];
            if ($guest = $modx->getObject('chGuest', array('username' => $sessionGuest)) and $guest->get('checked_phone')) {
                $receiver = $guest->get('username');
                $email = $guest->Profile->get('email');
                if ($paymentId == 1 and $guest->get('trusted')) $paymentId = $notFiscalPayment;
            }
        }

        $orderProps = array(
			'email'=>$email,
			'receiver'=>$receiver,
			'phone'=>'',
			'comment'=>$orderComment,
			'payment'=>$paymentId,
			'delivery'=>1,
			'index'=>'',
			'region'=>'',
			'city'=>'',
			'street'=>'',
			'building'=>'',
			'room'=>'',
			'ms2_action'=>'order/submit',
			'ctx'=>'web',
            'properties' => $props,
            'useAccount' => $useAccount
		);

//        $modx->log(1,'order props: '.print_r($orderProps,1));
		$response = $miniShop2->order->submit($orderProps);

        return $response;
		break;

    case 'order/delayed':
        $miniShop2 = $modx->getService('minishop2');
        $miniShop2->initialize('web', array('json_response' => true));
        if (!($miniShop2 instanceof miniShop2)) {
            //@session_write_close();
            exit('Could not initialize miniShop2');
        }
        $response = array();
        $response['cart'] = $miniShop2->cart->get();
        $response['time'] = date("Y-m-d H:i:s");
        $cart = $miniShop2->cart->status();
        $response['cost'] = $cart['total_cost'];

        //$response['OrderComment'] = '';
        if (isset($_SESSION['OrderProperties'])) {
            $response['OrderProperties'] = $_SESSION['OrderProperties'];
            unset($_SESSION['OrderProperties']);
        }

        $_SESSION['delayedOrders'][] = $response;
        $miniShop2->cart->clean();
        break;

    case 'order/return-delayed':
        $miniShop2 = $modx->getService('minishop2');
        $miniShop2->initialize('web', array('json_response' => true));
        if (!($miniShop2 instanceof miniShop2)) {
            //@session_write_close();
            exit('Could not initialize miniShop2');
        }

        if(isset($_REQUEST['confirmation'])){
            $miniShop2->cart->clean();
            unset($_SESSION['OrderProperties']);
            unset($_REQUEST['confirmation']);
        }

        $response = $miniShop2->cart->get();
        if (empty($response)) {
            $delayedId = $_REQUEST['delayedId'];
            $_SESSION['OrderProperties'] = $_SESSION['delayedOrders'][$delayedId]['OrderProperties'];
            unset($_SESSION['delayedOrders'][$delayedId]['OrderProperties']);
            unset($_SESSION['delayedOrders'][$delayedId]['cost']);
            $delayedOrder = $_SESSION['delayedOrders'][$delayedId];
            $miniShop2->cart->set($delayedOrder['cart']);
            unset($_SESSION['delayedOrders'][$delayedId]);
//            $modx->log(1, 'DELAYED AFTER:' . $delayedId);
            return $modx->makeUrl(12);
        } else {
            return '0';
        }
        break;

    case 'order/dib':
        $dib = $_REQUEST['orderDibId'];
        $result = '';
        if (isset($_SESSION['OrderProperties']['orderDibs'])) {
            $dibs = $_SESSION['OrderProperties']['orderDibs'];

            $key = array_search($dib, $dibs);
            /*если ключ найден значит фишка была нажата. Удаляем значение по найденому ключу из session и снимаем active у фишки,
            иначе добавляем в session и ставим active у фишки*/
            if($key !== false) {
                unset($_SESSION['OrderProperties']['orderDibs'][$key]);
                $result['active'] = 0;
                $result['id'] = $dib;
            } else {
                $_SESSION['OrderProperties']['orderDibs'][] = $dib;
                $result['active'] = 1;
                $result['id'] = $dib;
            }
        } else {
            $_SESSION['OrderProperties']['orderDibs'][] = $dib;
            $result['active'] = 1;
            $result['id'] = $dib;
        }
        $output = json_encode($result);
        return $output;
        break;

    case 'prod/dib':
        $dib = $_REQUEST['prodDibId'];
        $id = $_REQUEST['prodId'];
        $result = '';

        $miniShop2 = $modx->getService('minishop2');
        $miniShop2->initialize('web', array('json_response' => true));
        if (!($miniShop2 instanceof miniShop2)) {
            //@session_write_close();
            exit('Could not initialize miniShop2');
        }
        $cart = $miniShop2->cart->get();
        $product = $cart[$id];
        $options = json_decode($product['options'], 1);


        if (isset($options['prodDibs'])) {
            $dibs = $options['prodDibs'];

            $key = array_search($dib, $dibs);
            /*если ключ найден значит фишка была нажата. Удаляем значение по найденому ключу из session и снимаем active у фишки,
            иначе ставим active у фишки*/
            if($key !== false) {
                unset($dibs[$key]);
                $result['active'] = 0;
                $result['id'] = $dib;
            } else {
                $dibs[]= $dib;
                $result['active'] = 1;
                $result['id'] = $dib;
            }
        } else {
            $dibs[]= $dib;
            $result['active'] = 1;
            $result['id'] = $dib;
        }

        //записываем обновленные фишки в базу
        $options['prodDibs']=$dibs;
        $json_options = json_encode($options);
        $product['options'] = $json_options;
        $cart[$id] = $product;
        $miniShop2->cart->set($cart);

        $output = json_encode($result);
        return $output;
        break;

    case 'customGood/getInfo':
        $goodIT = $modx->sanitizeString($_REQUEST['goodIT']);
        if ($good = $modx->getObject('msProduct',array('introtext' => $goodIT))) {
            return json_encode(array(
                'success' => true,
                'data' => array(
                    'goodName' => $good->get('pagetitle'),
                    'goodIT' => $good->get('introtext')
                )
            ));
        } else {
            return json_encode(array('success' => false, 'message' => 'Товар не найден'));
        }

    case 'customGood/sell':
        $goodPrice = (int) $_REQUEST['goodSum'];
        $goodIT = $modx->sanitizeString($_REQUEST['goodIT']);
        $modx->log(1,'good price: '.$goodPrice);
        if ($goodPrice > 0 && $good = $modx->getObject('msProduct',array('introtext' => $goodIT))) {
            $_REQUEST['ch_action'] = 'order/delayed';
            $modx->runSnippet('chbarchCreateOrder');

            $miniShop2 = $modx->getService('minishop2');
            $miniShop2->initialize('web', array('json_response' => true));
            if (!($miniShop2 instanceof miniShop2)) {
                //@session_write_close();
                exit('Could not initialize miniShop2');
            }

            $good->set('price',$goodPrice);
            $good->save();
            $miniShop2->cart->add($good->get('id'));
            $_REQUEST['ch_action'] = 'order/create';
            return $modx->runSnippet('chbarchCreateOrder');
        } else {
            return json_encode(array('success' => false, 'message' => 'Товар не найден'));
        }


        break;
    case 'giftsList':

        $sessionGuestVar = $modx->getOption('chguests_session_var');
        $sessionGuest = $_SESSION[$sessionGuestVar]['username'];
        $date = new DateTime();
        $today = $date->format('Y-m-d');
        $day = $date->format('D');
        $todaydob = $date->format('m-d');
        $toyear = $date->format('Y');
        $hour = $date->format('H');

        $daynumber = $date->format('d');
        $month = $date->format('m');

        $giftname = array();
        $uid = array();
        $count = array();
        $nameuser = array(0 => '', 1 => '', 2 => '');
        $nameprod = array();
        $idprod = array();


        if ($user = $modx->getObject('modUser', array('username' => $sessionGuest))) {
            $nameuser = $user->getOne('Profile')->get('fullname');
            $nameuser = explode(" ", $nameuser);
        }

        if (isset($sessionGuest)) {

            $response = $modx->runProcessor('gift/guest/getlist', array('username' => $sessionGuest),
                array("processors_path" => $modx->getOption('core_path') . 'components/chgifts/processors/'));
            $json = $response->response;
            $json = json_decode($json);
            $giftname = array();
            $price =array();
            foreach ($json->results as $key => $v) {
                if ($v->blocked == 0 and $v->gifts_count > 0) {
                    $response = $modx->runProcessor('gift/get', array('uid' => $v->gift_uid,),
                        array("processors_path" => $modx->getOption('core_path') . 'components/chgifts/processors/'));

                    $expired_date = $response->response['object']['expired_date'];

                    $expired_date = $date->setTimestamp($expired_date);
                    $expired_date = $date->format('Y-m-d');
                    $expiredDateGift = $v->expired_date;

                    $expiredDateGift = $date->setTimestamp($expiredDateGift);
                    $expiredDateGift = $date->format('Y-m-d');
                    if ($response->response['object']['blocked'] ==0) {
                        if (strtotime($today) <= strtotime($expired_date) || $expired_date == '1970-01-01') {

                            if ($expiredDateGift == '1970-01-01' || strtotime($today) < strtotime($expiredDateGift)) {

                                if ($response->response['object']['type'] == 'product') {
                                    if ($response->response['object']['for_members'] == true) {
                                        $response2 = $modx->runProcessor('gift/log/getlist',
                                            array('gift_uid' => $v->gift_uid),
                                            array("processors_path" => $modx->getOption('core_path') . 'components/chgifts/processors/'));
                                        $issetgift = $response2->response;
                                        $issetgift = json_decode($issetgift);
                                        $giftname[] = $response->response['object']['name'];
                                        $uid[] = $v->gift_uid;
                                        $price[] = $response->response['object']['price'];
                                        $giftid[] = $v->id;
                                        $count[] = $v->gifts_count;


                                        foreach ($issetgift->results as $k => $val) {
                                            $properties = $val->properties;

                                            //$properties = json_decode($properties);

                                            if (
                                                $properties->period->accrual == $today
                                                || $today < $properties->period->period
                                            ) {
                                                unset($giftname[$k]);
                                                unset($uid[$k]);
                                                unset($giftid[$k]);
                                                unset($price[$k]);
                                                unset($count[$k]);
                                            }


                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $response = $modx->runProcessor('gift/getlist', array('for_members' => false, 'limit' => 0),
                array("processors_path" => $modx->getOption('core_path') . 'components/chgifts/processors/'));
            $json = $response->response;
            $json = json_decode($json);

            foreach ($json->results as $k => $v) {
                if ($v->for_members == false) {
                    if ($v->blocked == 0) {
                        $expired_date = $v->expired_date;

                        $expired_date = $date->setTimestamp($expired_date);
                        $expired_date = $date->format('Y-m-d');


                        if (
                            strtotime($today) <= strtotime($expired_date)
                            || $expired_date == '1970-01-01'
                            || $expired_date == null
                        ) {
                            $criteria = $v->criteria;

                            if ($criteria->datetime->userdob == 'today') {

                                $userdob = $user->getOne('Profile')->get('dob');

                                $date->setTimestamp($userdob);
                                $userdob = $date->format('m-d');

                                if ($todaydob == $userdob) {

                                    $giftname[] = $v->name;
                                    $uid[] = $v->uid;
                                    $count[] = 1;
                                    $price[] = $v->price;
                                }
                            }

                            if ($criteria->datetime->day == $day) {
                                $giftname[] = $v->name;
                                $uid[] = $v->uid;
                                $count[] = 1;
                                $price[] = $v->price;

                            }

                            if ($criteria->datetime->hour == $hour) {

                                $giftname[] = $v->name;
                                $uid[] = $v->uid;
                                $count[] = 1;
                                $price[] = $v->price;

                            }
                            if ($criteria->datetime->daynum == $daynumber) {

                                $giftname[] = $v->name;
                                $uid[] = $v->uid;
                                $count[] = 1;
                                $price[] = $v->price;

                            }

                            if ($criteria->datetime->month == $month) {

                                $giftname[] = $v->name;
                                $uid[] = $v->uid;
                                $count[] = 1;
                                $price[] = $v->price;

                            }


                            if (isset($nameuser[2]) and trim($criteria->user->name) == trim($nameuser[2])) {

                                $giftname[] = $v->name;
                                $uid[] = $v->uid;
                                $count[] = 1;
                                $price[] = $v->price;

                            }
                            if (
                                !$criteria->user->name
                                and !$criteria->datetime->userdob
                                and !$criteria->datetime->month
                                and !$criteria->datetime->daynum
                                and !$criteria->datetime->hour
                                and !$criteria->datetime->day
                            ) {
                                $giftname[] = $v->name;
                                $uid[] = $v->uid;
                                $count[] = 1;
                                $price[] = $v->price;
                            }


                        }


                    }
                }

                $response2 = $modx->runProcessor('gift/log/getlist', array('gift_uid' => $v->uid),
                    array("processors_path" => $modx->getOption('core_path') . 'components/chgifts/processors/'));
                $issetgift = $response2->response;
                $issetgift = json_decode($issetgift);
                foreach ($issetgift->results as $key => $val) {
                    if ($val->guest == $sessionGuest) {
                        $properties = $val->properties;
                        if ($properties->period_type->year == $toyear) {
                            $del = array_search($v->name, $giftname);
                            unset($giftname[$del]);
                            unset($uid[$del]);


                        }
                        $properties = $val->properties;
                        //$properties = json_decode($properties);
                        //var_dump($properties->period->accrual);
                        if ($properties->period->accrual == $today || $today < $properties->period->period) {
                            $del = array_search($v->name, $giftname);
                            unset($giftname[$del]);
                            unset($uid[$del]);


                        }

                        if ($properties->criteria->datetime->day == $day) {

                            $del = array_search($v->name, $giftname);
                            unset($giftname[$del]);
                            unset($uid[$del]);

                        }
                        if ($properties->criteria->datetime->hour == $hour) {

                            $del = array_search($v->name, $giftname);
                            unset($giftname[$del]);
                            unset($uid[$del]);
                        }
                        if ($properties->criteria->datetime->daynum == $daynumber) {

                            $del = array_search($v->name, $giftname);
                            unset($giftname[$del]);
                            unset($uid[$del]);
                        }
                        if ($properties->criteria->datetime->month == $month) {

                            $del = array_search($v->name, $giftname);
                            unset($giftname[$del]);
                            unset($uid[$del]);
                        }
                        if ($properties->criteria->user->name == $nameuser[2] and $nameuser[2]!="") {

                            $del = array_search($v->name, $giftname);
                            unset($giftname[$del]);
                            unset($uid[$del]);
                        }

                    }
                }
            }

            foreach ($uid as $key => $v) {

                $response = $modx->runProcessor(
                    'gift/product/getlist',
                    array("uid" => $v, 'blocked' => 0),
                    array("processors_path" => $modx->getOption('core_path') . 'components/chgifts/processors/'));
                $json = $response->response;
                $json = json_decode($json);
                $products = array();
                foreach ($json->results as $k => $val) {

                    $products[] = $val->product;

                }

                foreach ($products as $keys => $valu) {

                    $prod = $modx->getObject('modResource', array('introtext' => $valu));
                    $idprod[] = $prod->get('id');
                    $nameprod[] = array('name' => $prod->get('pagetitle'), 'gift_uid' => $v);

                }

            }

        }

        if (!empty($giftname)) {

            return json_encode(array(
                'success' => true,
                'giftname' => $giftname,
                'count' => $count,
                'uid' => $uid,
                'nameprod' => $nameprod,
                'idprod' => $idprod,
                'price' => $price
            ));

        } else {
            return json_encode(array('success' => false, 'giftname' => $giftname));
        }
        break;

}

return;
