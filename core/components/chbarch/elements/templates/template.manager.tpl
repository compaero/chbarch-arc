<!DOCTYPE html>
<html lang="ru">
<head>
	[[$chbarch.head]]
</head>
<body>
<header>
	[[$chbarch.navbar]]
</header>
<div class="workspace">
	[[$chbarch.aside.left.menu? &parents=`6` &addClass =`managerBlock`]]
	<section class="col-md-10 content">
		<div class="row">
			<div class="col-md-11 panel panel-default menu">
				[[*ch.needCalendar?]]
			</div>
		</div>
		<div class="row full-height">
			<div class="col-md-11 panel panel-default menu">
				[[*content]]
			</div>
		</div>
	</section>
</div>
[[$chbarch.footer]]
[[chbarchCollectAllModals]]
[[!$chbarch.scripts]]
</body>
</html>