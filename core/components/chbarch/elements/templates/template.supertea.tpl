**********************************************
**********************************************
**********************************************

ФАЙЛ УСТАРЕЛ, БОЛЕЕ НЕ ИСПОЛЬЗУЕТСЯ

**********************************************
**********************************************
**********************************************
[[!createOrder]]
<!DOCTYPE html>
<html lang="ru">
<head>
    [[$head]]
</head>
<body>
<header>
    [[$navbar]]
</header>
<div class="workspace footer-show">
    [[$aside.left.menu? &parents=`11`]]
    <section class="col-md-10 content">
        <div class="row full-height">
            <div class="col-md-7 panel panel-default menu">
                <div class="panel-heading no-padding">
                    <div class="sort">
                        <!--
                                    <ul class="nav nav-pills pull-right">
                                      <li class="active"><a href="#"><i class="fa fa-sort-numeric-asc"></i></a></li>
                                      <li><a href="#"><i class="fa fa-sort-numeric-desc"></i></a></li>
                                      <li><a href="#"><i class="fa fa-sort-alpha-asc"></i></a></li>
                                      <li><a href="#"><i class="fa fa-sort-alpha-desc"></i></a></li>
                                    </ul>
                        -->
                        [[-!AjaxSnippet? &snippet=`pdoMenu` &as_target=`.sort` &tplOuter=`@INLINE <ul[[+classes]]><li><a href="[[#12.uri]]" rel="external">Все</a></li>[[+wrapper]]</ul>` &tpl=`@INLINE <li[[+classes]]><a href="[[+link]]" [[+attributes]] rel="external">[[+menutitle]]</a>[[+wrapper]]</li>` &parents=`[[*parent]]`  &templates=`4` &limit=`0` &level=`1` &outerClass=`nav nav-pills` &firstClass=`` &lastClass=``]]
                        [[!pdoMenu?
                        &as_target=`.sort`
                        &tplOuter=`@INLINE <ul[[+classes]]><!--li><a href="[[#12.uri]]" rel="external">Все</a></li-->[[+wrapper]]</ul>`
                        &tpl=`@INLINE <li[[+classes]]><button type="button" class="btn btn-lg btn-success" onClick="location.href='[[+link]]';">[[+menutitle]]</button></li>`
                        &tplHere=`@INLINE <li[[+classes]]><button type="button" class="btn btn-lg btn-info" onClick="location.href='[[+link]]';">[[+menutitle]]</button></li>`
                        &parents=`[[*parent]]`
                        &templates=`4`
                        &limit=`0`
                        &level=`1`
                        &outerClass=`nav nav-pills`
                        &firstClass=``
                        &lastClass=``
                        ]]
                    </div>
                </div>
                <div class="panel-body">
                    <div class="metro menu-list">
                        [[!pdoPage?
                        &element=`pdoResources`
                        &as_target=`.menu-list`
                        &tpl=`chunk.tpl.menuitem.sTea`
                        &limit=`15`
                        &sortby=`[[*ch.categorySortDir]]`
                        &parents=`[[*id]]`
                        &depth=`1`
                        &showLog=`1`
                        &prepareTVs=`1`
                        &includeTVs=`chbarch_ProductSmall, chbarch_ProductMedium, chbarch_ProductLarge, chbarch_18_only`
                        &tplPageWrapper=`@INLINE <div class="btn-group btn-group-lg btn-group-justified">[[+first]][[+prev]][[+pages]][[+next]][[+last]]</div>`
                        &tplPage=`@INLINE <a href="[[+href]]" class="btn btn-success">[[+pageNo]]</a>`
                        &tplPageActive=`@INLINE <a href="[[+href]]" class="btn btn-info active">[[+pageNo]]</a>`
                        &tplPageFirst=`@INLINE <a href="[[+href]]" class="btn btn-success">В начало</a>`
                        &tplPageLast=`@INLINE <a href="[[+href]]" class="btn btn-success">В конец</a>`
                        &tplPagePrev=`@INLINE <a href="[[+href]]" class="btn btn-success">&laquo;</a>`
                        &tplPageNext=`@INLINE <a href="[[+href]]" class="btn btn-success">&raquo;</a>`
                        &tplPageSkip=`@INLINE <a class="btn btn-success disabled">...</a>`
                        &tplPageFirstEmpty=`@INLINE <a class="btn btn-success disabled">В начало</a>`
                        &tplPageLastEmpty=`@INLINE <a class="btn btn-success disabled">В конец</a>`
                        &tplPagePrevEmpty=`@INLINE <a class="btn btn-success disabled">&laquo;</a>`
                        &tplPageNextEmpty=`@INLINE <a class="btn btn-success disabled">&raquo;</a>`
                        ]]

                        [[-!pdoPage?
                        &element=`msProducts`
                        &tpl=`tpl.menu.item`
                        &limit=`15`
                        &parents=`[[*id]]`
                        &depth=`1`
                        &sortby=`[[*ch.categorySortDir]]`
                        &tplPageWrapper=`@INLINE <div class="btn-group btn-group-lg btn-group-justified">[[+first]][[+prev]][[+pages]][[+next]][[+last]]</div>`
                        &tplPage=`@INLINE <a href="[[+href]]" class="btn btn-success">[[+pageNo]]</a>`
                        &tplPageActive=`@INLINE <a href="[[+href]]" class="btn btn-info active">[[+pageNo]]</a>`
                        &tplPageFirst=`@INLINE <a href="[[+href]]" class="btn btn-success">В начало</a>`
                        &tplPageLast=`@INLINE <a href="[[+href]]" class="btn btn-success">В конец</a>`
                        &tplPagePrev=`@INLINE <a href="[[+href]]" class="btn btn-success">&laquo;</a>`
                        &tplPageNext=`@INLINE <a href="[[+href]]" class="btn btn-success">&raquo;</a>`
                        &tplPageSkip=`@INLINE <a class="btn btn-success disabled">...</a>`
                        &tplPageFirstEmpty=`@INLINE <a class="btn btn-success disabled">В начало</a>`
                        &tplPageLastEmpty=`@INLINE <a class="btn btn-success disabled">В конец</a>`
                        &tplPagePrevEmpty=`@INLINE <a class="btn btn-success disabled">&laquo;</a>`
                        &tplPageNextEmpty=`@INLINE <a class="btn btn-success disabled">&raquo;</a>`
                        ]]
                    </div>
                </div>
                <div class="panel-footer no-padding">
                    [[+page.nav]]
                </div>
            </div>
            [[$aside.right]]
        </div>
    </section>
</div>
[[$footer]]

<!-- Modals -->
[[chCollectAllModals]]
<!-- /Modals -->

[[$scripts]]
</body>
</html>