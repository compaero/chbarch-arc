<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>[[++site_name]] - [[*pagetitle]]</title>
	<base href="[[++site_url]]">
	<link rel="stylesheet" href="/assets/components/chbarch/src/src/style/custom.css" type="text/css">
	<link rel="shortcut icon" href="/assets/components/chbarch/src/src/ico/favicon.ico">
</head>
<body>
<div class="notify">
    <div class="notifymessage" id="notifymessage" style="float:left;">
        <div class="success">

        </div>
        <div class="error">

        </div>
    </div>
</div>
[[!Login?
&loginTpl=`tpl.chbarch.login`
&logoutTpl=`tpl.chbarch.logout`
&errTpl=`lgnErrTpl`
&loginResourceId=`1`
&logoutResourceId=`1`
]]
</body>
</html>