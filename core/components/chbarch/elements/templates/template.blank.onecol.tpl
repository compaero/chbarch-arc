<!DOCTYPE html>
<html lang="ru">
<head>
	[[$chbarch.head?]]
</head>
<body>
<header>
	[[$chbarch.navbar?]]
</header>
<div class="workspace">
	<div class="full-height" style="padding: 0 15px;">
		[[*ch.needCalendar?]]
		[[*content]]
	</div>
</div>
[[chbarchCollectAllModals]]
</body>
</html>