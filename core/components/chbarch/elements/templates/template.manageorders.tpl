<!DOCTYPE html>
<html lang="ru">
<head>
	[[$chbarch.head?]]
</head>
<body>
<header>
	[[$chbarch.navbar?]]
</header>
<div class="workspace">
	[[$aside.left.orders.open?]]
	<section class="col-md-10 content">
		<div class="row">
			<div class="col-md-12 panel panel-default menu">
				[[$chbarch.calendarSort?]]
			</div>
		</div>
		<div class="row full-height">
			<div class="col-md-12 panel panel-default menu">
				<div class="panel-body">
						<div class="metro menu-list realbarch">
						<div class="isotope">
							[[*content]]
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
[[$chbarch.scripts?]]

<!-- Modals -->

[[chbarchCollectAllModals]]
</body>
</html>