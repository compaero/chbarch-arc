<!DOCTYPE html>
<html lang="ru">
<head>
	[[$chbarch.head?]]
</head>
<body>
<header>
	[[$chbarch.navbar?]]
</header>
<div class="workspace footer-show_">
	[[*aside.left]]
	<section class="col-md-10 content">
		<div class="row full-height">
			<div class="col-md-12 panel panel-default menu">
				[[*ch.needCalendar?]]
				[[*content]]
			</div>
		</div>
	</section>
</div>
[[$chbarch.footer?]]

<!-- Modals -->
[[chbarchCollectAllModals?]]
<!-- /Modals -->

[[$chbarch.scripts?]]
</body>
</html>