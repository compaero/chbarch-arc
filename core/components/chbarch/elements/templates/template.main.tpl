<!DOCTYPE html>
<html lang="ru">
<head>
	[[$chbarch.head]]
</head>
<body>
<header>
	[[$chbarch.navbar]]
</header>
<div class="workspace">
	[[$aside.left.menu? &parents=`6`]]
	<section class="col-md-10 content">
		<div class="row full-height">
			<div class="col-md-7 panel panel-default menu">
				[[*ch.needCalendar?]]
				[[*content]]
			</div>
		</div>
	</section>
</div>
[[$footer]]
[[chCollectAllModals]]
[[$scripts]]
</body>
</html>