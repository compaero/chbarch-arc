<?php
if ($modx->context->key == 'mgr') {
    return;
}

exit('<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>АСУ отключена. Обратитесь к руководству Сети.</title>
 </head>
 <body>
  <p>АСУ отключена. Обратитесь к руководству Сети.</p>
 </body>
</html>');