<?php
switch ($modx->event->name) {
	case 'msOnBeforeCreateOrder':
		$orderData = $order->get();
		if ($orderData['payment'] == 1) {
			$fiscal = true;
			$timePeriods = array();
			$timePeriod = $modx->getOption('ch_fiscal_not_time');
			$timePeriod = explode('-',$timePeriod);
			if ($timePeriod[0] > $timePeriod[1]) {
				$timePeriods[0] = array($timePeriod[0],'23:59');
				$timePeriods[1] = array('00:00',$timePeriod[1]);
			} else $timePeriods[0] = $timePeriod;
			unset($timePeriod);
			$timeCurrent = date('H:i');
			foreach($timePeriods as $timePeriod) {
				if (($timePeriod[0] <= $timeCurrent) and ($timeCurrent <= $timePeriod[1])) $fiscal = false;
			}
			if (!$fiscal) {
				//die('not fiscal');
				$orderData['payment'] = 3;
				$order->set($orderData);
				//die(print_r($order->get()));
			}
		}
		break;
}