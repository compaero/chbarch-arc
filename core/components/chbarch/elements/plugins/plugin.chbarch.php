<?php

/** @var modX $modx */
switch ($modx->event->name) {


case 'msOnBeforeAddToCart':
    $options = array();
    $options['price'] = $product->get('price');
    $options['count'] = $product->get('old_price');
    $modx->event->returnedValues['options'] = $options;
        break;
    case 'msOnCreateOrder':
        $sessionGuestVar = $modx->getOption('chguests_session_var');
        unset($_SESSION[$sessionGuestVar]);
        break;

    case 'OnHandleRequest':
        if (isset($_REQUEST['chbarch_action'])) {
            $output = '';
            switch ($_REQUEST['chbarch_action']) {
                case 'tables/refresh': $output = $modx->runSnippet('chbarchGetOrderWithEmptyTable');
            }
            if (is_array($output)) $output = json_encode($output);
            exit($output);
        }
        break;


}