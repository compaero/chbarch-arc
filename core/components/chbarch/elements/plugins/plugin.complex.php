<?php
/** @var msCartHandler $cart */
switch ($modx->event->name) {

    case 'msOnChangeInCart':
        $tmp = $cart->get();
//		$modx->log(1,json_encode($tmp[$key]));
        if ($count) {
            $options = $tmp[$key]['options'];
            if (!is_array($options)) {
                $options = json_decode($options, true);
            }
            //$modx->log(1,var_dump($options));
            $modx->log(1, 'spOffer: ' . $options['spOffer']);
            if (isset($options['spOffer'])) {
                $product = $modx->getObject('msProduct', array('id' => $tmp[$key]['id']));
                //$modx->log(1,'selected resource. template: '.$product->template);
                //if ($product->template == 10) {
                $spOfferKey = $options['spOffer'];
                if (isset($_REQUEST['countDirection']) and ($_REQUEST['countDirection'] == 'up')) {
                    $rCount = round($count / $options['count']);
//					$modx->log(1,'ISSET($_POST[\'countDirection\']): '.$product->template);
                } else {
                    if (isset($_REQUEST['countDirection']) and ($_REQUEST['countDirection'] == 'down')) {
                        $rCount = floor($count / $options['count']);
                    } else {
                        $rCount = $count;
//					$modx->log(1,'EMPTY($_POST[\'countDirection\'])');
                    }
                }

                //$modx->log(1, 'spOfferKey: '.$spOfferKey);
                foreach ($tmp as $k => $productInCart) {

                    //$pOptions = json_decode($productInCart['options'], true);
                    $pOptions = $productInCart['options'];
                    if (isset($pOptions['spOffer'])) {
//						$modx->log(1, 'options -> spOffer found.');
                        if ($pOptions['spOffer'] == $spOfferKey) {
                            if (isset($pOptions['count'])) {
                                $oCount = $pOptions['count'];
                            } else {
                                $oCount = 1;
                            }
                            $tmp[$k]['count'] = $rCount * $oCount;
                        }
                    }
                }
                /*} else {
                    if (isset($options['count'])) {
                        $tmp[$key]['count'] = $options['count'];
                    } else {
                        $tmp[$key]['count'] = 1;
                    }
                    //$count = 1;
                    $modx->log(1,'applySpOffer reset count');
                }*/
            }
        }
        $cart->set($tmp);
        //$modx->log(1,print_r($cart->get()));
        break;

    case 'msOnBeforeRemoveFromCart':
        $tmp = $cart->get();
        //$modx->log(1,print_r($tmp[$key]));
        $options = $tmp[$key]['options'];
        if (!is_array($options)) {
            $options = json_decode($options, true);
        }
        //$modx->log(1,var_dump($options));
        //$modx->log(1,'spOffer: '.$options['spOffer']);
        if (isset($options['spOffer'])) {
            $spOfferKey = $options['spOffer'];
            //$modx->log(1, 'spOffer found');
            $rightProducts = array();
            foreach ($tmp as &$productInCart) {
                $optionsTmp = $productInCart['options'];
                if (!is_array($optionsTmp)) {
                    $optionsTmp = json_decode($optionsTmp, true);
                }
                //$modx->log(1, 'walk for cart. '.$optionsTmp['spOffer']);
                if ($optionsTmp['spOffer'] == $spOfferKey) {
                    unset($productInCart);
//					unset($k);
                    //$modx->log(1,'remove good');
                } else {
                    $rightProducts[] = $productInCart;
                }
            }
            $cart->set($rightProducts);
        }
        break;

    default:
        break;
}