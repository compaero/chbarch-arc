<?php
/**
 * Created by PhpStorm.
 * User: Владимир
 * Date: 19.12.2014
 * Time: 11:12
 */

switch ($modx->event->name) {
    case 'msOnEmptyCart':
    case 'msOnCreateOrder':
    case 'msOnEmptyOrder':
        unset($_SESSION['OrderProperties']);
        break;

    case 'msOnRemoveFromCart':
        $chCart = $cart->get();
        if(empty($chCart)) {
            unset($_SESSION['OrderProperties']);
        }
        break;

    default: break;

}