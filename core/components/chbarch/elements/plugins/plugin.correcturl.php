<?php
if ($modx->event->name != 'OnPageNotFound') {return false;}
$modx->log(1, 'Page not found: index.php?q='.$_REQUEST['q'].'. User: '.$modx->user->id);
if (!($modx->user->id)) {return $modx->log(1, 'User ID is incorrect');}

$uriArray = explode('/',$_REQUEST['q']);
$alias = $uriArray[count($uriArray)-1];
if ($res = $modx->getObject('modResource',array('alias'=>$alias))) {
	$modx->sendRedirect($modx->makeUrl($res->get('id')), 0, 'REDIRECT_HEADER', 'HTTP/1.1 301 Moved Permanently');
}
else {return $modx->log(1, 'Resource with requested alias not found');}