<?php
/* Бывший плагин chDiscount. */

switch ($modx->event->name) {
    // События на добавление товара
    case 'OnHandleRequest':

        if (isset($_REQUEST['chDiscount'])) {
            if (!isset($_SESSION['chDiscount'])) {
                $coeff = (100 - (int)$_REQUEST['chDiscount']) / 100;
                echo $coeff;
                /* @var miniShop2 $miniShop2 */
                $miniShop2 = $modx->getService('minishop2');
                $miniShop2->initialize($ctx, array('json_response' => 1));
                if (!($miniShop2 instanceof miniShop2)) {
                    @session_write_close();
                    exit('Could not initialize miniShop2');
                }
                $cart = $miniShop2->cart->get();

                foreach ($cart as &$product) {
                    $allowDiscount = true;
                    if ($msProduct = $modx->getObject('msProduct', $product['id'])) {
                        if (
                            $msProduct->getTVValue('chbarch_no_discount')
                            or (
                                $parent = $msProduct->getOne('Parent')
                                and $parent->getTVValue('chbarch_no_discount')
                            )
                        ) {
                            $allowDiscount = false;
                        }
                    }
                    if ($allowDiscount) {
                        $product['price'] = round($product['price'] * $coeff);
                    }
                }
                $miniShop2->cart->set($cart);
                $_SESSION['chDiscount']['coeff'] = $coeff;
                if ($chVNet = $modx->getService('chvnet', 'chVNet', $modx->getOption('chvnet_core_path', null,
                        $modx->getOption('core_path') . 'components/chvnet/') . 'model/chvnet/', $scriptProperties)
                ) {
                    //return 'Could not load chVNet class!';
                }
                exit('success');
            }

        }
        break;

    case 'OnBeforeAddToCart':


        // Очистка корзины
    case 'msOnBeforeEmptyCart':
        break; // получает $cart
    case 'msOnEmptyCart':
        unset($_SESSION['chDiscount']);
        break; // получает $cart

}