<?php

/**
 * The base class for chBarch.
 */
class chBarch {
	/* @var modX $modx */
	public $modx;


	/**
	 * @param modX $modx
	 * @param array $config
	 */
	function __construct(modX &$modx, array $config = array()) {
		$this->modx =& $modx;

		$corePath = $this->modx->getOption('chbarch_core_path', $config, $this->modx->getOption('core_path') . 'components/chbarch/');
		$assetsUrl = $this->modx->getOption('chbarch_assets_url', $config, $this->modx->getOption('assets_url') . 'components/chbarch/');
		$connectorUrl = $assetsUrl . 'connector.php';

		$this->config = array_merge(array(
			'assetsUrl' => $assetsUrl,
			'cssUrl' => $assetsUrl . 'css/',
			'jsUrl' => $assetsUrl . 'js/',
			'imagesUrl' => $assetsUrl . 'images/',
			'connectorUrl' => $connectorUrl,

			'corePath' => $corePath,
			'modelPath' => $corePath . 'model/',
			'chunksPath' => $corePath . 'elements/chunks/',
			'templatesPath' => $corePath . 'elements/templates/',
			'chunkSuffix' => '.chunk.tpl',
			'snippetsPath' => $corePath . 'elements/snippets/',
			'processorsPath' => $corePath . 'processors/'
		), $config);

		$this->modx->addPackage('chbarch', $this->config['modelPath']);
		$this->modx->lexicon->load('chbarch:default');
	}


    function findFiscalMode() {//$notFiscalTime = false, $percentNotFiscal = 0) {
        /** @var string $notFiscalTime */
        $notFiscalTime  = $this->modx->getOption('chbarch_fiscal_not_time');
        /** @var float $notFiscalPercent */
        $notFiscalPercent = $this->modx->getOption('chbarch_fiscal_not_percent');
        /** @var int $notFiscalPayment */
        $notFiscalPayment = $this->modx->getOption('chbarch_fiscal_not_payment');
        /** @var boolean $fiscalMode */
        $fiscalMode = true;
        /** @var boolean $currentTimeNotFiscal */
        $currentTimeNotFiscal = false;
        /** @var float $currentNotFiscalPercent */
        $currentNotFiscalPercent = 0;
        /** @var float $countAllOrders */
        $countAllOrders = 0;
        /** @var array $timePeriods */
        $timePeriods = array();
        $beginWorkshiftTime = '';
        $beginWorkshiftDate = '';
        $beginNotFiscalPeriod = '';
        if ($notFiscalTime) {
            $notFiscalTime = explode('-',$notFiscalTime);
            if ($notFiscalTime[0] > $notFiscalTime[1]) {
                $timePeriods[0] = array($notFiscalTime[0],'23:59:59');
                $timePeriods[1] = array('00:00:00',$notFiscalTime[1]);
            } else {
                $timePeriods[0] = $notFiscalTime;
            }
            $beginWorkshiftTime = $notFiscalTime[0];
            if ($beginWorkshiftTime > date('H:s:i')) {
                $beginWorkshiftDate = date("Y-m-d",time() - 24 * 60 * 60);
            }
            else {
                $beginWorkshiftDate = date('Y-m-d');
            }
            $beginNotFiscalPeriod = $beginWorkshiftDate.' '.$beginWorkshiftTime;
            $this->modx->log(1,'begin date: '.$beginNotFiscalPeriod);
            unset($notFiscalTime);
            $timeCurrent = date('H:i:s');
            foreach($timePeriods as $notFiscalTime) {
                if (($notFiscalTime[0] <= $timeCurrent) and ($timeCurrent <= $notFiscalTime[1])) {
                    $currentTimeNotFiscal = true;
                }
            }
        }
        /** @TODO Delete this logging */
        /*if (!$currentTimeNotFiscal) {
            $modx->log(1, 'fiscal time :(');
        }
        else {
            $modx->log(1, 'not fiscal time :)');
        }*/
        if (($currentTimeNotFiscal) and ($notFiscalPercent)){
            $query = $this->modx->newQuery('msOrder');
            $query->select(array('id','payment','count(id) as cnt'));
            $query->where(array('createdon:>='=>$beginNotFiscalPeriod,array('payment'=>'1','OR:payment:='=>5)));
            $query->groupby('payment');
            $query->prepare();
            $query->stmt->execute();
            $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($result as $res) {
                $countAllOrders = $countAllOrders + $res['cnt'];
            }
            if (!$countAllOrders) return true;
            $percent0 = $result[0]['cnt']/$countAllOrders*100;
            $percent1 = $result[1]['cnt']/$countAllOrders*100;
            //$modx->log(1,'payment '.$result[0]['payment'].': '.$percent0.', payment '.$result[1]['payment'].': '.$percent1);
            foreach($result as $res) {
                if ($res['payment'] == $notFiscalPayment) {
                    $currentNotFiscalPercent = ($res['cnt']+1)/$countAllOrders*100;
                    break;
                }
            }
            if (!$currentNotFiscalPercent) {
                $currentNotFiscalPercent = 1 / $countAllOrders*100;
            }
            /** @TODO Delete this logging */
            /*$modx->log(1, 'not fiscal percent: '.$notFiscalPercent);
            $modx->log(1, 'current not fiscal percent: '.$currentNotFiscalPercent);*/
            if ($currentNotFiscalPercent < $notFiscalPercent) {
                $fiscalMode = false;
            }
        }

        return $fiscalMode;
    }


}