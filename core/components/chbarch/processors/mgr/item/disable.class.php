<?php

/**
 * Disable an Item
 */
class chBarchItemDisableProcessor extends modObjectProcessor {
	public $objectType = 'chBarchItem';
	public $classKey = 'chBarchItem';
	public $languageTopics = array('chbarch');
	//public $permission = 'save';


	/**
	 * @return array|string
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}

		$ids = $this->modx->fromJSON($this->getProperty('ids'));
		if (empty($ids)) {
			return $this->failure($this->modx->lexicon('chbarch_item_err_ns'));
		}

		foreach ($ids as $id) {
			/** @var chBarchItem $object */
			if (!$object = $this->modx->getObject($this->classKey, $id)) {
				return $this->failure($this->modx->lexicon('chbarch_item_err_nf'));
			}

			$object->set('active', false);
			$object->save();
		}

		return $this->success();
	}

}

return 'chBarchItemDisableProcessor';
