<?php

/**
 * Class chBarchMainController
 */
abstract class chBarchMainController extends modExtraManagerController {
	/** @var chBarch $chBarch */
	public $chBarch;


	/**
	 * @return void
	 */
	public function initialize() {
		$corePath = $this->modx->getOption('chbarch_core_path', null, $this->modx->getOption('core_path') . 'components/chbarch/');
		require_once $corePath . 'model/chbarch/chbarch.class.php';

		$this->chBarch = new chBarch($this->modx);
		$this->addCss($this->chBarch->config['cssUrl'] . 'mgr/main.css');
		$this->addJavascript($this->chBarch->config['jsUrl'] . 'mgr/chbarch.js');
		$this->addHtml('
		<script type="text/javascript">
			chBarch.config = ' . $this->modx->toJSON($this->chBarch->config) . ';
			chBarch.config.connector_url = "' . $this->chBarch->config['connectorUrl'] . '";
		</script>
		');

		parent::initialize();
	}


	/**
	 * @return array
	 */
	public function getLanguageTopics() {
		return array('chbarch:default');
	}


	/**
	 * @return bool
	 */
	public function checkPermissions() {
		return true;
	}
}


/**
 * Class IndexManagerController
 */
class IndexManagerController extends chBarchMainController {

	/**
	 * @return string
	 */
	public static function getDefaultController() {
		return 'home';
	}
}