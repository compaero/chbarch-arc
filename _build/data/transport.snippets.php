<?php

$snippets = array();

$tmp = array(
	'chbarch' => array(
		'file' => 'chbarch',
		'description' => '',
	),
	'chbarchCollectAllModals' => array(
		'file' => 'collectallmodals',
		'description' => '',
	),
	'chbarchCheckCode' => array(
		'file' => 'checkcode',
		'description' => '',
	),
	'chbarchCreateOrder' => array(
		'file' => 'createorder',
		'description' => '',
	),
	'chbarchGetBarmenLog' => array(
		'file' => 'getbarmenlog',
		'description' => '',
	),
	'chbarchGetOrders' => array(
		'file' => 'getorders',
		'description' => '',
	),
	'chbarchDetectCorrectParents' => array(
		'file' => 'detectcorrectparents',
		'description' => '',
	),
	'chbarchTrimzeros' => array(
		'file' => 'trimzeros',
		'description' => '',
	),
	'chbarchGetOrderWithEmptyTable' => array(
		'file' => 'getorderwithemptytable',
		'description' => '',
	),
	'chbarchRedirectToHome' => array(
		'file' => 'redirecttohome',
		'description' => '',
	),
	'chbarchTimeFirstAddToCart' => array(
		'file' => 'timefirstaddtocart',
		'description' => '',
	),
    'chbarchCreateGroupUserAndUser' => array(
        'file' => 'createGroupUserAndUser',
        'description' => '',
    ),
    'chbarchComplex' => array(
        'file' => 'complex',
        'description' => 'Сниппет для клмплексных заказов',
    ),
);

foreach ($tmp as $k => $v) {
	/* @avr modSnippet $snippet */
	$snippet = $modx->newObject('modSnippet');
	$snippet->fromArray(array(
		'id' => 0,
		'name' => $k,
		'description' => @$v['description'],
		'snippet' => getSnippetContent($sources['source_core'] . '/elements/snippets/snippet.' . $v['file'] . '.php'),
		'static' => BUILD_SNIPPET_STATIC,
		'source' => 1,
		'static_file' => 'core/components/' . PKG_NAME_LOWER . '/elements/snippets/snippet.' . $v['file'] . '.php',
	), '', true, true);

	$properties = include $sources['build'] . 'properties/properties.' . $v['file'] . '.php';
	$snippet->setProperties($properties);

	$snippets[] = $snippet;
}

unset($tmp, $properties);
return $snippets;