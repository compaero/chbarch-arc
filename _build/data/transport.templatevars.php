<?php
$tvs = array();
$tmp = array(
	'chadmint_customcss' => array(
		'caption'   => 'AdmInt custom css',
		'description' => '',
		'type'      => 'textfield',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chadmint_customjs' => array(
		'caption'   => 'AdmInt custom js',
		'description' => '',
		'type'      => 'textfield',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),


	'chadmint_small_tea' => array(
		'caption' => 'маленький',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),

	'chadmint_average_tea' => array(
		'caption'   => 'средний',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),

	'chadmint_big_tea' => array(
		'caption'   => 'большой',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),

	'chbarch_ProductSmall' => array(
		'caption'   => 'Маленький',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_ProductMedium' => array(
		'caption'   => 'Средний',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_ProductLarge' => array(
		'caption'   => 'Большой',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_ProductSingle' => array(
		'caption'   => 'Сингл',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_ProductDouble' => array(
		'caption'   => 'Дабл',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_ProductSingleOut' => array(
		'caption'   => 'Сингл с собой',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_ProductDoubleOut' => array(
		'caption'   => 'Дабл с собой',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_asideLeft' => array(
		'caption'   => 'chbarch_asideLeft',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_categorySortDir' => array(
		'caption'   => 'chbarch_categorySortDir',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_needCalendar' => array(
		'caption'   => 'chbarch_needCalendar',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),

	'chbarch_18_only' => array(
		'caption'   => 'Только после 18 лет',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),

	'chbarch_no_discount' => array(
		'caption'   => 'Скидки сотрудникам не предоставляются',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
	'chbarch_can_decrease' =>array(
		'caption'   => 'списываемый',
		'description' => '',
		'type'      => 'text',
		'display' => 'default',
		'elements' => '',  /* input option values */
		'locked' => 0,
		'rank' => 0,
		'display_params' => '',
		'default_text' => '',
		'properties' => array(),
		'input_properties'  => array(
			'allowBlank'    => true,
			'maxLength'     => '',
			'minLength' => '',
		),
	),
);


foreach ($tmp as $k => $v) {
	/* @avr modTemplate $template */
	$tv = $modx->newObject('modTemplateVar');
	$tv->fromArray(array(
		'id' => 0,
		'name' => $k,
		'caption'   => @$v['caption'],
		'description' => @$v['description'],
		'type'      => @$v['type'],
		'display' => @$v['display'],
		'elements' => @$v['elements'],
		'locked' => @$v['locked'],
		'rank' => @$v['rank'],
		'display_params' => @$v['display_params'],
		'default_text' => @$v['default_text'],
		'properties' => @$v['properties'],
		'input_properties'  => @$v['input_properties'],
		'static' => BUILD_TV_STATIC,
	),'',true,true);
	$tvs[] = $tv;
}
unset($tmp);
return $tvs;