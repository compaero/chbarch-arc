<?php
$templates = array();
$tmp = array(
	'Manager' => array(
		'file' => 'manager',
		'description' => 'Страница раздела менеджера',
	),
	'Authorize' => array(
		'file' => 'authorize',
		'description' => 'Страница авторизации в баре',
	),
	'NewOrder.Category' => array(
		'file' => 'neworder.category',
		'description' => 'Основной раздел',
	),
	'NewOrder.Subcategory' => array(
		'file' => 'neworder.category',
		'description' => 'Подраздел',
	),
	'Product' => array(
		'file' => 'blank',
		'description' => 'Обычный товар',
	),
	'ProductComplex' => array(
		'file' => 'blank',
		'description' => 'Товар-комплекс',
	),
	'ProductSpecial' => array(
		'file' => 'blank',
		'description' => '[УСТАРЕЛ] Товар спец. предложение',
	),
	'SuperProduct' => array(
		'file' => 'blank',
		'description' => 'Кофе (Универсальный), 4 дочерних',
	),
	'SuperTea' => array(
		'file' => 'blank',
		'description' => 'Чай, 3 дочерних',
	),
	'ManageOrders' => array(
		'file' => 'manageorders',
		'description' => 'Управление заказами',
	),
	'Blank.LeftPlusBig' => array(
		'file' => 'blank.leftplusbig',
		'description' => '',
	),
	'Blank.OneCol' => array(
		'file' => 'blank.onecol',
		'description' => '',
	),
);
foreach ($tmp as $k => $v) {
	/* @avr modTemplate $template */
	$template = $modx->newObject('modTemplate');
	$template->fromArray(array(
		'id' => 0,
		'templatename' => $k,
		'description' => @$v['description'],
		'content' => file_get_contents($sources['source_core'].'/elements/templates/template.'.$v['file'].'.tpl'),
		'static' => BUILD_TEMPLATE_STATIC,
		'source' => 1,
		'static_file' => 'core/components/'.PKG_NAME_LOWER.'/elements/templates/template.'.$v['file'].'.tpl',
	),'',true,true);
	$templates[] = $template;
}
unset($tmp);
return $templates;