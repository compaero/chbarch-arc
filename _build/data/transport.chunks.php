<?php

$chunks = array();

$tmp = array(
	'chbarch.order.modal.additional' => array(
		'file' => 'ordermodaladditional',
		'description' => '',
	),
	'chbarch.order.modal.close' => array(
		'file' => 'ordermodalclose',
		'description' => '',
	),
	'chbarch.order.modal.closeByCard' => array(
		'file' => 'ordermodalclosebycard',
		'description' => '',
	),
	'chbarch.order.modal.closeByCash' => array(
		'file' => 'ordermodalclosebycash',
		'description' => '',
	),
	'chbarch.aside.left.menu' => array(
		'file' => 'asideleftmenu',
		'description' => '',
	),
	'chbarch.aside.right' => array(
		'file' => 'asideright',
		'description' => '',
	),
	'tpl.chbarch.logout' => array(
		'file' => 'tpl.logout',
		'description' => '',
	),
	/*
	 * @TODO Вырезать костыль!!!
	 */
	'ch.calendarSort' => array(
		'file' => 'calendarsort',
		'description' => '',
	),
	'chbarch.calendarSort' => array(
		'file' => 'calendarsort',
		'description' => '',
	),
	'tpl.chbarch.orderWithEmptyTable' => array(
		'file' => 'tpl.orderwithemptytable',
		'description' => '',
	),
	'chbarch.msCart.outer' => array(
		'file' => 'mscartouter',
		'description' => '',
	),
	'tpl.chbarch.msCart.row' => array(
		'file' => 'tpl.mscartrow',
		'description' => '',
	),
	'tpl.chbarch.login' => array(
		'file' => 'tpl.login',
		'description' => '',
	),
	'chbarch.footer' => array(
		'file' => 'footer',
		'description' => '',
	),
	'chbarch.head' => array(
		'file' => 'head',
		'description' => '',
	),
	'chbarch.navbar' => array(
		'file' => 'navbar',
		'description' => '',
	),
	'chbarch.scripts' => array(
		'file' => 'scripts',
		'description' => '',
	),
    'tpl.chbarch.menuItem' => array(
        'file' => 'tpl.menuitem',
        'description' => '',
    ),
    'tpl.chbarch.menuItemComplex' => array(
        'file' => 'tpl.menuitemcomplex',
        'description' => '',
    ),
	'tpl.chbarch.menuItemSpecial' => array(
		'file' => 'tpl.menuitem.special',
		'description' => '',
	),
    'tpl.chbarch.menuItem.sProd' => array(
        'file' => 'tpl.menuitem.sprod',
        'description' => '',
    ),
    'tpl.chbarch.menuItem.sTea' => array(
        'file' => 'tpl.menuitem.stea',
        'description' => '',
    ),
    'tpl.chbarch.menuItem.changer' => array(
        'file' => 'tpl.menuitem.changer',
        'description' => '',
    ),
	'tpl.chbarch.orders' => array(
		'file' => 'orders',
		'description' => '',
	),
	'tpl.chbarch.ordersList' => array(
		'file' => 'orderslist',
		'description' => '',
	),
	'tpl.chbarch.delayed' => array(
		'file' => 'tpl.delayed',
		'description' => '',
	),
	'chbarch.order.modal.delayedReturn' => array(
		'file' => 'ordermodaldelayed',
		'description' => '',
	),
	'chbarch.product.modal.additional' => array(
		'file' => 'productmodaladditional',
		'description' => '',
	),
	'chbarch.wrapper.modal.additional' => array(
		'file' => 'wrappermodaladditional',
		'description' => '',
	),
	'tpl.chbarch.comment' => array(
		'file' => 'comment',
		'description' => '',
	),
	'tpl.chbarch.others' => array(
		'file' => 'others',
		'description' => '',
	),
	'tpl.chbarch.chips' => array(
		'file' => 'chips',
		'description' => '',
	),
	'tpl.chbarch.complex' => array(
		'file' => 'complex',
		'description' => '',
	),
	'tpl.chbarch.complexGroup' => array(
		'file' => 'complexgroup',
		'description' => '',
	),
	'tpl.chbarch.complexSub' => array(
		'file' => 'complexsub',
		'description' => '',
	),
	'tpl.chbarch.complexProduct' => array(
		'file' => 'complexproduct',
		'description' => '',
	),
	'tpl.chbarch.complexProductList' => array(
		'file' => 'complexproductlist',
		'description' => '',
	),
    'tpl.chbarch.customGood.button' => array(
		'file' => 'tpl.customgoodbutton',
		'description' => '',
	),
	'chbarch.customGood.modal.close' => array(
		'file' => 'customgoodmodalclose',
		'description' => '',
	),
	'chbarch.complexProduct.modal.count' => array(
		'file' => 'complexproductcount',
		'description' => '',
	),
	/*'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),*/
	/*'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),*/
	/*'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),*/
	/*'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),*/
	/*'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),
	'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),
	'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),
	'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),
	'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),
	'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),
	'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),
	'tpl.chbarch.item' => array(
		'file' => 'item',
		'description' => '',
	),*/
);

// Save chunks for setup options
$BUILD_CHUNKS = array();

foreach ($tmp as $k => $v) {
	/* @avr modChunk $chunk */
	$chunk = $modx->newObject('modChunk');
	$chunk->fromArray(array(
		'id' => 0,
		'name' => $k,
		'description' => @$v['description'],
		'snippet' => file_get_contents($sources['source_core'] . '/elements/chunks/chunk.' . $v['file'] . '.tpl'),
		'static' => BUILD_CHUNK_STATIC,
		'source' => 1,
		'static_file' => 'core/components/' . PKG_NAME_LOWER . '/elements/chunks/chunk.' . $v['file'] . '.tpl',
	), '', true, true);

	$chunks[] = $chunk;

	$BUILD_CHUNKS[$k] = file_get_contents($sources['source_core'] . '/elements/chunks/chunk.' . $v['file'] . '.tpl');
}

unset($tmp);
return $chunks;