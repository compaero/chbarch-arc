<?php

$plugins = array();

$tmp = array(
	'chBarch' => array(
		'file' => 'chbarch',
		'description' => '',
		'events' => array(
			'msOnCreateOrder' => array(),
			'msOnBeforeAddToCart' => array(),
			'OnHandleRequest' => array(),
		)
	),
	'chbarchTimeCreateOrderChain' => array(
		'file' => 'timecreateorderchain',
		'description' => '',
		'events' => array(
			'msOnAddToCart' => array(),
			'msOnEmptyCart' => array()
		)
	),
	'chbarchCorrectUrl' => array(
		'file' => 'correcturl',
		'description' => '',
		'events' => array(
			'OnPageNotFound' => array(),
		)
	),
	'chbarchCinnamon' => array(
		'file' => 'cinnamon',
		'description' => '',
		'events' => array(
			'msOnAddToCart' => array(),
			'msOnChangeInCart' => array()
		)
	),
	'chbarchChangePayment' => array(
		'file' => 'changepayment',
		'description' => '',
		'events' => array(
			'msOnBeforeCreateOrder' => array(),
		)
	),
    'chbarchClearOrderProps' => array(
        'file' => 'clearorderprops',
        'description' => '',
        'events' => array(
			'msOnEmptyCart' => array(),
			'msOnEmptyOrder' => array(),
			'msOnRemoveFromCart' => array(),
			'msOnCreateOrder' => array(),
        )
    ),
	'chbarchComplex' => array(
		'file' => 'complex',
		'description' => '',
		'events' => array(
			'msOnAddToCart' => array(),
			'msOnBeforeAddToCart' => array(),
			'msOnChangeInCart' => array(),
			'msOnBeforeRemoveFromCart' => array(),
			'msOnRemoveFromCart' => array(),
		)
	),

	'chbarchDiscount' => array(
		'file' => 'discount',
		'description' => '',
		'events' => array(
			'msOnEmptyCart' => array(),
			'OnHandleRequest' => array(),
			'OnWebPageInit' => array(),
		)
	),

	'chbarchBlockCoffee' => array(
		'file' => 'blockcoffee',
		'description' => '',
		'events' => array(
//			'OnHandleRequest' => array(),
		)
	),
);

foreach ($tmp as $k => $v) {
	/* @avr modplugin $plugin */
	$plugin = $modx->newObject('modPlugin');
	$plugin->fromArray(array(
		'name' => $k,
		'category' => 0,
		'description' => @$v['description'],
		'plugincode' => getSnippetContent($sources['source_core'] . '/elements/plugins/plugin.' . $v['file'] . '.php'),
		'static' => BUILD_PLUGIN_STATIC,
		'source' => 1,
		'static_file' => 'core/components/' . PKG_NAME_LOWER . '/elements/plugins/plugin.' . $v['file'] . '.php'
	), '', true, true);

	$events = array();
	if (!empty($v['events'])) {
		foreach ($v['events'] as $k2 => $v2) {
			/* @var modPluginEvent $event */
			$event = $modx->newObject('modPluginEvent');
			$event->fromArray(array_merge(
				array(
					'event' => $k2,
					'priority' => 0,
					'propertyset' => 0,
				), $v2
			), '', true, true);
			$events[] = $event;
		}
		unset($v['events']);
	}

	if (!empty($events)) {
		$plugin->addMany($events);
	}

	$plugins[] = $plugin;
}

unset($tmp, $properties);
return $plugins;