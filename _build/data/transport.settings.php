<?php

$settings = array();

$tmp = array(
	'workshift_day' => array(
		'xtype' => 'textfield',
		'value' => '09:00:00-20:59:59',
		'area' => 'chbarch_workshift',
	),
	'workshift_night' => array(
		'xtype' => 'textfield',
		'value' => '21:00:00-08:59:59',
		'area' => 'chbarch_workshift',
	),
	'fiscal_not_time' => array(
		'xtype' => 'textfield',
		'value' => '01:00-06:59',
		'area' => 'chbarch_fiscal',
	),
	'fiscal_not_percent' => array(
		'xtype' => 'textfield',
		'value' => '90',
		'area' => 'chbarch_fiscal',
	),
	'fiscal_not_payment' => array(
		'xtype' => 'textfield',
		'value' => '5',
		'area' => 'chbarch_fiscal',
	),
	'orders_dibs' => array(
		'xtype' => 'textfield',
		'value' => '{"dibOrder1":{"name":"\u0424\u0438\u0448\u043a\u0430 \u0437\u043a 1","icon":"fa"},"dibOrder2":{"name":"\u0424\u0438\u0448\u043a\u0430 \u0437\u043a 2","icon":"fa"},"dibOrder3":{"name":"\u0424\u0438\u0448\u043a\u0430 \u0437\u043a 3","icon":"fa"}}',
		'area' => 'chbarch_dibs',
	),
	'products_dibs' => array(
		'xtype' => 'textfield',
		'value' => '{"dibProd1":{"name":"\u0424\u0438\u0448\u043a\u0430 \u043f\u0440 1","icon":"fa"},"dibProd2":{"name":"\u0424\u0438\u0448\u043a\u0430 \u043f\u0440 2","icon":"fa"},"dibProd3":{"name":"\u0424\u0438\u0448\u043a\u0430 \u043f\u0440 3","icon":"fa"}}',
		'area' => 'chbarch_dibs',
	),
	'goods_tree_id' => array(
		'xtype' => 'textfield',
		'value' => '11',
		'area' => 'chbarch_main',
	),
	'force_update_scripts' => array(
		'xtype' => 'combo-boolean',
		'value' => false,
		'area' => 'chbarch_main',
	),
	'template_product' => array(
		'xtype' => 'textfield',
		'value' => '1',
		'area' => 'chbarch_templates',
	),
	'template_product_special' => array(
		'xtype' => 'textfield',
		'value' => '10',
		'area' => 'chbarch_templates',
	),
	'template_product_super' => array(
		'xtype' => 'textfield',
		'value' => '11',
		'area' => 'chbarch_templates',
	),
	'template_product_supertea' => array(
		'xtype' => 'textfield',
		'value' => '12',
		'area' => 'chbarch_templates',
	),
	'template_product_complex' => array(
		'xtype' => 'textfield',
		'value' => '13',
		'area' => 'chbarch_templates',
	),
	'custom_discount' => array(
		'xtype' => 'textfield',
		'value' => '0',
		'area' => 'chbarch_main',
	),

);

foreach ($tmp as $k => $v) {
	/* @var modSystemSetting $setting */
	$setting = $modx->newObject('modSystemSetting');
	$setting->fromArray(array_merge(
		array(
			'key' => 'chbarch_' . $k,
			'namespace' => PKG_NAME_LOWER,
		), $v
	), '', true, true);

	$settings[] = $setting;
}

unset($tmp);
return $settings;
