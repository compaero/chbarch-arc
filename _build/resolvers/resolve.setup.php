<?php
/**
 * Resolves setup-options settings
 *
 * @var xPDOObject $object
 * @var array $options
 */

if ($object->xpdo) {
    /** @var modX $modx */
    $modx =& $object->xpdo;

    $success = false;
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
            //createResource ($modx);
        case xPDOTransport::ACTION_UPGRADE:
        if($blacktie = $modx->getObject('modResource',array('introtext'=>'fb69df49c20e9baca7ffad1196c20acf'))){
            if($neword = $modx->getObject('modResource',array('introtext'=>'9a1a009d2c17ae652cf78989a5bf8813'))){
                $neword->set('content',$blacktie->get('id'));
                $neword->save();
            }

        }
            if (!$catRes = $modx->getObject('modResource', array('introtext' => 'werwrd4543gh45cbdghf6435bsefd'))) {

                $pages = $modx->newObject('modResource', array(
                    'introtext' => 'werwrd4543gh45cbdghf6435bsefd',
                    'pagetitle' => 'categories',
                    'published' => 1,
                    'alias' => 'categories',
                    'parent' => 299,
                    'hidemenu' => 1,
                    'type' => 'document',
                    'contentType' => 'text/html',
                    'content' => file_get_contents(MODX_ASSETS_PATH . 'components/chbarch/resource/categories_content.tpl'),
                    'show_in_tree' => 1,


                ));
                $pages->save();

            } else {
                $catRes->set('content',
                    file_get_contents(MODX_ASSETS_PATH . 'components/chbarch/resource/categories_content.tpl'));
                $catRes->save();
            }

            $orderDibs = array(
                'dibOrder1' => array(
                    'name' => 'Позже',
                    'icon' => 'fa fa-clock-o'
                ),
                'dibOrder2' => array(
                    'name' => 'С собой',
                    'icon' => 'fa fa-car'
                ),
                'dibOrder3' => array(
                    'name' => 'В один',
                    'icon' => 'fa fa-compress'
                ),
            );

            $setting = $modx->getObject('modSystemSetting', 'chbarch_orders_dibs');
            $setting->set('value', json_encode($orderDibs));
            $setting->save();
            $modx->cacheManager->refresh(array(
                'system_settings' => array('key' => 'chbarch_orders_dibs')
            ));

            $productDibs = array(
                'dibOrder1' => array(
                    'name' => 'Позже',
                    'icon' => 'fa fa-clock-o'
                ),
                'dibOrder2' => array(
                    'name' => 'Холодное',
                    'icon' => 'fa fa-asterisk'
                ),
                'dibOrder3' => array(
                    'name' => 'Горячее',
                    'icon' => 'fa fa-fire'
                ),
            );

            $setting = $modx->getObject('modSystemSetting', 'chbarch_products_dibs');
            $setting->set('value', json_encode($productDibs));
            $setting->save();
            $modx->cacheManager->refresh(array(
                'system_settings' => array('key' => 'chbarch_products_dibs')
            ));
//
//
//            $tvNames = array('chbarch_no_discount', 'chbarch_18_only');
//            $templateNames = array(
//                'NewOrder.Category',
//                'NewOrder.Subcategory',
//                'Product',
//                'ProductComplex',
//                'ProductSpecial',
//                'SuperProduct' ,
//                'SuperTea'
//            );
//            $templates = $modx->getCollection('modTemplate', array('templatename:IN' => $templateNames));
//            foreach ($tvNames as $tvName) {
//                if ($tv = $modx->getObject('modTemplateVar', array('name' => $tvName))) {
//                    foreach ($templates as $template) {
//                        if (!$rel = $modx->getObject('modTemplateVarTemplate', array('tmplvarid' => $tv->get('id'), 'templateid' => $template->get('id')))) {
//                            $rel = $modx->newObject('modTemplateVarTemplate');
//							$rel->set('tmplvarid', $tv->get('id'));
//							$rel->set('templateid', $template->get('id'));
//							$rel->save();
//							unset($rel);
//                        }
//                    }
//
//                }
//            }


                    $tvNames = array('chbarch_can_decrease');
            $templateNames = array(
                'Product',
            );
            $templates = $modx->getCollection('modTemplate', array('templatename:IN' => $templateNames));
            foreach ($tvNames as $tvName) {
                if ($tv = $modx->getObject('modTemplateVar', array('name' => $tvName))) {
                    foreach ($templates as $template) {
                        if (!$rel = $modx->getObject('modTemplateVarTemplate', array('tmplvarid' => $tv->get('id'), 'templateid' => $template->get('id')))) {
                            $rel = $modx->newObject('modTemplateVarTemplate');
							$rel->set('tmplvarid', $tv->get('id'));
							$rel->set('templateid', $template->get('id'));
							$rel->save();
							unset($rel);
                        }
                    }

                }
            }

            // Checking and installing required packages
            $packages = array(/*'pdoTools' => array(
					'version_major' => 1,
					'version_minor:>=' => 9,
				)*/
            );
            foreach ($packages as $package => $options) {
                $query = array('package_name' => $package);
                if (!empty($options)) {
                    $query = array_merge($query, $options);
                }
                if (!$modx->getObject('transport.modTransportPackage', $query)) {
                    $modx->log(modX::LOG_LEVEL_INFO, 'Trying to install <b>' . $package . '</b>. Please wait...');

                    $response = installPackage($package);
                    if ($response['success']) {
                        $level = modX::LOG_LEVEL_INFO;
                    } else {
                        $level = modX::LOG_LEVEL_ERROR;
                    }

                    $modx->log($level, $response['message']);
                }
            }
            $success = true;
            break;

        case xPDOTransport::ACTION_UNINSTALL:
            $success = true;
            break;
    }

    return $success;
}


/**
 * @param $packageName
 *
 * @return array|bool
 */
///*function installPackage($packageName)
//{
//    global $modx;
//
//    /** @var modTransportProvider $provider */
//    if (!$provider = $modx->getObject('transport.modTransportProvider', array('service_url:LIKE' => '%simpledream%'))) {
//        $provider = $modx->getObject('transport.modTransportProvider', 1);
//    }
//
////	$provider->getClient();
//    $version = $modx->getVersionData();
//    $productVersion = $version['code_name'] . '-' . $version['full_version'];
//
//    $response = $provider->request('package', 'GET', array(
//        'supports' => $productVersion,
//        'query' => $packageName
//    ));
//
//    if (!empty($response)) {
//        $foundPackages = simplexml_load_string($response->response);
//        foreach ($foundPackages as $foundPackage) {
//            /** @var modTransportPackage $foundPackage */
//            if ($foundPackage->name == $packageName) {
//                $sig = explode('-', $foundPackage->signature);
//                $versionSignature = explode('.', $sig[1]);
//                $url = $foundPackage->location;
//
//                if (!downloadPackage($url,
//                    MODX_CORE_PATH . 'packages/' . $foundPackage->signature . '.transport.zip')
//                ) {
//                    return array(
//                        'success' => 0,
//                        'message' => 'Could not download package <b>' . $packageName . '</b>.',
//                    );
//                }
//
//                // Add in the package as an object so it can be upgraded
//                /** @var modTransportPackage $package */
//                $package = $modx->newObject('transport.modTransportPackage');
//                $package->set('signature', $foundPackage->signature);
//                $package->fromArray(array(
//                    'created' => date('Y-m-d h:i:s'),
//                    'updated' => null,
//                    'state' => 1,
//                    'workspace' => 1,
//                    'provider' => $provider->id,
//                    'source' => $foundPackage->signature . '.transport.zip',
//                    'package_name' => $foundPackage->name,
//                    'version_major' => $versionSignature[0],
//                    'version_minor' => !empty($versionSignature[1]) ? $versionSignature[1] : 0,
//                    'version_patch' => !empty($versionSignature[2]) ? $versionSignature[2] : 0,
//                ));
//
//                if (!empty($sig[2])) {
//                    $r = preg_split('/([0-9]+)/', $sig[2], -1, PREG_SPLIT_DELIM_CAPTURE);
//                    if (is_array($r) && !empty($r)) {
//                        $package->set('release', $r[0]);
//                        $package->set('release_index', (isset($r[1]) ? $r[1] : '0'));
//                    } else {
//                        $package->set('release', $sig[2]);
//                    }
//                }
//
//                if ($package->save() && $package->install()) {
//                    return array(
//                        'success' => 1,
//                        'message' => '<b>' . $packageName . '</b> was successfully installed',
//                    );
//                } else {
//                    return array(
//                        'success' => 0,
//                        'message' => 'Could not save package <b>' . $packageName . '</b>',
//                    );
//                }
//                break;
//            }
//        }
//    } else {
//        return array(
//            'success' => 0,
//            'message' => 'Could not find <b>' . $packageName . '</b> in MODX repository',
//        );
//    }
//
//    return true;
//}*/


/**
 * @param $src
 * @param $dst
 *
 * @return bool
 */
//function downloadPackage($src, $dst)
//{
//    if (ini_get('allow_url_fopen')) {
//        $file = @file_get_contents($src);
//    } else {
//        if (function_exists('curl_init')) {
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $src);
//            curl_setopt($ch, CURLOPT_HEADER, 0);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
//            $safeMode = @ini_get('safe_mode');
//            $openBasedir = @ini_get('open_basedir');
//            if (empty($safeMode) && empty($openBasedir)) {
//                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//            }
//
//            $file = curl_exec($ch);
//            curl_close($ch);
//        } else {
//            return false;
//        }
//    }
//    file_put_contents($dst, $file);
//
//    return file_exists($dst);
//}


/*function createResource (&$modx) {
    $my_file = file_get_contents($_SERVER['DOCUMENT_ROOT']."/assets/components/chbarch/resource/jsonResource.txt");//assets/components/chbarch/resource/
    $resArr = json_decode($my_file, true);

    foreach ($resArr as $key => $val) {
        $response = $modx->runProcessor('resource/create', $val);
        if ($response->isError()) {
            $modx->log(modX::LOG_LEVEL_ERROR, 'Ошибка создания ресурса: '.$key);
        } else {
            $modx->log(modX::LOG_LEVEL_ERROR, 'Ресурс создан: '.$key);
        }


        $idRoot = $response->response['object']['id'];
        $idArr[$val['introtext']] = $idRoot;
    }
    //print_r($idArr);

    foreach($resArr as $key => $val) {
        if($val['parent'] !== 0) {
            $idRoor = $idArr[$val['parent']];
            $idChild = $idArr[$val['introtext']];
            $alias = $val['alias'];
            $response = $modx->runProcessor('resource/update', array('id' => $idChild, 'parent' => $idRoor, 'alias' => $alias, 'context_key' => 'web'));
            if ($response->isError()) {
                $modx->log(modX::LOG_LEVEL_ERROR, 'Ошибка update ресурса: '.$key);
            } else {
                $modx->log(modX::LOG_LEVEL_ERROR, 'Ресурс update: '.$key);
            }
        }
    }
    $modx->log(modX::LOG_LEVEL_ERROR, 'Создание русурсов завершенно!');
}*/